package main

import (
	"gitee.com/ichub/gogateway/cmd/gogateway/gateway"
	"gitee.com/ichub/gomini/mini/file/minifactroy"
	"github.com/sirupsen/logrus"
	"os"
)

const ThisPkgName = "gitee.com/ichub/gogateway/gateway/server/router"

func main() {
	var pkgroot = minifactroy.FindBeanMinifactroy().FindPkgRootDirOf(ThisPkgName)
	os.Setenv("BasePath", pkgroot)
	logrus.Info("base path=", os.Getenv("BasePath"))

	gateway.StartWeb()
}
