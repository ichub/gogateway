package dao

/*
   @Title  文件名称: ServiceInfodao.go
   @Description 描述: 服务ServiceInfodao

   @Author  作者: leijianming@163.com  时间(2024-06-01 09:25:07)
   @Update  作者: leijianming@163.com  时间(2024-06-01 09:25:07)

*/

import (
	//"fmt"

	"gitee.com/ichub/goconfig/common/base/basedto"
	"gitee.com/ichub/goweb/common/basedao"
	"log"
	//"github.com/jinzhu/gorm"
	// "gitee.com/ichub/goconfig/common/base/basemodel"

	"gitee.com/ichub/goweb/common/pagemsg/page"
	//"gitee.com/ichub/goconfig/common/base/baseconsts"
	//"gitee.com/ichub/goconfig/common/dbcontent"
	//"gitee.com/ichub/gocode/db/dto"
	"gitee.com/ichub/gogateway/db/model"
)

var InstServiceInfoDao ServiceInfoDao

type ServiceInfoDao struct {
	basedao.BaseDao
	basedto.BaseEntitySingle
}

func NewServiceInfoDao() *ServiceInfoDao {
	return &ServiceInfoDao{}
}

func (this *ServiceInfoDao) FindData(result *basedto.IchubResult) *model.ServiceInfo {
	return result.Data.(*model.ServiceInfo)
}

func (this *ServiceInfoDao) FindDataList(result *page.PageResult) *[]model.ServiceInfo {
	return result.Data.(*[]model.ServiceInfo)
}

/*
@title     函数名称:  Insert
@description      :  新增记录
@auth      作者:      leijianming@163.com 时间: 2024-06-01 09:25:07
@param     输入参数名: entity *model.ServiceInfo
@return    返回参数名: int32,error
*/
func (this *ServiceInfoDao) Insert(entity *model.ServiceInfo) (int32, error) {

	err := this.GetDB().Create(entity).Error
	if err != nil {
		log.Println(err.Error())
		return -1, err
	}

	return entity.Id, err
}

/*
@title     函数名称:  DeleteById
@description      :  根据主键Id 删除记录

@auth      作者:      leijianming@163.com 时间: 2024-06-01 09:25:07
@param     输入参数名: Id int32
@return    返回参数名: error
*/
func (this *ServiceInfoDao) DeleteById(Id int32) error {
	var entity model.ServiceInfo

	err := this.GetDB().Where("id=?", Id).Delete(&entity).Error
	return err
}

/*
@title     函数名称:  Save
@description      :  保存记录

@auth      作者:      leijianming@163.com 时间: 2024-06-01 09:25:07
@param     输入参数名: entity *model.ServiceInfo
@return    返回参数名: int32,error
*/
func (this *ServiceInfoDao) Save(entity *model.ServiceInfo) (int32, error) {

	err := this.GetDB().Save(entity).Error
	if err != nil {
		log.Println(err.Error())
		return -1, err
	}
	return entity.Id, err
}

/*
@title     函数名称:  Update
@description      :  修改记录

@auth      作者:      leijianming@163.com 时间: 2024-06-01 09:25:07
@param     输入参数名: entity *model.ServiceInfo
@return    返回参数名: int32,error
*/
func (this *ServiceInfoDao) Update(entity *model.ServiceInfo) (int32, error) {

	err := this.GetDB().Model(&model.ServiceInfo{}).Where("id=?", entity.Id).Updates(entity).Error
	if err != nil {
		log.Println(err.Error())
		return -1, err
	}
	return entity.Id, err
}

func (this *ServiceInfoDao) UpdateNotNull(Id int32, maps map[string]interface{}) (int32, error) {

	err := this.GetDB().Model(&model.ServiceInfo{}).Where("id=?", Id).Updates(maps).Error
	return Id, err
}

/*
@title     函数名称:  UpdateMap
@description      :  根据主键Id,map修改指定字段

@auth      作者:      leijianming@163.com 时间: 2024-06-01 09:25:07
@param     输入参数名: Id int64,entity map[string]interface{}
@return    返回参数名: int64,error
*/
func (this *ServiceInfoDao) UpdateMap(Id int32, maps map[string]interface{}) (int32, error) {

	err := this.GetDB().Model(&model.ServiceInfo{}).Where("id=?", Id).Updates(maps).Error
	return Id, err
}

/*
@title     函数名称:  FindById
@description      :  根据主键Id 查询记录

@auth      作者:      leijianming@163.com 时间: 2024-06-01 09:25:07
@param     输入参数名: Id int32
@return    返回参数名: entity *model.ServiceInfo, found bool, err error
*/
func (this *ServiceInfoDao) FindById(Id int32) (entity *model.ServiceInfo, found bool, err error) {

	entity = new(model.ServiceInfo)

	db := this.GetDB().First(entity, Id)
	found = !db.RecordNotFound()
	err = db.Error

	return entity, found, err

}

/*
@title     函数名称:  FindByIds
@description      :  根据主键Id 查询多条记录; FindByIds("1,36,39")

@auth      作者:      leijianming@163.com 时间: 2024-06-01 09:25:07
@param     输入参数名: pks string
@return    返回参数名: *[]model.ServiceInfo,error
*/
func (this *ServiceInfoDao) FindByIds(pks string) (*[]model.ServiceInfo, error) {

	var entities []model.ServiceInfo
	db := this.GetDB().Raw("select * from service_info where id in (" + pks + ")").Scan(&entities)

	return &entities, db.Error
}

/*
@title     函数名称:  Query
@description      :  查询符合条件的记录！通用条件分页

@auth      作者:      leijianming@163.com 时间: 2024-06-01 09:25:07
@param     输入参数名: param *pagedto.IchubPageRequest
@return    返回参数名: *pagedto.IchubPageResult
*/
func (this *ServiceInfoDao) Query(param *page.PageRequest) *page.PageResult {

	var entities []model.ServiceInfo
	return param.Query(&model.ServiceInfo{}, &entities)
}

/*
@title     函数名称:  Count
@description      :  计算符合条件的记录总数！PageRequest通用条件

@auth      作者:      leijianming@163.com 时间: 2024-06-01 09:25:07
@param     输入参数名: param *pagedto.IchubPageRequest
@return    返回参数名: int, error
*/
func (this *ServiceInfoDao) Count(param *page.PageRequest) (int, error) {

	return param.Count(&model.ServiceInfo{})
}
