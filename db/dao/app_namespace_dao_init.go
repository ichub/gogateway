package dao

import (
	"gitee.com/ichub/goconfig/common/base/baseiface"
	"gitee.com/ichub/goconfig/common/basedi"
	"github.com/sirupsen/logrus"
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: app_namespace_dao_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: LEIJMDAS@163.COM 时间(2024-06-01 10:25:52)
	@Update 作者: LEIJMDAS@163.COM 时间(2024-06-01 10:25:52)

* *********************************************************/

const singleNameAppNamespaceDao = "dao.AppNamespaceDao"

// init center load
func init() {
	registerBeanAppNamespaceDao()
}

// center AppNamespaceDao
func registerBeanAppNamespaceDao() {
	basedi.RegisterLoadBean(singleNameAppNamespaceDao, LoadAppNamespaceDao)
}

func FindBeanAppNamespaceDao() *AppNamespaceDao {
	bean, ok := basedi.FindBean(singleNameAppNamespaceDao).(*AppNamespaceDao)
	if !ok {
		logrus.Errorf("FindBeanAppNamespaceDao: failed to cast bean to *AppNamespaceDao")
		return nil
	}
	return bean

}

func LoadAppNamespaceDao() baseiface.ISingleton {
	var s = NewAppNamespaceDao()
	InjectAppNamespaceDao(s)
	return s

}

func InjectAppNamespaceDao(s *AppNamespaceDao) {

	logrus.Debug("inject")
}
