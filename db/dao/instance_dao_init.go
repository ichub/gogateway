package dao

import (
	"gitee.com/ichub/goconfig/common/base/baseiface"
	"gitee.com/ichub/goconfig/common/basedi"
	"github.com/sirupsen/logrus"
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: instance_dao_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: LEIJMDAS@163.COM 时间(2024-06-01 10:25:52)
	@Update 作者: LEIJMDAS@163.COM 时间(2024-06-01 10:25:52)

* *********************************************************/

const singleNameInstanceDao = "dao.InstanceDao"

// init center load
func init() {
	registerBeanInstanceDao()
}

// center InstanceDao
func registerBeanInstanceDao() {
	basedi.RegisterLoadBean(singleNameInstanceDao, LoadInstanceDao)
}

func FindBeanInstanceDao() *InstanceDao {
	bean, ok := basedi.FindBean(singleNameInstanceDao).(*InstanceDao)
	if !ok {
		logrus.Errorf("FindBeanInstanceDao: failed to cast bean to *InstanceDao")
		return nil
	}
	return bean

}

func LoadInstanceDao() baseiface.ISingleton {
	var s = NewInstanceDao()
	InjectInstanceDao(s)
	return s

}

func InjectInstanceDao(s *InstanceDao) {

	logrus.Debug("inject")
}
