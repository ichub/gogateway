package dao

import (
	"gitee.com/ichub/goconfig/common/base/baseiface"
	"gitee.com/ichub/goconfig/common/basedi"
	"github.com/sirupsen/logrus"
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: env_dao_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: LEIJMDAS@163.COM 时间(2024-06-01 10:25:52)
	@Update 作者: LEIJMDAS@163.COM 时间(2024-06-01 10:25:52)

* *********************************************************/

const singleNameEnvDao = "dao.EnvDao"

// init center load
func init() {
	registerBeanEnvDao()
}

// center EnvDao
func registerBeanEnvDao() {
	basedi.RegisterLoadBean(singleNameEnvDao, LoadEnvDao)
}

func FindBeanEnvDao() *EnvDao {
	bean, ok := basedi.FindBean(singleNameEnvDao).(*EnvDao)
	if !ok {
		logrus.Errorf("FindBeanEnvDao: failed to cast bean to *EnvDao")
		return nil
	}
	return bean

}

func LoadEnvDao() baseiface.ISingleton {
	var s = NewEnvDao()
	InjectEnvDao(s)
	return s

}

func InjectEnvDao(s *EnvDao) {

	logrus.Debug("inject")
}
