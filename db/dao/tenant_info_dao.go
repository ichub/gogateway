package dao

/*
   @Title  文件名称: TenantInfodao.go
   @Description 描述: 服务TenantInfodao

   @Author  作者: leijianming@163.com  时间(2024-06-01 09:25:13)
   @Update  作者: leijianming@163.com  时间(2024-06-01 09:25:13)

*/

import (
	//"fmt"

	"gitee.com/ichub/goconfig/common/base/basedto"
	"gitee.com/ichub/goweb/common/basedao"
	"log"
	//"github.com/jinzhu/gorm"
	// "gitee.com/ichub/goconfig/common/base/basemodel"

	"gitee.com/ichub/goweb/common/pagemsg/page"
	//"gitee.com/ichub/goconfig/common/base/baseconsts"
	//"gitee.com/ichub/goconfig/common/dbcontent"
	//"gitee.com/ichub/gocode/db/dto"
	"gitee.com/ichub/gogateway/db/model"
)

var InstTenantInfoDao TenantInfoDao

type TenantInfoDao struct {
	basedao.BaseDao
	basedto.BaseEntitySingle
}

func NewTenantInfoDao() *TenantInfoDao {
	return &TenantInfoDao{}
}

func (this *TenantInfoDao) FindData(result *basedto.IchubResult) *model.TenantInfo {
	return result.Data.(*model.TenantInfo)
}

func (this *TenantInfoDao) FindDataList(result *page.PageResult) *[]model.TenantInfo {
	return result.Data.(*[]model.TenantInfo)
}

/*
@title     函数名称:  Insert
@description      :  新增记录
@auth      作者:      leijianming@163.com 时间: 2024-06-01 09:25:13
@param     输入参数名: entity *model.TenantInfo
@return    返回参数名: int64,error
*/
func (this *TenantInfoDao) Insert(entity *model.TenantInfo) (int64, error) {

	err := this.GetDB().Create(entity).Error
	if err != nil {
		log.Println(err.Error())
		return -1, err
	}

	return entity.Id, err
}

/*
@title     函数名称:  DeleteById
@description      :  根据主键Id 删除记录

@auth      作者:      leijianming@163.com 时间: 2024-06-01 09:25:13
@param     输入参数名: Id int64
@return    返回参数名: error
*/
func (this *TenantInfoDao) DeleteById(Id int64) error {
	var entity model.TenantInfo

	err := this.GetDB().Where("id=?", Id).Delete(&entity).Error
	return err
}

/*
@title     函数名称:  Save
@description      :  保存记录

@auth      作者:      leijianming@163.com 时间: 2024-06-01 09:25:13
@param     输入参数名: entity *model.TenantInfo
@return    返回参数名: int64,error
*/
func (this *TenantInfoDao) Save(entity *model.TenantInfo) (int64, error) {

	err := this.GetDB().Save(entity).Error
	if err != nil {
		log.Println(err.Error())
		return -1, err
	}
	return entity.Id, err
}

/*
@title     函数名称:  Update
@description      :  修改记录

@auth      作者:      leijianming@163.com 时间: 2024-06-01 09:25:13
@param     输入参数名: entity *model.TenantInfo
@return    返回参数名: int64,error
*/
func (this *TenantInfoDao) Update(entity *model.TenantInfo) (int64, error) {

	err := this.GetDB().Model(&model.TenantInfo{}).Where("id=?", entity.Id).Updates(entity).Error
	if err != nil {
		log.Println(err.Error())
		return -1, err
	}
	return entity.Id, err
}

func (this *TenantInfoDao) UpdateNotNull(Id int64, maps map[string]interface{}) (int64, error) {

	err := this.GetDB().Model(&model.TenantInfo{}).Where("id=?", Id).Updates(maps).Error
	return Id, err
}

/*
@title     函数名称:  UpdateMap
@description      :  根据主键Id,map修改指定字段

@auth      作者:      leijianming@163.com 时间: 2024-06-01 09:25:13
@param     输入参数名: Id int64,entity map[string]interface{}
@return    返回参数名: int64,error
*/
func (this *TenantInfoDao) UpdateMap(Id int64, maps map[string]interface{}) (int64, error) {

	err := this.GetDB().Model(&model.TenantInfo{}).Where("id=?", Id).Updates(maps).Error
	return Id, err
}

/*
@title     函数名称:  FindById
@description      :  根据主键Id 查询记录

@auth      作者:      leijianming@163.com 时间: 2024-06-01 09:25:13
@param     输入参数名: Id int64
@return    返回参数名: entity *model.TenantInfo, found bool, err error
*/
func (this *TenantInfoDao) FindById(Id int64) (entity *model.TenantInfo, found bool, err error) {

	entity = new(model.TenantInfo)

	db := this.GetDB().First(entity, Id)
	found = !db.RecordNotFound()
	err = db.Error

	return entity, found, err

}

/*
@title     函数名称:  FindByIds
@description      :  根据主键Id 查询多条记录; FindByIds("1,36,39")

@auth      作者:      leijianming@163.com 时间: 2024-06-01 09:25:13
@param     输入参数名: pks string
@return    返回参数名: *[]model.TenantInfo,error
*/
func (this *TenantInfoDao) FindByIds(pks string) (*[]model.TenantInfo, error) {

	var entities []model.TenantInfo
	db := this.GetDB().Raw("select * from tenant_info where id in (" + pks + ")").Scan(&entities)

	return &entities, db.Error
}

/*
@title     函数名称:  Query
@description      :  查询符合条件的记录！通用条件分页

@auth      作者:      leijianming@163.com 时间: 2024-06-01 09:25:13
@param     输入参数名: param *pagedto.IchubPageRequest
@return    返回参数名: *pagedto.IchubPageResult
*/
func (this *TenantInfoDao) Query(param *page.PageRequest) *page.PageResult {

	var entities []model.TenantInfo
	return param.Query(&model.TenantInfo{}, &entities)
}

/*
@title     函数名称:  Count
@description      :  计算符合条件的记录总数！PageRequest通用条件

@auth      作者:      leijianming@163.com 时间: 2024-06-01 09:25:13
@param     输入参数名: param *pagedto.IchubPageRequest
@return    返回参数名: int, error
*/
func (this *TenantInfoDao) Count(param *page.PageRequest) (int, error) {

	return param.Count(&model.TenantInfo{})
}
