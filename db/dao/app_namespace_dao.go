package dao

/*
   @Title  文件名称: AppNamespacedao.go
   @Description 描述: 服务AppNamespacedao

   @Author  作者: leijianming@163.com  时间(2024-06-01 09:24:28)
   @Update  作者: leijianming@163.com  时间(2024-06-01 09:24:28)

*/

import (
	//"fmt"

	"gitee.com/ichub/goconfig/common/base/basedto"
	"gitee.com/ichub/goweb/common/basedao"
	"log"
	//"github.com/jinzhu/gorm"
	// "gitee.com/ichub/goconfig/common/base/basemodel"

	"gitee.com/ichub/goweb/common/pagemsg/page"
	//"gitee.com/ichub/goconfig/common/base/baseconsts"
	//"gitee.com/ichub/goconfig/common/dbcontent"
	//"gitee.com/ichub/gocode/db/dto"
	"gitee.com/ichub/gogateway/db/model"
)

var InstAppNamespaceDao AppNamespaceDao

type AppNamespaceDao struct {
	basedao.BaseDao
	basedto.BaseEntitySingle
}

func NewAppNamespaceDao() *AppNamespaceDao {
	return &AppNamespaceDao{}
}

func (this *AppNamespaceDao) FindData(result *basedto.IchubResult) *model.AppNamespace {
	return result.Data.(*model.AppNamespace)
}

func (this *AppNamespaceDao) FindDataList(result *page.PageResult) *[]model.AppNamespace {
	return result.Data.(*[]model.AppNamespace)
}

/*
@title     函数名称:  Insert
@description      :  新增记录
@auth      作者:      leijianming@163.com 时间: 2024-06-01 09:24:28
@param     输入参数名: entity *model.AppNamespace
@return    返回参数名: int32,error
*/
func (this *AppNamespaceDao) Insert(entity *model.AppNamespace) (int32, error) {

	err := this.GetDB().Create(entity).Error
	if err != nil {
		log.Println(err.Error())
		return -1, err
	}

	return entity.Id, err
}

/*
@title     函数名称:  DeleteById
@description      :  根据主键Id 删除记录

@auth      作者:      leijianming@163.com 时间: 2024-06-01 09:24:28
@param     输入参数名: Id int32
@return    返回参数名: error
*/
func (this *AppNamespaceDao) DeleteById(Id int32) error {
	var entity model.AppNamespace

	err := this.GetDB().Where("id=?", Id).Delete(&entity).Error
	return err
}

/*
@title     函数名称:  Save
@description      :  保存记录

@auth      作者:      leijianming@163.com 时间: 2024-06-01 09:24:28
@param     输入参数名: entity *model.AppNamespace
@return    返回参数名: int32,error
*/
func (this *AppNamespaceDao) Save(entity *model.AppNamespace) (int32, error) {

	err := this.GetDB().Save(entity).Error
	if err != nil {
		log.Println(err.Error())
		return -1, err
	}
	return entity.Id, err
}

/*
@title     函数名称:  Update
@description      :  修改记录

@auth      作者:      leijianming@163.com 时间: 2024-06-01 09:24:28
@param     输入参数名: entity *model.AppNamespace
@return    返回参数名: int32,error
*/
func (this *AppNamespaceDao) Update(entity *model.AppNamespace) (int32, error) {

	err := this.GetDB().Model(&model.AppNamespace{}).Where("id=?", entity.Id).Updates(entity).Error
	if err != nil {
		log.Println(err.Error())
		return -1, err
	}
	return entity.Id, err
}

func (this *AppNamespaceDao) UpdateNotNull(Id int32, maps map[string]interface{}) (int32, error) {

	err := this.GetDB().Model(&model.AppNamespace{}).Where("id=?", Id).Updates(maps).Error
	return Id, err
}

/*
@title     函数名称:  UpdateMap
@description      :  根据主键Id,map修改指定字段

@auth      作者:      leijianming@163.com 时间: 2024-06-01 09:24:28
@param     输入参数名: Id int64,entity map[string]interface{}
@return    返回参数名: int64,error
*/
func (this *AppNamespaceDao) UpdateMap(Id int32, maps map[string]interface{}) (int32, error) {

	err := this.GetDB().Model(&model.AppNamespace{}).Where("id=?", Id).Updates(maps).Error
	return Id, err
}

/*
@title     函数名称:  FindById
@description      :  根据主键Id 查询记录

@auth      作者:      leijianming@163.com 时间: 2024-06-01 09:24:28
@param     输入参数名: Id int32
@return    返回参数名: entity *model.AppNamespace, found bool, err error
*/
func (this *AppNamespaceDao) FindById(Id int32) (entity *model.AppNamespace, found bool, err error) {

	entity = new(model.AppNamespace)

	db := this.GetDB().First(entity, Id)
	found = !db.RecordNotFound()
	err = db.Error

	return entity, found, err

}

/*
@title     函数名称:  FindByIds
@description      :  根据主键Id 查询多条记录; FindByIds("1,36,39")

@auth      作者:      leijianming@163.com 时间: 2024-06-01 09:24:28
@param     输入参数名: pks string
@return    返回参数名: *[]model.AppNamespace,error
*/
func (this *AppNamespaceDao) FindByIds(pks string) (*[]model.AppNamespace, error) {

	var entities []model.AppNamespace
	db := this.GetDB().Raw("select * from app_namespace where id in (" + pks + ")").Scan(&entities)

	return &entities, db.Error
}

/*
@title     函数名称:  Query
@description      :  查询符合条件的记录！通用条件分页

@auth      作者:      leijianming@163.com 时间: 2024-06-01 09:24:28
@param     输入参数名: param *pagedto.IchubPageRequest
@return    返回参数名: *pagedto.IchubPageResult
*/
func (this *AppNamespaceDao) Query(param *page.PageRequest) *page.PageResult {

	var entities []model.AppNamespace
	return param.Query(&model.AppNamespace{}, &entities)
}

/*
@title     函数名称:  Count
@description      :  计算符合条件的记录总数！PageRequest通用条件

@auth      作者:      leijianming@163.com 时间: 2024-06-01 09:24:28
@param     输入参数名: param *pagedto.IchubPageRequest
@return    返回参数名: int, error
*/
func (this *AppNamespaceDao) Count(param *page.PageRequest) (int, error) {

	return param.Count(&model.AppNamespace{})
}
