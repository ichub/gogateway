package dao

import (
	"gitee.com/ichub/goconfig/common/base/baseiface"
	"gitee.com/ichub/goconfig/common/basedi"
	"github.com/sirupsen/logrus"
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: publish_dao_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: LEIJMDAS@163.COM 时间(2024-06-01 10:25:52)
	@Update 作者: LEIJMDAS@163.COM 时间(2024-06-01 10:25:52)

* *********************************************************/

const singleNamePublishDao = "dao.PublishDao"

// init center load
func init() {
	registerBeanPublishDao()
}

// center PublishDao
func registerBeanPublishDao() {
	basedi.RegisterLoadBean(singleNamePublishDao, LoadPublishDao)
}

func FindBeanPublishDao() *PublishDao {
	bean, ok := basedi.FindBean(singleNamePublishDao).(*PublishDao)
	if !ok {
		logrus.Errorf("FindBeanPublishDao: failed to cast bean to *PublishDao")
		return nil
	}
	return bean

}

func LoadPublishDao() baseiface.ISingleton {
	var s = NewPublishDao()
	InjectPublishDao(s)
	return s

}

func InjectPublishDao(s *PublishDao) {

	logrus.Debug("inject")
}
