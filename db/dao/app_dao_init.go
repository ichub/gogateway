package dao

import (
	"gitee.com/ichub/goconfig/common/base/baseiface"
	"gitee.com/ichub/goconfig/common/basedi"
	"github.com/sirupsen/logrus"
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: app_dao_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: LEIJMDAS@163.COM 时间(2024-06-01 10:25:52)
	@Update 作者: LEIJMDAS@163.COM 时间(2024-06-01 10:25:52)

* *********************************************************/

const singleNameAppDao = "dao.AppDao"

// init center load
func init() {
	registerBeanAppDao()
}

// center AppDao
func registerBeanAppDao() {
	basedi.RegisterLoadBean(singleNameAppDao, LoadAppDao)
}

func FindBeanAppDao() *AppDao {
	bean, ok := basedi.FindBean(singleNameAppDao).(*AppDao)
	if !ok {
		logrus.Errorf("FindBeanAppDao: failed to cast bean to *AppDao")
		return nil
	}
	return bean

}

func LoadAppDao() baseiface.ISingleton {
	var s = NewAppDao()
	InjectAppDao(s)
	return s

}

func InjectAppDao(s *AppDao) {

	logrus.Debug("inject")
}
