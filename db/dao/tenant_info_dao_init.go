package dao

import (
	"gitee.com/ichub/goconfig/common/base/baseiface"
	"gitee.com/ichub/goconfig/common/basedi"
	"github.com/sirupsen/logrus"
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: tenant_info_dao_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: LEIJMDAS@163.COM 时间(2024-06-01 10:25:52)
	@Update 作者: LEIJMDAS@163.COM 时间(2024-06-01 10:25:52)

* *********************************************************/

const singleNameTenantInfoDao = "dao.TenantInfoDao"

// init center load
func init() {
	registerBeanTenantInfoDao()
}

// center TenantInfoDao
func registerBeanTenantInfoDao() {
	basedi.RegisterLoadBean(singleNameTenantInfoDao, LoadTenantInfoDao)
}

func FindBeanTenantInfoDao() *TenantInfoDao {
	bean, ok := basedi.FindBean(singleNameTenantInfoDao).(*TenantInfoDao)
	if !ok {
		logrus.Errorf("FindBeanTenantInfoDao: failed to cast bean to *TenantInfoDao")
		return nil
	}
	return bean

}

func LoadTenantInfoDao() baseiface.ISingleton {
	var s = NewTenantInfoDao()
	InjectTenantInfoDao(s)
	return s

}

func InjectTenantInfoDao(s *TenantInfoDao) {

	logrus.Debug("inject")
}
