package dao

import (
	"gitee.com/ichub/goconfig/common/base/baseiface"
	"gitee.com/ichub/goconfig/common/basedi"
	"github.com/sirupsen/logrus"
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: item_dao_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: LEIJMDAS@163.COM 时间(2024-06-01 10:25:52)
	@Update 作者: LEIJMDAS@163.COM 时间(2024-06-01 10:25:52)

* *********************************************************/

const singleNameItemDao = "dao.ItemDao"

// init center load
func init() {
	registerBeanItemDao()
}

// center ItemDao
func registerBeanItemDao() {
	basedi.RegisterLoadBean(singleNameItemDao, LoadItemDao)
}

func FindBeanItemDao() *ItemDao {
	bean, ok := basedi.FindBean(singleNameItemDao).(*ItemDao)
	if !ok {
		logrus.Errorf("FindBeanItemDao: failed to cast bean to *ItemDao")
		return nil
	}
	return bean

}

func LoadItemDao() baseiface.ISingleton {
	var s = NewItemDao()
	InjectItemDao(s)
	return s

}

func InjectItemDao(s *ItemDao) {

	logrus.Debug("inject")
}
