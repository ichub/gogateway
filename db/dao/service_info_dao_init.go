package dao

import (
	"gitee.com/ichub/goconfig/common/base/baseiface"
	"gitee.com/ichub/goconfig/common/basedi"
	"github.com/sirupsen/logrus"
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: service_info_dao_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: LEIJMDAS@163.COM 时间(2024-06-01 10:25:52)
	@Update 作者: LEIJMDAS@163.COM 时间(2024-06-01 10:25:52)

* *********************************************************/

const singleNameServiceInfoDao = "dao.ServiceInfoDao"

// init center load
func init() {
	registerBeanServiceInfoDao()
}

// center ServiceInfoDao
func registerBeanServiceInfoDao() {
	basedi.RegisterLoadBean(singleNameServiceInfoDao, LoadServiceInfoDao)
}

func FindBeanServiceInfoDao() *ServiceInfoDao {
	bean, ok := basedi.FindBean(singleNameServiceInfoDao).(*ServiceInfoDao)
	if !ok {
		logrus.Errorf("FindBeanServiceInfoDao: failed to cast bean to *ServiceInfoDao")
		return nil
	}
	return bean

}

func LoadServiceInfoDao() baseiface.ISingleton {
	var s = NewServiceInfoDao()
	InjectServiceInfoDao(s)
	return s

}

func InjectServiceInfoDao(s *ServiceInfoDao) {

	logrus.Debug("inject")
}
