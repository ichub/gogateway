package dao

import (
	"gitee.com/ichub/goconfig/common/base/baseiface"
	"gitee.com/ichub/goconfig/common/basedi"
	"github.com/sirupsen/logrus"
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: namespace_dao_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: LEIJMDAS@163.COM 时间(2024-06-01 10:25:52)
	@Update 作者: LEIJMDAS@163.COM 时间(2024-06-01 10:25:52)

* *********************************************************/

const singleNameNamespaceDao = "dao.NamespaceDao"

// init center load
func init() {
	registerBeanNamespaceDao()
}

// center NamespaceDao
func registerBeanNamespaceDao() {
	basedi.RegisterLoadBean(singleNameNamespaceDao, LoadNamespaceDao)
}

func FindBeanNamespaceDao() *NamespaceDao {
	bean, ok := basedi.FindBean(singleNameNamespaceDao).(*NamespaceDao)
	if !ok {
		logrus.Errorf("FindBeanNamespaceDao: failed to cast bean to *NamespaceDao")
		return nil
	}
	return bean

}

func LoadNamespaceDao() baseiface.ISingleton {
	var s = NewNamespaceDao()
	InjectNamespaceDao(s)
	return s

}

func InjectNamespaceDao(s *NamespaceDao) {

	logrus.Debug("inject")
}
