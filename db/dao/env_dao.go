package dao

/*
   @Title  文件名称: Envdao.go
   @Description 描述: 服务Envdao

   @Author  作者: leijianming@163.com  时间(2024-06-01 09:24:35)
   @Update  作者: leijianming@163.com  时间(2024-06-01 09:24:35)

*/

import (
	//"fmt"

	"gitee.com/ichub/goconfig/common/base/basedto"
	"gitee.com/ichub/goweb/common/basedao"
	"log"
	//"github.com/jinzhu/gorm"
	// "gitee.com/ichub/goconfig/common/base/basemodel"

	"gitee.com/ichub/goweb/common/pagemsg/page"
	//"gitee.com/ichub/goconfig/common/base/baseconsts"
	//"gitee.com/ichub/goconfig/common/dbcontent"
	//"gitee.com/ichub/gocode/db/dto"
	"gitee.com/ichub/gogateway/db/model"
)

var InstEnvDao EnvDao

type EnvDao struct {
	basedao.BaseDao
	basedto.BaseEntitySingle
}

func NewEnvDao() *EnvDao {
	return &EnvDao{}
}

func (this *EnvDao) FindData(result *basedto.IchubResult) *model.Env {
	return result.Data.(*model.Env)
}

func (this *EnvDao) FindDataList(result *page.PageResult) *[]model.Env {
	return result.Data.(*[]model.Env)
}

/*
@title     函数名称:  Insert
@description      :  新增记录
@auth      作者:      leijianming@163.com 时间: 2024-06-01 09:24:35
@param     输入参数名: entity *model.Env
@return    返回参数名: int32,error
*/
func (this *EnvDao) Insert(entity *model.Env) (int32, error) {

	err := this.GetDB().Create(entity).Error
	if err != nil {
		log.Println(err.Error())
		return -1, err
	}

	return entity.Id, err
}

/*
@title     函数名称:  DeleteById
@description      :  根据主键Id 删除记录

@auth      作者:      leijianming@163.com 时间: 2024-06-01 09:24:35
@param     输入参数名: Id int32
@return    返回参数名: error
*/
func (this *EnvDao) DeleteById(Id int32) error {
	var entity model.Env

	err := this.GetDB().Where(" id=?", Id).Delete(&entity).Error
	return err
}

/*
@title     函数名称:  Save
@description      :  保存记录

@auth      作者:      leijianming@163.com 时间: 2024-06-01 09:24:35
@param     输入参数名: entity *model.Env
@return    返回参数名: int32,error
*/
func (this *EnvDao) Save(entity *model.Env) (int32, error) {

	err := this.GetDB().Save(entity).Error
	if err != nil {
		log.Println(err.Error())
		return -1, err
	}
	return entity.Id, err
}

/*
@title     函数名称:  Update
@description      :  修改记录

@auth      作者:      leijianming@163.com 时间: 2024-06-01 09:24:35
@param     输入参数名: entity *model.Env
@return    返回参数名: int32,error
*/
func (this *EnvDao) Update(entity *model.Env) (int32, error) {

	err := this.GetDB().Model(&model.Env{}).Where(" id=?", entity.Id).Updates(entity).Error
	if err != nil {
		log.Println(err.Error())
		return -1, err
	}
	return entity.Id, err
}

func (this *EnvDao) UpdateNotNull(Id int32, maps map[string]interface{}) (int32, error) {

	err := this.GetDB().Model(&model.Env{}).Where(" id=?", Id).Updates(maps).Error
	return Id, err
}

/*
@title     函数名称:  UpdateMap
@description      :  根据主键Id,map修改指定字段

@auth      作者:      leijianming@163.com 时间: 2024-06-01 09:24:35
@param     输入参数名: Id int64,entity map[string]interface{}
@return    返回参数名: int64,error
*/
func (this *EnvDao) UpdateMap(Id int32, maps map[string]interface{}) (int32, error) {

	err := this.GetDB().Model(&model.Env{}).Where(" id=?", Id).Updates(maps).Error
	return Id, err
}

/*
@title     函数名称:  FindById
@description      :  根据主键Id 查询记录

@auth      作者:      leijianming@163.com 时间: 2024-06-01 09:24:35
@param     输入参数名: Id int32
@return    返回参数名: entity *model.Env, found bool, err error
*/
func (this *EnvDao) FindById(Id int32) (entity *model.Env, found bool, err error) {

	entity = new(model.Env)

	db := this.GetDB().First(entity, Id)
	found = !db.RecordNotFound()
	err = db.Error

	return entity, found, err

}

/*
@title     函数名称:  FindByIds
@description      :  根据主键Id 查询多条记录; FindByIds("1,36,39")

@auth      作者:      leijianming@163.com 时间: 2024-06-01 09:24:35
@param     输入参数名: pks string
@return    返回参数名: *[]model.Env,error
*/
func (this *EnvDao) FindByIds(pks string) (*[]model.Env, error) {

	var entities []model.Env
	db := this.GetDB().Raw("select * from env where  id in (" + pks + ")").Scan(&entities)

	return &entities, db.Error
}

/*
@title     函数名称:  Query
@description      :  查询符合条件的记录！通用条件分页

@auth      作者:      leijianming@163.com 时间: 2024-06-01 09:24:35
@param     输入参数名: param *pagedto.IchubPageRequest
@return    返回参数名: *pagedto.IchubPageResult
*/
func (this *EnvDao) Query(param *page.PageRequest) *page.PageResult {

	var entities []model.Env
	return param.Query(&model.Env{}, &entities)
}

/*
@title     函数名称:  Count
@description      :  计算符合条件的记录总数！PageRequest通用条件

@auth      作者:      leijianming@163.com 时间: 2024-06-01 09:24:35
@param     输入参数名: param *pagedto.IchubPageRequest
@return    返回参数名: int, error
*/
func (this *EnvDao) Count(param *page.PageRequest) (int, error) {

	return param.Count(&model.Env{})
}
