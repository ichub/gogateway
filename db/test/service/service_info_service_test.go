package service

/*
   @Title  文件名称: TestServiceInfoServiceSuite.go
   @Description 描述: 测试服务TestServiceInfoServiceSuite

   @Author  作者: leijianming@163.com 时间: 2024-06-01 09:34:30
   @Update  作者: leijianming@163.com 时间: 2024-06-01 09:34:30

*/

import (
	"fmt"
	"gitee.com/ichub/goconfig/common/base/basedto"
	"gitee.com/ichub/goconfig/common/base/baseutils"
	"gitee.com/ichub/gogateway/db/model"
	"gitee.com/ichub/gogateway/db/service"
	"gitee.com/ichub/goweb/common/basedao"
	"gitee.com/ichub/goweb/common/pagemsg/page"
	"github.com/go-basic/ipv4"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/suite"
	"net"
	"os"
	"strings"
	"testing"
)

type TestServiceInfoServiceSuite struct {
	suite.Suite
	basedao.BaseDao
	service service.ServiceInfoService
}

func TestServiceInfoServiceSuites(t *testing.T) {
	suite.Run(t, new(TestServiceInfoServiceSuite))
}

/*
@title     函数名称: Setup
@description      : 测试套前置脚本
@auth      作者: leijianming@163.com
@date      时间: 2024-06-01 09:34:30
@param     输入参数名:        无
@return    返回参数名:        无
*/
func (this *TestServiceInfoServiceSuite) SetupTest() {

	logrus.Info("setUp all tests")
	// mysql postgres cockroach
}

/*
@title           : 测试套后置脚本
@auth      作者: leijianming@163.com
@description 函数名称: Teardown
@date      时间: 2024-06-01 09:34:30
@param     输入参数名:        无
@return    返回参数名:        无
*/
func (this *TestServiceInfoServiceSuite) TearDownTest() {
	logrus.Info("tearDown all tests")
}

/*
@title     函数名称: Save
@description      : 保存接口
@auth      作者    :  leijianming@163.com
@date      时间    :  2024-06-01 09:34:30
@param     输入参数名:  t *testing.T
@return    返回参数名:  无
*/
func (this *TestServiceInfoServiceSuite) Save() int32 {
	var entity = model.NewServiceInfo()
	entity.Port = 12121
	entity.Ip = "192.168.4.121"
	entity.ServiceName = "www.s.com"

	result := this.service.Save(entity)
	logrus.Info(result.String())
	if result.Code != 200 {
		logrus.Errorf("save error: %s", result.Msg)
		return -1
	}
	return entity.Id
}

/*
@title     函数名称: SaveEntity
@description : 保存接口
@auth      作者    : leijianming@163.com
@date      时间    : 2024-06-01 09:34:30
@param     输入参数名: 无
@return    返回参数名: 无
*/
func (this *TestServiceInfoServiceSuite) SaveEntity(entity *model.ServiceInfo) int32 {

	result := this.service.Save(entity)
	logrus.Info(result)
	if result.Code != 200 {
		logrus.Errorf("save error: %s", result.Msg)
		return -1
	}
	return entity.Id
}

/*
@title     函数名称: UpdateNotNull
@description      : 公共函数：更新功能
@auth      作者     :  leijianming@163.com
@时间: 2024-06-01 09:34:30
@param     输入参数名:  model.ServiceInfo
@return    返回参数名:  pkey int32
*/
func (this *TestServiceInfoServiceSuite) UpdateNotNull(entity *model.ServiceInfo) int32 {

	//result := this.centerservice.UpdateNotNull(&entity)
	//fmt.Println(result.String())
	return entity.Id

}

func (this *TestServiceInfoServiceSuite) Test000_ip() {
	addrs, err := net.InterfaceAddrs()
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	for _, address := range addrs {
		if ipnet, ok := address.(*net.IPNet); ok && !ipnet.IP.IsLoopback() {
			if ipnet.IP.To4() != nil {
				fmt.Println(ipnet.IP.String())
			}
		}
	}
	baseutils.Uuid()
	ip := ipv4.LocalIP()
	fmt.Println(ip)
	ips, _ := ipv4.LocalIPv4s()
	fmt.Println(ips)
}

/*
@title     函数名称: Test001_Save
@description      : 测试接口-保存记录, 主键%s为nil或者为0新增, !=nil修改

@auth      作者    : leijianming@163.com
@date      时间    : 2024-06-01 09:34:30
@param     输入参数名:
@return    返回参数名:  无
*/
func (this *TestServiceInfoServiceSuite) Test001_Save() {
	logrus.Info("start Save ...")
	//pkey := this.Save()
	var entity = model.NewServiceInfo()
	entity.Port = 88
	entity.Ip = ipv4.LocalIP() // "192.168.4.121"
	entity.ServiceName = "www.goplatform.com"
	entity.ServiceId = baseutils.Uuid()
	entity.Version = "v1"

	result := this.service.Save(entity)

	result = this.service.FindById(entity.Id)
	logrus.Info(result)
	this.Equal(200, result.Code)

}

/*
@title     函数名称: Test002_UpdateNotNull
@description      : 测试接口-更新非空字段,条件为主键Id

@auth      作者    :   leijianming@163.com
@时间: 2024-06-01 09:34:30
@param     输入参数名:
@return    返回参数名:  无
*/
func (this *TestServiceInfoServiceSuite) Test002_UpdateNotNull() {
	logrus.Info("start UpdateNotNull ...")

	entity := &model.ServiceInfo{}
	pkey := this.SaveEntity(entity)
	result := this.service.FindById(pkey)

	if result.Code != 200 {
		logrus.Println(result.String())
		logrus.Errorf(result.Msg)
	}

	entity = this.service.FindData(result)
	entity.Id = pkey
	// entity.WorkId = fmt.Sprintf("Name%d", time.Now().Second())
	pkey = this.UpdateNotNull(entity)
	logrus.Infof("%d", pkey)

	findResult := this.service.FindById(pkey)
	var retenity = this.service.FindData(findResult)
	//if findResult.Code != 200 || retenity.WorkId != entity.WorkId {
	//	logrus.Errorf("findResult:=" + findResult.String())
	//	logrus.Errorf("UpdateNotNullProp faild! not eq " + entity.WorkId)
	//}
	logrus.Infof("success UpdateNotNull %s", retenity.String())
	//logrus.Infof("success UpdateNotNull pkey = %d, entity.WorkId=%s...", pkey, entity.WorkId)

}

/*
@title     函数名称: Test003_DeleteById
@description      : 测试接口-根据主键Id删除记录
@auth      作者    :   leijianming@163.com
@时间: 2024-06-01 09:34:30
@param     输入参数名:
@return    返回参数名:  无
*/
func (this *TestServiceInfoServiceSuite) Test003_DeleteById() {
	logrus.Info("start DeleteById ...")
	pkey := this.Save()
	result := this.service.FindById(pkey)
	if result.Code != 200 {
		logrus.Errorf("DeleteById no found id:%d!", pkey)
	}
	baseresult := this.service.DeleteById(pkey)
	if baseresult.Code != 200 {
		logrus.Errorf(baseresult.Msg)
	} else {
		logrus.Infof(baseresult.Msg)
	}
	result = this.service.FindById(pkey)

	if result.Code != basedto.CODE_NOFOUND_RECORD {
		logrus.Errorf("Fail to delete!")
	}
	logrus.Infof("success DeleteById %d...", pkey)

}

/*
@title     函数名称: Test004_FindById
@description      : 测试接口-根据主键Id查找记录
@auth      作者    : leijianming@163.com
@date      时间    : 2024-06-01 09:34:30
@param     输入参数名:
@return    返回参数名:  无
*/
func (this *TestServiceInfoServiceSuite) Test004_FindById() {

	logrus.Info("start Test FindById ...")
	entity := model.NewServiceInfo()
	//	entity.DepartmentId = 1
	pkey := this.SaveEntity(entity)

	logrus.Infof("Test save pkey is : %d", pkey)
	result := this.service.FindById(pkey)
	if result.Code != 200 {
		logrus.Errorf(result.String())
	}
	logrus.Info(result.ToString())

	if this.service.FindData(result).Id != pkey {
		logrus.Errorf("Test FindById no found %d!", pkey)
	}

}

/*
@title     函数名称: Test005_FindByIds
@description      : 测试接口-根据主键查询多条记录, FindByIds("1,36,39")
@auth      作者    : leijianming@163.com
@date      时间    : 2024-06-01 09:34:30
@param     输入参数名:
@return    返回参数名:  无
*/
func (this *TestServiceInfoServiceSuite) Test005_FindByIds() {
	logrus.Infof("start FindByIds ...")
	var pkeys []string
	var entity = model.NewServiceInfo()
	//entity.DepartmentId = 1
	pkey := this.SaveEntity(entity)
	pkeys = append(pkeys, fmt.Sprintf("%d", pkey))

	entity = model.NewServiceInfo()
	//entity.DepartmentId = 1
	pkey = this.SaveEntity(entity)
	pkeys = append(pkeys, fmt.Sprintf("%d", pkey))

	result := this.service.FindByIds(strings.Join(pkeys, ","))
	logrus.Info(result.ToPrettyString())
	if result.Total != 2 {
		logrus.Errorf("no found!")
	}
	logrus.Infof("success   FindByIds( %s ) OK! ", strings.Join(pkeys, ","))
}

/*
@title     函数名称: Test006_Query
@description      : 测试接口-通用查询
@auth      作者    :   leijianming@163.com
@时间: 2024-06-01 09:34:30
@param     输入参数名:
@return    返回参数名:  无
*/
func (this *TestServiceInfoServiceSuite) Test006_Query() {
	logrus.Info("start  Query ...")

	pageRequest := page.NewPageRequest(3, 1)

	pageResult := this.service.Query(pageRequest)
	logrus.Info(pageResult)

	logrus.Infof("Query total = %d!", pageResult.Total)

}

/*
@title     函数名称: Test007_JoinTables
@description      : 多表关联实例
@auth      作者    : leijianming@163.com
@时间: 2024-06-01 09:34:30
@param     输入参数名:
@return    返回参数名:       无
*/
func (this *TestServiceInfoServiceSuite) Test007_JoinTables() {
	/* var ms []ResultServiceInfo
		db := this.GetDB().Table("ServiceInfo").Select("ServiceInfo.id,ServiceInfo.name")
		db = db.Joins("JOIN department on department.id = ServiceInfo.departmentId")
		db = db.Order("id desc ").Limit(5).Find(&ms)
		db = this.GetDB().Table("ServiceInfo").Select("ServiceInfo.*").Find(&ms)
	    logrus.Info(db.Error +" " + db.RecordNotFound())

		if db.Error != nil {
	        t.Errorf(db.Error.Error())
	    }
		for _, v := range ms {
			fmt.Println(v.String())
		}
	*/
}
