package dao

/*
   @Title  文件名称: TestAppDaoSuite.go
   @Description 描述: 测试服务TestAppDaoSuite

   @Author  作者: leijianming@163.com 时间: 2024-06-01 09:33:45
   @Update  作者: leijianming@163.com 时间: 2024-06-01 09:33:45

*/

import (
	"fmt"
	"gitee.com/ichub/goweb/common/basedao"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/suite"
	// "gitee.com/ichub/goconfig/common/base/basedto"
	"gitee.com/ichub/goconfig/common/base/baseutils/jsonutils"

	"gitee.com/ichub/gogateway/db/dao"
	"gitee.com/ichub/gogateway/db/model"
	pagedto "gitee.com/ichub/goweb/common/pagemsg/page"
	"strings"
	"testing"
	// "time"
)

type TestAppDaoSuite struct {
	suite.Suite
	basedao.BaseDao
	Dao dao.AppDao
}

func TestAppDaoSuites(t *testing.T) {
	suite.Run(t, new(TestAppDaoSuite))
}

/*
@title     函数名称: Setup
@description      : 测试套前置脚本
@auth      作者: leijianming@163.com
@date      时间: 2024-06-01 09:33:45
@param     输入参数名:        无
@return    返回参数名:        无
*/
func (this *TestAppDaoSuite) SuiteSetupTest() {

	logrus.Info("setUp all tests")
	// mysql postgres cockroach
}

/*
@title           : 测试套后置脚本
@auth      作者: leijianming@163.com
@description 函数名称: Teardown
@date      时间: 2024-06-01 09:33:45
@param     输入参数名:        无
@return    返回参数名:        无
*/
func (this *TestAppDaoSuite) SuiteTearDownTest() {
	logrus.Info("tearDown all tests")
}

/*
@title     函数名称: Save
@description      : 保存接口
@auth      作者    :  leijianming@163.com
@date      时间    :  2024-06-01 09:33:45
@param     输入参数名:  t *testing.T
@return    返回参数名:  无
*/
func (this *TestAppDaoSuite) Save() int32 {
	var entity model.App

	this.Dao.Save(&entity)

	return entity.Id
}

/*
@title     函数名称: SaveEntity
@description : 保存接口
@auth      作者    : leijianming@163.com
@date      时间    : 2024-06-01 09:33:45
@param     输入参数名: 无
@return    返回参数名: 无
*/
func (this *TestAppDaoSuite) SaveEntity(entity *model.App) int32 {

	this.Dao.Save(entity)

	return entity.Id
}

/*
@title     函数名称: UpdateNotNull
@description      : 公共函数：更新功能
@auth      作者     :  leijianming@163.com
@时间: 2024-06-01 09:33:45
@param     输入参数名:  model.App
@return    返回参数名:  pkey int32
*/
func (this *TestAppDaoSuite) UpdateNotNull(entity *model.App) int32 {

	//result := this.Dao.UpdateNotNull(&entity)
	//fmt.Println(result.String())
	return entity.Id

}

/*
@title     函数名称: Test001_Save
@description      : 测试接口-保存记录, 主键%s为nil或者为0新增, !=nil修改

@auth      作者    : leijianming@163.com
@date      时间    : 2024-06-01 09:33:45
@param     输入参数名:
@return    返回参数名:  无
*/
func (this *TestAppDaoSuite) Test001_Save() {
	logrus.Info("start Save ...")
	pkey := this.Save()
	logrus.Infof(fmt.Sprintf("success Save ok! pkey = %d", pkey))

	result, _, _ := this.Dao.FindById(pkey)
	logrus.Infof("result:%s", result)
}

/*
@title     函数名称: Test002_UpdateNotNull
@description      : 测试接口-更新非空字段,条件为主键Id

@auth      作者    :   leijianming@163.com
@时间: 2024-06-01 09:33:45
@param     输入参数名:
@return    返回参数名:  无
*/
func (this *TestAppDaoSuite) Test002_UpdateNotNull() {
	logrus.Info("start UpdateNotNull ...")

	/*entity := &model.App{}
	pkey := this.SaveEntity(entity)
	result := this.Dao.FindById(pkey)

	if result.Code != 200 {
		logrus.Println(result.String())
		logrus.Errorf(result.Msg)
	}

	entity = this.Dao.FindData(result)
	entity.Id = pkey
	pkey = this.UpdateNotNull(entity)
	logrus.Infof("%d", pkey)

	findResult := this.Dao.FindById(pkey)
	var retenity = this.Dao.FindData(findResult)

	logrus.Infof("success UpdateNotNull %s", retenity.String())
	*/
}

/*
@title     函数名称: Test003_DeleteById
@description      : 测试接口-根据主键Id删除记录
@auth      作者    :   leijianming@163.com
@时间: 2024-06-01 09:33:45
@param     输入参数名:
@return    返回参数名:  无
*/
func (this *TestAppDaoSuite) Test003_DeleteById() {
	logrus.Info("start DeleteById ...")
	/*	pkey := this.Save()
		result := this.Dao.FindById(pkey)
		if result.Code != 200 {
			logrus.Errorf("DeleteById no found id:%d!", pkey)
		}
		baseresult := this.Dao.DeleteById(pkey)
		if baseresult.Code != 200 {
			logrus.Errorf(baseresult.Msg)
		} else {
			logrus.Infof(baseresult.Msg)
		}
		result = this.Dao.FindById(pkey)

		if result.Code != basedto.CODE_NOFOUND_RECORD {
			logrus.Errorf("Fail to delete!")
		}
		logrus.Infof("success DeleteById %d...", pkey)
	*/
}

/*
@title     函数名称: Test004_FindById
@description      : 测试接口-根据主键Id查找记录
@auth      作者    : leijianming@163.com
@date      时间    : 2024-06-01 09:33:45
@param     输入参数名:
@return    返回参数名:  无
*/
func (this *TestAppDaoSuite) Test004_FindById() {
	/*
		logrus.Info("start Test FindById ...")
		entity := model.NewApp()
		//	entity.DepartmentId = 1
		pkey := this.SaveEntity(entity)

		logrus.Infof("Test save pkey is : %d", pkey)
		result := this.Dao.FindById(pkey)
		if result.Code != 200 {
			logrus.Errorf(result.String())
		}
		logrus.Info(result.ToString())

		if this.Dao.FindData(result).Id != pkey {
			logrus.Errorf("Test FindById no found %d!", pkey)
		}
	*/
}

/*
@title     函数名称: Test005_FindByIds
@description      : 测试接口-根据主键查询多条记录, FindByIds("1,36,39")
@auth      作者    : leijianming@163.com
@date      时间    : 2024-06-01 09:33:45
@param     输入参数名:
@return    返回参数名:  无
*/
func (this *TestAppDaoSuite) Test005_FindByIds() {
	logrus.Infof("start FindByIds ...")
	var pkeys []string
	var entity = model.NewApp()
	//entity.DepartmentId = 1
	pkey := this.SaveEntity(entity)
	pkeys = append(pkeys, fmt.Sprintf("%d", pkey))

	entity = model.NewApp()
	//entity.DepartmentId = 1
	pkey = this.SaveEntity(entity)
	pkeys = append(pkeys, fmt.Sprintf("%d", pkey))

	result, err := this.Dao.FindByIds(strings.Join(pkeys, ","))
	if err != nil {
		logrus.Error(err)
	}
	logrus.Info("success FindByIds OK! ", jsonutils.ToJsonPretty(result))

}

/*
@title     函数名称: Test006_Query
@description      : 测试接口-通用查询
@auth      作者    :   leijianming@163.com
@时间: 2024-06-01 09:33:45
@param     输入参数名:
@return    返回参数名:  无
*/
func (this *TestAppDaoSuite) Test006_Query() {
	logrus.Info("start  Query ...")

	//entity := model.NewApp()

	//pkey := this.SaveEntity(entity)
	pageRequest := pagedto.Default()
	//pageRequest.Eq("Id", pkey)

	pageRequest.PageCurrent = 1
	pageRequest.PageSize = 3
	logrus.Info(pageRequest)

	pageResult := this.Dao.Query(pageRequest)
	logrus.Info(pageResult)
	if pageResult.Total != 1 {
		logrus.Errorf("未找到记录！")
	}
	for _, record := range *this.Dao.FindDataList(pageResult) {
		logrus.Infof("record=%s", record.String())
	}

	logrus.Infof("Query total = %d!", pageResult.Total)

}

/*
@title     函数名称: Test007_JoinTables
@description      : 多表关联实例
@auth      作者    : leijianming@163.com
@时间: 2024-06-01 09:33:45
@param     输入参数名:
@return    返回参数名:       无
*/
func (this *TestAppDaoSuite) Test007_JoinTables() {
	/* var ms []ResultApp
		db := this.GetDB().Table("App").Select("App.id,App.name")
		db = db.Joins("JOIN department on department.id = App.departmentId")
		db = db.Order("id desc ").Limit(5).Find(&ms)
		db = this.GetDB().Table("App").Select("App.*").Find(&ms)
	    logrus.Info(db.Error +" " + db.RecordNotFound())

		if db.Error != nil {
	        t.Errorf(db.Error.Error())
	    }
		for _, v := range ms {
			fmt.Println(v.String())
		}
	*/
}
