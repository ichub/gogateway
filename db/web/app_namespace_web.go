package web

import (
	"gitee.com/ichub/goconfig/common/base/basedto"
	"gitee.com/ichub/goconfig/common/base/baseutils/stringutils"
	"gitee.com/ichub/gogateway/db/model"
	"gitee.com/ichub/gogateway/db/service"
	"gitee.com/ichub/goweb/common/baseweb"
	"gitee.com/ichub/goweb/common/pagemsg/page"
	"github.com/gin-gonic/gin"
)

/*
@Title  文件名称 : AppNamespaceWeb.go
@Description 描述: WEB服务AppNamespaceWeb
@Author  作者: leijianming@163.com  时间: 2024-06-01 09:24:30
@Update  作者: leijianming@163.com  时间: 2024-06-01 09:24:30
*/
func init() {
	baseweb.Register("AppNamespace", NewAppNamespaceWeb())
}

var InstAppNamespaceWeb baseweb.WebIface = &AppNamespaceWeb{}

func NewAppNamespaceWeb() *AppNamespaceWeb {
	return &AppNamespaceWeb{}
}

//var InstAppNamespaceWeb AppNamespaceWeb

type AppNamespaceWeb struct {
	basedto.BaseEntitySingle
	service *service.AppNamespaceService `godi:"auto"`

	baseweb.BaseWeb
}

func (this *AppNamespaceWeb) Query(ctx *gin.Context) {

	defer this.Fclose(ctx)

	var pageRequest = page.Default()
	err := ctx.BindJSON(pageRequest)
	if err != nil {
		var result = basedto.NewIchubError(basedto.CODE_REQUEST_BAD, "bad request!")
		ctx.JSON(basedto.CODE_SUCCESS, result)
		return
	}

	var result = this.service.Query(pageRequest)
	ctx.Header("Content-Type", "application/json")
	ctx.IndentedJSON(basedto.CODE_SUCCESS, result)
}

func (this *AppNamespaceWeb) FindById(ctx *gin.Context) {

	sid := ctx.DefaultQuery("id", "0")

	ichubResult := this.service.FindById(stringutils.Str2Int32(sid))
	ctx.Header("Content-Type", "application/json")
	ctx.IndentedJSON(basedto.CODE_SUCCESS, ichubResult)

}

func (this *AppNamespaceWeb) DeleteById(ctx *gin.Context) {

	sid := ctx.DefaultQuery("id", "0")
	ichubResult := this.service.DeleteById(stringutils.Str2Int32(sid))
	ctx.Header("Content-Type", "application/json")
	ctx.IndentedJSON(basedto.CODE_SUCCESS, ichubResult)

}

/*
@title     函数名称: Save
@description      : 保存接口
@auth      作者    :   leijianming@163.com 时间: 2024-03-26 19:49:32
@param     输入参数名:  ctx *gin.Context
@return    返回参数名:  无
*/
func (this *AppNamespaceWeb) Save(ctx *gin.Context) {
	defer this.Fclose(ctx)

	var entity = model.NewAppNamespace()
	err := ctx.BindJSON(entity)
	if err != nil {
		var result = basedto.NewIchubError(basedto.CODE_REQUEST_BAD, "bad request!")
		ctx.JSON(basedto.CODE_SUCCESS, result)
		return
	}
	ichubResult := this.service.Save(entity)

	ctx.Header("Content-Type", "application/json")
	ctx.IndentedJSON(basedto.CODE_SUCCESS, ichubResult)

}

func (this *AppNamespaceWeb) UpdateNotNull(ctx *gin.Context) {
	defer this.Fclose(ctx)

	var entity = model.NewAppNamespace()
	var err = ctx.BindJSON(entity)
	if err != nil {
		var result = basedto.NewIchubError(basedto.CODE_REQUEST_BAD, "bad request!")
		ctx.JSON(basedto.CODE_SUCCESS, result)
		return
	}
	ctx.IndentedJSON(basedto.CODE_SUCCESS, entity)
}
