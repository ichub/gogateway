package web

import (
	"gitee.com/ichub/goconfig/common/base/baseiface"
	"gitee.com/ichub/goconfig/common/basedi"
	"gitee.com/ichub/gogateway/db/service"
	"github.com/sirupsen/logrus"
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: service_info_web_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: LEIJMDAS@163.COM 时间(2024-06-01 10:25:52)
	@Update 作者: LEIJMDAS@163.COM 时间(2024-06-01 10:25:52)

* *********************************************************/

const singleNameServiceInfoWeb = "server.ServiceInfoWeb"

// init center load
func init() {
	registerBeanServiceInfoWeb()
}

// center ServiceInfoWeb
func registerBeanServiceInfoWeb() {
	basedi.RegisterLoadBean(singleNameServiceInfoWeb, LoadServiceInfoWeb)
}

func FindBeanServiceInfoWeb() *ServiceInfoWeb {
	bean, ok := basedi.FindBean(singleNameServiceInfoWeb).(*ServiceInfoWeb)
	if !ok {
		logrus.Errorf("FindBeanServiceInfoWeb: failed to cast bean to *ServiceInfoWeb")
		return nil
	}
	return bean

}

func LoadServiceInfoWeb() baseiface.ISingleton {
	var s = NewServiceInfoWeb()
	InjectServiceInfoWeb(s)
	return s

}

func InjectServiceInfoWeb(s *ServiceInfoWeb) {

	// 自动实例化
	s.service = service.FindBeanServiceInfoService()
	logrus.Debug("inject")
}
