package web

import (
	"gitee.com/ichub/goconfig/common/base/baseiface"
	"gitee.com/ichub/goconfig/common/basedi"
	"gitee.com/ichub/gogateway/db/service"
	"github.com/sirupsen/logrus"
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: publish_web_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: LEIJMDAS@163.COM 时间(2024-06-01 10:25:52)
	@Update 作者: LEIJMDAS@163.COM 时间(2024-06-01 10:25:52)

* *********************************************************/

const singleNamePublishWeb = "server.PublishWeb"

// init center load
func init() {
	registerBeanPublishWeb()
}

// center PublishWeb
func registerBeanPublishWeb() {
	basedi.RegisterLoadBean(singleNamePublishWeb, LoadPublishWeb)
}

func FindBeanPublishWeb() *PublishWeb {
	bean, ok := basedi.FindBean(singleNamePublishWeb).(*PublishWeb)
	if !ok {
		logrus.Errorf("FindBeanPublishWeb: failed to cast bean to *PublishWeb")
		return nil
	}
	return bean

}

func LoadPublishWeb() baseiface.ISingleton {
	var s = NewPublishWeb()
	InjectPublishWeb(s)
	return s

}

func InjectPublishWeb(s *PublishWeb) {

	// 自动实例化
	s.service = service.FindBeanPublishService()
	logrus.Debug("inject")
}
