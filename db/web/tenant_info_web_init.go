package web

import (
	"gitee.com/ichub/goconfig/common/base/baseiface"
	"gitee.com/ichub/goconfig/common/basedi"
	"gitee.com/ichub/gogateway/db/service"
	"github.com/sirupsen/logrus"
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: tenant_info_web_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: LEIJMDAS@163.COM 时间(2024-06-01 10:25:52)
	@Update 作者: LEIJMDAS@163.COM 时间(2024-06-01 10:25:52)

* *********************************************************/

const singleNameTenantInfoWeb = "server.TenantInfoWeb"

// init center load
func init() {
	registerBeanTenantInfoWeb()
}

// center TenantInfoWeb
func registerBeanTenantInfoWeb() {
	basedi.RegisterLoadBean(singleNameTenantInfoWeb, LoadTenantInfoWeb)
}

func FindBeanTenantInfoWeb() *TenantInfoWeb {
	bean, ok := basedi.FindBean(singleNameTenantInfoWeb).(*TenantInfoWeb)
	if !ok {
		logrus.Errorf("FindBeanTenantInfoWeb: failed to cast bean to *TenantInfoWeb")
		return nil
	}
	return bean

}

func LoadTenantInfoWeb() baseiface.ISingleton {
	var s = NewTenantInfoWeb()
	InjectTenantInfoWeb(s)
	return s

}

func InjectTenantInfoWeb(s *TenantInfoWeb) {

	// 自动实例化
	s.service = service.FindBeanTenantInfoService()
	logrus.Debug("inject")
}
