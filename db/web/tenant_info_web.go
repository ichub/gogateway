package web

import (
	"gitee.com/ichub/goconfig/common/base/basedto"
	"gitee.com/ichub/goconfig/common/base/baseutils/stringutils"
	"gitee.com/ichub/gogateway/db/model"
	"gitee.com/ichub/gogateway/db/service"
	"gitee.com/ichub/goweb/common/baseweb"
	"gitee.com/ichub/goweb/common/pagemsg/page"
	"github.com/gin-gonic/gin"
)

/*
@Title  文件名称 : TenantInfoWeb.go
@Description 描述: WEB服务TenantInfoWeb
@Author  作者: leijianming@163.com  时间: 2024-06-01 09:25:15
@Update  作者: leijianming@163.com  时间: 2024-06-01 09:25:15
*/
func init() {
	baseweb.Register("TenantInfo", NewTenantInfoWeb())
}

var InstTenantInfoWeb baseweb.WebIface = &TenantInfoWeb{}

func NewTenantInfoWeb() *TenantInfoWeb {
	return &TenantInfoWeb{}
}

//var InstTenantInfoWeb TenantInfoWeb

type TenantInfoWeb struct {
	basedto.BaseEntitySingle
	service *service.TenantInfoService `godi:"auto"`

	baseweb.BaseWeb
}

func (this *TenantInfoWeb) Query(ctx *gin.Context) {

	defer this.Fclose(ctx)

	var pageRequest = page.Default()
	err := ctx.BindJSON(pageRequest)
	if err != nil {
		var result = basedto.NewIchubError(basedto.CODE_REQUEST_BAD, "bad request!")
		ctx.JSON(basedto.CODE_SUCCESS, result)
		return
	}

	var result = this.service.Query(pageRequest)
	ctx.Header("Content-Type", "application/json")
	ctx.IndentedJSON(basedto.CODE_SUCCESS, result)
}

func (this *TenantInfoWeb) FindById(ctx *gin.Context) {

	sid := ctx.DefaultQuery("id", "0")

	ichubResult := this.service.FindById(stringutils.Str2Int64(sid))
	ctx.Header("Content-Type", "application/json")
	ctx.IndentedJSON(basedto.CODE_SUCCESS, ichubResult)

}

func (this *TenantInfoWeb) DeleteById(ctx *gin.Context) {

	sid := ctx.DefaultQuery("id", "0")
	ichubResult := this.service.DeleteById(stringutils.Str2Int64(sid))
	ctx.Header("Content-Type", "application/json")
	ctx.IndentedJSON(basedto.CODE_SUCCESS, ichubResult)

}

/*
@title     函数名称: Save
@description      : 保存接口
@auth      作者    :   leijianming@163.com 时间: 2024-03-26 19:49:32
@param     输入参数名:  ctx *gin.Context
@return    返回参数名:  无
*/
func (this *TenantInfoWeb) Save(ctx *gin.Context) {
	defer this.Fclose(ctx)

	var entity = model.NewTenantInfo()
	err := ctx.BindJSON(entity)
	if err != nil {
		var result = basedto.NewIchubError(basedto.CODE_REQUEST_BAD, "bad request!")
		ctx.JSON(basedto.CODE_SUCCESS, result)
		return
	}
	ichubResult := this.service.Save(entity)

	ctx.Header("Content-Type", "application/json")
	ctx.IndentedJSON(basedto.CODE_SUCCESS, ichubResult)

}

func (this *TenantInfoWeb) UpdateNotNull(ctx *gin.Context) {
	defer this.Fclose(ctx)

	var entity = model.NewTenantInfo()
	var err = ctx.BindJSON(entity)
	if err != nil {
		var result = basedto.NewIchubError(basedto.CODE_REQUEST_BAD, "bad request!")
		ctx.JSON(basedto.CODE_SUCCESS, result)
		return
	}
	ctx.IndentedJSON(basedto.CODE_SUCCESS, entity)
}
