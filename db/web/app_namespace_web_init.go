package web

import (
	"gitee.com/ichub/goconfig/common/base/baseiface"
	"gitee.com/ichub/goconfig/common/basedi"
	"gitee.com/ichub/gogateway/db/service"
	"github.com/sirupsen/logrus"
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: app_namespace_web_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: LEIJMDAS@163.COM 时间(2024-06-01 10:25:52)
	@Update 作者: LEIJMDAS@163.COM 时间(2024-06-01 10:25:52)

* *********************************************************/

const singleNameAppNamespaceWeb = "server.AppNamespaceWeb"

// init center load
func init() {
	registerBeanAppNamespaceWeb()
}

// center AppNamespaceWeb
func registerBeanAppNamespaceWeb() {
	basedi.RegisterLoadBean(singleNameAppNamespaceWeb, LoadAppNamespaceWeb)
}

func FindBeanAppNamespaceWeb() *AppNamespaceWeb {
	bean, ok := basedi.FindBean(singleNameAppNamespaceWeb).(*AppNamespaceWeb)
	if !ok {
		logrus.Errorf("FindBeanAppNamespaceWeb: failed to cast bean to *AppNamespaceWeb")
		return nil
	}
	return bean

}

func LoadAppNamespaceWeb() baseiface.ISingleton {
	var s = NewAppNamespaceWeb()
	InjectAppNamespaceWeb(s)
	return s

}

func InjectAppNamespaceWeb(s *AppNamespaceWeb) {

	// 自动实例化
	s.service = service.FindBeanAppNamespaceService()
	logrus.Debug("inject")
}
