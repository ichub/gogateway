package web

import (
	"gitee.com/ichub/goconfig/common/base/baseiface"
	"gitee.com/ichub/goconfig/common/basedi"
	"gitee.com/ichub/gogateway/db/service"
	"github.com/sirupsen/logrus"
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: namespace_web_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: LEIJMDAS@163.COM 时间(2024-06-01 10:25:52)
	@Update 作者: LEIJMDAS@163.COM 时间(2024-06-01 10:25:52)

* *********************************************************/

const singleNameNamespaceWeb = "server.NamespaceWeb"

// init center load
func init() {
	registerBeanNamespaceWeb()
}

// center NamespaceWeb
func registerBeanNamespaceWeb() {
	basedi.RegisterLoadBean(singleNameNamespaceWeb, LoadNamespaceWeb)
}

func FindBeanNamespaceWeb() *NamespaceWeb {
	bean, ok := basedi.FindBean(singleNameNamespaceWeb).(*NamespaceWeb)
	if !ok {
		logrus.Errorf("FindBeanNamespaceWeb: failed to cast bean to *NamespaceWeb")
		return nil
	}
	return bean

}

func LoadNamespaceWeb() baseiface.ISingleton {
	var s = NewNamespaceWeb()
	InjectNamespaceWeb(s)
	return s

}

func InjectNamespaceWeb(s *NamespaceWeb) {

	// 自动实例化
	s.service = service.FindBeanNamespaceService()
	logrus.Debug("inject")
}
