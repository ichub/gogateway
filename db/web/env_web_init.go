package web

import (
	"gitee.com/ichub/goconfig/common/base/baseiface"
	"gitee.com/ichub/goconfig/common/basedi"
	"gitee.com/ichub/gogateway/db/service"
	"github.com/sirupsen/logrus"
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: env_web_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: LEIJMDAS@163.COM 时间(2024-06-01 10:25:52)
	@Update 作者: LEIJMDAS@163.COM 时间(2024-06-01 10:25:52)

* *********************************************************/

const singleNameEnvWeb = "server.EnvWeb"

// init center load
func init() {
	registerBeanEnvWeb()
}

// center EnvWeb
func registerBeanEnvWeb() {
	basedi.RegisterLoadBean(singleNameEnvWeb, LoadEnvWeb)
}

func FindBeanEnvWeb() *EnvWeb {
	bean, ok := basedi.FindBean(singleNameEnvWeb).(*EnvWeb)
	if !ok {
		logrus.Errorf("FindBeanEnvWeb: failed to cast bean to *EnvWeb")
		return nil
	}
	return bean

}

func LoadEnvWeb() baseiface.ISingleton {
	var s = NewEnvWeb()
	InjectEnvWeb(s)
	return s

}

func InjectEnvWeb(s *EnvWeb) {

	// 自动实例化
	s.service = service.FindBeanEnvService()
	logrus.Debug("inject")
}
