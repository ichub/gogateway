package web

import (
	"gitee.com/ichub/goconfig/common/base/baseiface"
	"gitee.com/ichub/goconfig/common/basedi"
	"gitee.com/ichub/gogateway/db/service"
	"github.com/sirupsen/logrus"
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: instance_web_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: LEIJMDAS@163.COM 时间(2024-06-01 10:25:52)
	@Update 作者: LEIJMDAS@163.COM 时间(2024-06-01 10:25:52)

* *********************************************************/

const singleNameInstanceWeb = "server.InstanceWeb"

// init center load
func init() {
	registerBeanInstanceWeb()
}

// center InstanceWeb
func registerBeanInstanceWeb() {
	basedi.RegisterLoadBean(singleNameInstanceWeb, LoadInstanceWeb)
}

func FindBeanInstanceWeb() *InstanceWeb {
	bean, ok := basedi.FindBean(singleNameInstanceWeb).(*InstanceWeb)
	if !ok {
		logrus.Errorf("FindBeanInstanceWeb: failed to cast bean to *InstanceWeb")
		return nil
	}
	return bean

}

func LoadInstanceWeb() baseiface.ISingleton {
	var s = NewInstanceWeb()
	InjectInstanceWeb(s)
	return s

}

func InjectInstanceWeb(s *InstanceWeb) {

	// 自动实例化
	s.service = service.FindBeanInstanceService()
	logrus.Debug("inject")
}
