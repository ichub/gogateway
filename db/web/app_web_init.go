package web

import (
	"gitee.com/ichub/goconfig/common/base/baseiface"
	"gitee.com/ichub/goconfig/common/basedi"
	"gitee.com/ichub/gogateway/db/service"
	"github.com/sirupsen/logrus"
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: app_web_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: LEIJMDAS@163.COM 时间(2024-06-01 10:25:52)
	@Update 作者: LEIJMDAS@163.COM 时间(2024-06-01 10:25:52)

* *********************************************************/

const singleNameAppWeb = "server.AppWeb"

// init center load
func init() {
	registerBeanAppWeb()
}

// center AppWeb
func registerBeanAppWeb() {
	basedi.RegisterLoadBean(singleNameAppWeb, LoadAppWeb)
}

func FindBeanAppWeb() *AppWeb {
	bean, ok := basedi.FindBean(singleNameAppWeb).(*AppWeb)
	if !ok {
		logrus.Errorf("FindBeanAppWeb: failed to cast bean to *AppWeb")
		return nil
	}
	return bean

}

func LoadAppWeb() baseiface.ISingleton {
	var s = NewAppWeb()
	InjectAppWeb(s)
	return s

}

func InjectAppWeb(s *AppWeb) {

	// 自动实例化
	s.service = service.FindBeanAppService()
	logrus.Debug("inject")
}
