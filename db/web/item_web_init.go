package web

import (
	"gitee.com/ichub/goconfig/common/base/baseiface"
	"gitee.com/ichub/goconfig/common/basedi"
	"gitee.com/ichub/gogateway/db/service"
	"github.com/sirupsen/logrus"
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: item_web_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: LEIJMDAS@163.COM 时间(2024-06-01 10:25:52)
	@Update 作者: LEIJMDAS@163.COM 时间(2024-06-01 10:25:52)

* *********************************************************/

const singleNameItemWeb = "server.ItemWeb"

// init center load
func init() {
	registerBeanItemWeb()
}

// center ItemWeb
func registerBeanItemWeb() {
	basedi.RegisterLoadBean(singleNameItemWeb, LoadItemWeb)
}

func FindBeanItemWeb() *ItemWeb {
	bean, ok := basedi.FindBean(singleNameItemWeb).(*ItemWeb)
	if !ok {
		logrus.Errorf("FindBeanItemWeb: failed to cast bean to *ItemWeb")
		return nil
	}
	return bean

}

func LoadItemWeb() baseiface.ISingleton {
	var s = NewItemWeb()
	InjectItemWeb(s)
	return s

}

func InjectItemWeb(s *ItemWeb) {

	// 自动实例化
	s.service = service.FindBeanItemService()
	logrus.Debug("inject")
}
