package service

import (
	"gitee.com/ichub/goconfig/common/base/baseiface"
	"gitee.com/ichub/goconfig/common/basedi"
	"github.com/sirupsen/logrus"
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: instance_service_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: LEIJMDAS@163.COM 时间(2024-06-01 10:25:52)
	@Update 作者: LEIJMDAS@163.COM 时间(2024-06-01 10:25:52)

* *********************************************************/

const singleNameInstanceService = "centerservice.InstanceService"

// init center load
func init() {
	registerBeanInstanceService()
}

// center InstanceService
func registerBeanInstanceService() {
	basedi.RegisterLoadBean(singleNameInstanceService, LoadInstanceService)
}

func FindBeanInstanceService() *InstanceService {
	bean, ok := basedi.FindBean(singleNameInstanceService).(*InstanceService)
	if !ok {
		logrus.Errorf("FindBeanInstanceService: failed to cast bean to *InstanceService")
		return nil
	}
	return bean

}

func LoadInstanceService() baseiface.ISingleton {
	var s = NewInstanceService()
	InjectInstanceService(s)
	return s

}

func InjectInstanceService(s *InstanceService) {

	logrus.Debug("inject")
}
