package service

import (
	"gitee.com/ichub/goconfig/common/base/baseiface"
	"gitee.com/ichub/goconfig/common/basedi"
	"github.com/sirupsen/logrus"
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: tenant_info_service_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: LEIJMDAS@163.COM 时间(2024-06-01 10:25:52)
	@Update 作者: LEIJMDAS@163.COM 时间(2024-06-01 10:25:52)

* *********************************************************/

const singleNameTenantInfoService = "centerservice.TenantInfoService"

// init center load
func init() {
	registerBeanTenantInfoService()
}

// center TenantInfoService
func registerBeanTenantInfoService() {
	basedi.RegisterLoadBean(singleNameTenantInfoService, LoadTenantInfoService)
}

func FindBeanTenantInfoService() *TenantInfoService {
	bean, ok := basedi.FindBean(singleNameTenantInfoService).(*TenantInfoService)
	if !ok {
		logrus.Errorf("FindBeanTenantInfoService: failed to cast bean to *TenantInfoService")
		return nil
	}
	return bean

}

func LoadTenantInfoService() baseiface.ISingleton {
	var s = NewTenantInfoService()
	InjectTenantInfoService(s)
	return s

}

func InjectTenantInfoService(s *TenantInfoService) {

	logrus.Debug("inject")
}
