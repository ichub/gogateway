package service

import (
	"gitee.com/ichub/goconfig/common/base/baseiface"
	"gitee.com/ichub/goconfig/common/basedi"
	"github.com/sirupsen/logrus"
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: item_service_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: LEIJMDAS@163.COM 时间(2024-06-01 10:25:52)
	@Update 作者: LEIJMDAS@163.COM 时间(2024-06-01 10:25:52)

* *********************************************************/

const singleNameItemService = "centerservice.ItemService"

// init center load
func init() {
	registerBeanItemService()
}

// center ItemService
func registerBeanItemService() {
	basedi.RegisterLoadBean(singleNameItemService, LoadItemService)
}

func FindBeanItemService() *ItemService {
	bean, ok := basedi.FindBean(singleNameItemService).(*ItemService)
	if !ok {
		logrus.Errorf("FindBeanItemService: failed to cast bean to *ItemService")
		return nil
	}
	return bean

}

func LoadItemService() baseiface.ISingleton {
	var s = NewItemService()
	InjectItemService(s)
	return s

}

func InjectItemService(s *ItemService) {

	logrus.Debug("inject")
}
