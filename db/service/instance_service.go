package service

/*
   @Title  文件名称 : Instanceservice.go
   @Description 描述: 服务Instanceservice

   @Author  作者: leijianming@163.com  时间: 2024-06-01 09:24:42
   @Update  作者: leijianming@163.com  时间: 2024-06-01 09:24:42

*/

import (
	"fmt"
	"gitee.com/ichub/goconfig/common/base/basedto"
	"gitee.com/ichub/gogateway/db/dao"
	"gitee.com/ichub/gogateway/db/model"
	"gitee.com/ichub/goweb/common/pagemsg/page"
)

// SERVICE层服务实例变量
var InstInstanceService = NewInstanceService()

// SERVICE层服务结构体
type InstanceService struct {
	basedto.BaseEntitySingle

	Dao dao.InstanceDao // `godi:"auto"`
}

func NewInstanceService() *InstanceService {
	return &InstanceService{}
}

func (this *InstanceService) FindData(result *basedto.IchubResult) *model.Instance {
	return result.Data.(*model.Instance)
}

func (this *InstanceService) FindDataList(result *page.PageResult) *[]model.Instance {
	if result.Data == nil {
		var t = make([]model.Instance, 0)
		return &t
	}
	return result.Data.(*[]model.Instance)
}

/*
@title     函数名称:  Query
@description      :  通用查询
@auth      作者:      leijianming@163.com时间: 2024-03-26 12:46:24
@param     输入参数名: param * page.PageRequest
@return    返回参数名: * page.PageResult
*/
func (this *InstanceService) Query(param *page.PageRequest) *page.PageResult {

	return this.Dao.Query(param)
	//	this.fills(entities)
}

/*
   @title     函数名称 :  Count
   @description       :  通用查询计数
   @auth      作者     : leijianming@163.com时间: 2024-03-26 12:46:24
   @param     输入参数名: param * page.PageRequest
   @return    返回参数名: int  ,error
*/

func (this *InstanceService) Count(param *page.PageRequest) (int, error) {
	return this.Dao.Count(param)
}

/*
@title     函数名称: fills
@description      : 填充关联子表信息。
@auth      作者   : leijianming@163.com
@时间: 2024-06-01 09:24:42
@param     输入参数名: entity *model.Instance
@return    返回参数名: 无
*/
func (this *InstanceService) fills(entities *[]model.Instance) {

	for index := range *entities {

		this.fill(&(*entities)[index])
	}

}

/*
@title     函数名称: fill
@description      : 填充关联子表信息。
@auth      作者   : leijianming@163.com
@时间: 2024-06-01 09:24:42
@param     输入参数名: entity *model.Instance
@return    返回参数名: 无
*/
func (this *InstanceService) fill(entity *model.Instance) {

}

/*
@title     函数名称: FindById(Id int32)
@description      : 根据主键查询记录
@auth      作者:     leijianming@163.com
@时间: 2024-06-01 09:24:42
@param     输入参数名:Id int32
@return    返回参数名:*basedto.IchubResult
*/
func (this *InstanceService) FindById(Id int32) *basedto.IchubResult {

	var result = basedto.NewIchubResult()
	entity, found, err := this.Dao.FindById(Id)

	if err != nil || !found {
		result.CodeMsg(basedto.CODE_NOFOUND_RECORD, err.Error())
		return result
	}

	result.Data = entity
	return result

}

/*
@title     函数名称: FindByIds(pks string)
@description      :

	根据主键Id  查询多条记录
	例子： FindByIds("1,36,39")

@auth      作者: leijianming@163.com
@date:     时间: 2024-06-01 09:24:42
@param     输入参数名:Id int32
@return    返回参数名:*pagedto.IchubPageResult
*/
func (this *InstanceService) FindByIds(pks string) *page.PageResult {
	var result = page.NewIchubPageResult()
	entities, err := this.Dao.FindByIds(pks)
	if err != nil {
		return result.FailMsg(err.Error())
	}

	result.Data = entities
	return result
}

/*
@title     函数名称: DeleteById
@description      : 根据主键软删除。
@auth      作者   : leijianming@163.com 时间: 2024-06-01 09:24:42
@param     输入参数名: Id int32
@return    返回参数名: *basedto.IchubResult
*/
func (this *InstanceService) DeleteById(Id int32) *basedto.IchubResult {
	err := this.Dao.DeleteById(Id)
	var result = basedto.NewIchubResult()
	if err != nil {
		return result.FailMessage(err.Error())

	}

	return result
}

/*
@title     函数名称: Save
@description      :
@auth      作者   : leijianming@163.com 时间: 2024-06-01 09:24:42
@param     输入参数名: entity *model.Instance
@return    返回参数名: *basedto.IchubResult
*/
func (this *InstanceService) Save(entity *model.Instance) *basedto.IchubResult {

	var e error
	if entity.Id == 0 {
		_, e = this.Dao.Insert(entity)
	} else {
		_, e = this.Dao.Update(entity)
	}
	var result = basedto.NewIchubResult()
	if e != nil {
		return result.FailMessage(e.Error())
	}

	result.Data = fmt.Sprintf("%d", entity.Id)
	return result
}

/*
@title     函数名称: UpdateNotNull
@description      : 更新非空字段
@auth      作者   : leijianming@163.com 时间: 2024-06-01 09:24:42
@param     输入参数名: entity *model.Instance
@return    返回参数名: *basedto.IchubResult
*/
func (this *InstanceService) UpdateNotNull(Id int32, entity map[string]interface{}) *basedto.IchubResult {

	var result = basedto.NewIchubResult()
	_, err := this.Dao.UpdateNotNull(Id, entity)

	if err != nil {
		return result.FailMessage(err.Error())

	}
	return result

}

// End of  InstanceService
