package service

import (
	"gitee.com/ichub/goconfig/common/base/baseiface"
	"gitee.com/ichub/goconfig/common/basedi"
	"github.com/sirupsen/logrus"
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: env_service_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: LEIJMDAS@163.COM 时间(2024-06-01 10:25:52)
	@Update 作者: LEIJMDAS@163.COM 时间(2024-06-01 10:25:52)

* *********************************************************/

const singleNameEnvService = "centerservice.EnvService"

// init center load
func init() {
	registerBeanEnvService()
}

// center EnvService
func registerBeanEnvService() {
	basedi.RegisterLoadBean(singleNameEnvService, LoadEnvService)
}

func FindBeanEnvService() *EnvService {
	bean, ok := basedi.FindBean(singleNameEnvService).(*EnvService)
	if !ok {
		logrus.Errorf("FindBeanEnvService: failed to cast bean to *EnvService")
		return nil
	}
	return bean

}

func LoadEnvService() baseiface.ISingleton {
	var s = NewEnvService()
	InjectEnvService(s)
	return s

}

func InjectEnvService(s *EnvService) {

	logrus.Debug("inject")
}
