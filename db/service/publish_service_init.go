package service

import (
	"gitee.com/ichub/goconfig/common/base/baseiface"
	"gitee.com/ichub/goconfig/common/basedi"
	"github.com/sirupsen/logrus"
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: publish_service_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: LEIJMDAS@163.COM 时间(2024-06-01 10:25:52)
	@Update 作者: LEIJMDAS@163.COM 时间(2024-06-01 10:25:52)

* *********************************************************/

const singleNamePublishService = "centerservice.PublishService"

// init center load
func init() {
	registerBeanPublishService()
}

// center PublishService
func registerBeanPublishService() {
	basedi.RegisterLoadBean(singleNamePublishService, LoadPublishService)
}

func FindBeanPublishService() *PublishService {
	bean, ok := basedi.FindBean(singleNamePublishService).(*PublishService)
	if !ok {
		logrus.Errorf("FindBeanPublishService: failed to cast bean to *PublishService")
		return nil
	}
	return bean

}

func LoadPublishService() baseiface.ISingleton {
	var s = NewPublishService()
	InjectPublishService(s)
	return s

}

func InjectPublishService(s *PublishService) {

	logrus.Debug("inject")
}
