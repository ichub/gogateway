package service

import (
	"gitee.com/ichub/goconfig/common/base/baseiface"
	"gitee.com/ichub/goconfig/common/basedi"
	"github.com/sirupsen/logrus"
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: app_namespace_service_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: LEIJMDAS@163.COM 时间(2024-06-01 10:25:52)
	@Update 作者: LEIJMDAS@163.COM 时间(2024-06-01 10:25:52)

* *********************************************************/

const singleNameAppNamespaceService = "centerservice.AppNamespaceService"

// init center load
func init() {
	registerBeanAppNamespaceService()
}

// center AppNamespaceService
func registerBeanAppNamespaceService() {
	basedi.RegisterLoadBean(singleNameAppNamespaceService, LoadAppNamespaceService)
}

func FindBeanAppNamespaceService() *AppNamespaceService {
	bean, ok := basedi.FindBean(singleNameAppNamespaceService).(*AppNamespaceService)
	if !ok {
		logrus.Errorf("FindBeanAppNamespaceService: failed to cast bean to *AppNamespaceService")
		return nil
	}
	return bean

}

func LoadAppNamespaceService() baseiface.ISingleton {
	var s = NewAppNamespaceService()
	InjectAppNamespaceService(s)
	return s

}

func InjectAppNamespaceService(s *AppNamespaceService) {

	logrus.Debug("inject")
}
