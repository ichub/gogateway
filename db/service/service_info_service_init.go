package service

import (
	"gitee.com/ichub/goconfig/common/base/baseiface"
	"gitee.com/ichub/goconfig/common/basedi"
	"github.com/sirupsen/logrus"
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: service_info_service_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: LEIJMDAS@163.COM 时间(2024-06-01 10:25:52)
	@Update 作者: LEIJMDAS@163.COM 时间(2024-06-01 10:25:52)

* *********************************************************/

const singleNameServiceInfoService = "centerservice.ServiceInfoService"

// init center load
func init() {
	registerBeanServiceInfoService()
}

// center ServiceInfoService
func registerBeanServiceInfoService() {
	basedi.RegisterLoadBean(singleNameServiceInfoService, LoadServiceInfoService)
}

func FindBeanServiceInfoService() *ServiceInfoService {
	bean, ok := basedi.FindBean(singleNameServiceInfoService).(*ServiceInfoService)
	if !ok {
		logrus.Errorf("FindBeanServiceInfoService: failed to cast bean to *ServiceInfoService")
		return nil
	}
	return bean

}

func LoadServiceInfoService() baseiface.ISingleton {
	var s = NewServiceInfoService()
	InjectServiceInfoService(s)
	return s

}

func InjectServiceInfoService(s *ServiceInfoService) {

	logrus.Debug("inject")
}
