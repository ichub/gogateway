package service

import (
	"gitee.com/ichub/goconfig/common/base/baseiface"
	"gitee.com/ichub/goconfig/common/basedi"
	"github.com/sirupsen/logrus"
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: namespace_service_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: LEIJMDAS@163.COM 时间(2024-06-01 10:25:52)
	@Update 作者: LEIJMDAS@163.COM 时间(2024-06-01 10:25:52)

* *********************************************************/

const singleNameNamespaceService = "centerservice.NamespaceService"

// init center load
func init() {
	registerBeanNamespaceService()
}

// center NamespaceService
func registerBeanNamespaceService() {
	basedi.RegisterLoadBean(singleNameNamespaceService, LoadNamespaceService)
}

func FindBeanNamespaceService() *NamespaceService {
	bean, ok := basedi.FindBean(singleNameNamespaceService).(*NamespaceService)
	if !ok {
		logrus.Errorf("FindBeanNamespaceService: failed to cast bean to *NamespaceService")
		return nil
	}
	return bean

}

func LoadNamespaceService() baseiface.ISingleton {
	var s = NewNamespaceService()
	InjectNamespaceService(s)
	return s

}

func InjectNamespaceService(s *NamespaceService) {

	logrus.Debug("inject")
}
