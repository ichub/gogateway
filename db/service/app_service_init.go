package service

import (
	"gitee.com/ichub/goconfig/common/base/baseiface"
	"gitee.com/ichub/goconfig/common/basedi"
	"github.com/sirupsen/logrus"
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: app_service_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: LEIJMDAS@163.COM 时间(2024-06-01 10:25:52)
	@Update 作者: LEIJMDAS@163.COM 时间(2024-06-01 10:25:52)

* *********************************************************/

const singleNameAppService = "centerservice.AppService"

// init center load
func init() {
	registerBeanAppService()
}

// center AppService
func registerBeanAppService() {
	basedi.RegisterLoadBean(singleNameAppService, LoadAppService)
}

func FindBeanAppService() *AppService {
	bean, ok := basedi.FindBean(singleNameAppService).(*AppService)
	if !ok {
		logrus.Errorf("FindBeanAppService: failed to cast bean to *AppService")
		return nil
	}
	return bean

}

func LoadAppService() baseiface.ISingleton {
	var s = NewAppService()
	InjectAppService(s)
	return s

}

func InjectAppService(s *AppService) {

	logrus.Debug("inject")
}
