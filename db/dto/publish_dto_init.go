package dto

import (
	"gitee.com/ichub/goconfig/common/base/baseiface"
	"gitee.com/ichub/goconfig/common/basedi"
	"github.com/sirupsen/logrus"
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: publish_dto_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: LEIJMDAS@163.COM 时间(2024-06-01 10:25:52)
	@Update 作者: LEIJMDAS@163.COM 时间(2024-06-01 10:25:52)

* *********************************************************/

const singleNamePublishDto = "dto.PublishDto"

// init center load
func init() {
	registerBeanPublishDto()
}

// center PublishDto
func registerBeanPublishDto() {
	basedi.RegisterLoadBean(singleNamePublishDto, LoadPublishDto)
}

func FindBeanPublishDto() *PublishDto {
	bean, ok := basedi.FindBean(singleNamePublishDto).(*PublishDto)
	if !ok {
		logrus.Errorf("FindBeanPublishDto: failed to cast bean to *PublishDto")
		return nil
	}
	return bean

}

func LoadPublishDto() baseiface.ISingleton {
	var s = NewPublishDto()
	InjectPublishDto(s)
	return s

}

func InjectPublishDto(s *PublishDto) {

	logrus.Debug("inject")
}
