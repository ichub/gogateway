package dto

import (
	"gitee.com/ichub/goconfig/common/base/baseiface"
	"gitee.com/ichub/goconfig/common/basedi"
	"github.com/sirupsen/logrus"
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: service_info_dto_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: LEIJMDAS@163.COM 时间(2024-06-01 10:25:52)
	@Update 作者: LEIJMDAS@163.COM 时间(2024-06-01 10:25:52)

* *********************************************************/

const singleNameServiceInfoDto = "dto.ServiceInfoDto"

// init center load
func init() {
	registerBeanServiceInfoDto()
}

// center ServiceInfoDto
func registerBeanServiceInfoDto() {
	basedi.RegisterLoadBean(singleNameServiceInfoDto, LoadServiceInfoDto)
}

func FindBeanServiceInfoDto() *ServiceInfoDto {
	bean, ok := basedi.FindBean(singleNameServiceInfoDto).(*ServiceInfoDto)
	if !ok {
		logrus.Errorf("FindBeanServiceInfoDto: failed to cast bean to *ServiceInfoDto")
		return nil
	}
	return bean

}

func LoadServiceInfoDto() baseiface.ISingleton {
	var s = NewServiceInfoDto()
	InjectServiceInfoDto(s)
	return s

}

func InjectServiceInfoDto(s *ServiceInfoDto) {

	logrus.Debug("inject")
}
