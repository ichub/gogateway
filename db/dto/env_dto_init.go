package dto

import (
	"gitee.com/ichub/goconfig/common/base/baseiface"
	"gitee.com/ichub/goconfig/common/basedi"
	"github.com/sirupsen/logrus"
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: env_dto_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: LEIJMDAS@163.COM 时间(2024-06-01 10:25:52)
	@Update 作者: LEIJMDAS@163.COM 时间(2024-06-01 10:25:52)

* *********************************************************/

const singleNameEnvDto = "dto.EnvDto"

// init center load
func init() {
	registerBeanEnvDto()
}

// center EnvDto
func registerBeanEnvDto() {
	basedi.RegisterLoadBean(singleNameEnvDto, LoadEnvDto)
}

func FindBeanEnvDto() *EnvDto {
	bean, ok := basedi.FindBean(singleNameEnvDto).(*EnvDto)
	if !ok {
		logrus.Errorf("FindBeanEnvDto: failed to cast bean to *EnvDto")
		return nil
	}
	return bean

}

func LoadEnvDto() baseiface.ISingleton {
	var s = NewEnvDto()
	InjectEnvDto(s)
	return s

}

func InjectEnvDto(s *EnvDto) {

	logrus.Debug("inject")
}
