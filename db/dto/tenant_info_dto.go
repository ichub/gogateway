package dto

import (
	// "encoding/json"
	"gitee.com/ichub/goconfig/common/base/basedto"
	"gitee.com/ichub/gogateway/db/model"
)

/*
   @Title  文件名称: TenantInfodto.go
   @Description 描述: 服务TenantInfodto

   @Author  作者: leijianming@163.com  时间(2024-06-01 09:25:13)
   @Update  作者: leijianming@163.com  时间(2024-06-01 09:25:13)

*/

/*
tenant_info
*/
type TenantInfoDto struct {
	basedto.BaseEntity
	model.TenantInfo
}

func NewTenantInfoDto() *TenantInfoDto {
	var m = &TenantInfoDto{}
	m.InitProxy(m)
	return m
}

type TenantInfoParamsDto struct {
	model.TenantInfoParams
}
type TenantInfoEntityDto struct {
	model.TenantInfoEntity
}

func NewTenantInfoEntityDto() *TenantInfoEntityDto {
	var m = &TenantInfoEntityDto{}
	//m.InitProxy(m)
	return m
}

func NewTenantInfoParamsDto() *TenantInfoParamsDto {
	var m = &TenantInfoParamsDto{}
	//m.InitProxy(m)
	return m
}
