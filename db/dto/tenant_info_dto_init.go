package dto

import (
	"gitee.com/ichub/goconfig/common/base/baseiface"
	"gitee.com/ichub/goconfig/common/basedi"
	"github.com/sirupsen/logrus"
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: tenant_info_dto_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: LEIJMDAS@163.COM 时间(2024-06-01 10:25:52)
	@Update 作者: LEIJMDAS@163.COM 时间(2024-06-01 10:25:52)

* *********************************************************/

const singleNameTenantInfoDto = "dto.TenantInfoDto"

// init center load
func init() {
	registerBeanTenantInfoDto()
}

// center TenantInfoDto
func registerBeanTenantInfoDto() {
	basedi.RegisterLoadBean(singleNameTenantInfoDto, LoadTenantInfoDto)
}

func FindBeanTenantInfoDto() *TenantInfoDto {
	bean, ok := basedi.FindBean(singleNameTenantInfoDto).(*TenantInfoDto)
	if !ok {
		logrus.Errorf("FindBeanTenantInfoDto: failed to cast bean to *TenantInfoDto")
		return nil
	}
	return bean

}

func LoadTenantInfoDto() baseiface.ISingleton {
	var s = NewTenantInfoDto()
	InjectTenantInfoDto(s)
	return s

}

func InjectTenantInfoDto(s *TenantInfoDto) {

	logrus.Debug("inject")
}
