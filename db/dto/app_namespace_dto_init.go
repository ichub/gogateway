package dto

import (
	"gitee.com/ichub/goconfig/common/base/baseiface"
	"gitee.com/ichub/goconfig/common/basedi"
	"github.com/sirupsen/logrus"
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: app_namespace_dto_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: LEIJMDAS@163.COM 时间(2024-06-01 10:25:52)
	@Update 作者: LEIJMDAS@163.COM 时间(2024-06-01 10:25:52)

* *********************************************************/

const singleNameAppNamespaceDto = "dto.AppNamespaceDto"

// init center load
func init() {
	registerBeanAppNamespaceDto()
}

// center AppNamespaceDto
func registerBeanAppNamespaceDto() {
	basedi.RegisterLoadBean(singleNameAppNamespaceDto, LoadAppNamespaceDto)
}

func FindBeanAppNamespaceDto() *AppNamespaceDto {
	bean, ok := basedi.FindBean(singleNameAppNamespaceDto).(*AppNamespaceDto)
	if !ok {
		logrus.Errorf("FindBeanAppNamespaceDto: failed to cast bean to *AppNamespaceDto")
		return nil
	}
	return bean

}

func LoadAppNamespaceDto() baseiface.ISingleton {
	var s = NewAppNamespaceDto()
	InjectAppNamespaceDto(s)
	return s

}

func InjectAppNamespaceDto(s *AppNamespaceDto) {

	logrus.Debug("inject")
}
