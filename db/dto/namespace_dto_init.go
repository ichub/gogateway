package dto

import (
	"gitee.com/ichub/goconfig/common/base/baseiface"
	"gitee.com/ichub/goconfig/common/basedi"
	"github.com/sirupsen/logrus"
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: namespace_dto_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: LEIJMDAS@163.COM 时间(2024-06-01 10:25:52)
	@Update 作者: LEIJMDAS@163.COM 时间(2024-06-01 10:25:52)

* *********************************************************/

const singleNameNamespaceDto = "dto.NamespaceDto"

// init center load
func init() {
	registerBeanNamespaceDto()
}

// center NamespaceDto
func registerBeanNamespaceDto() {
	basedi.RegisterLoadBean(singleNameNamespaceDto, LoadNamespaceDto)
}

func FindBeanNamespaceDto() *NamespaceDto {
	bean, ok := basedi.FindBean(singleNameNamespaceDto).(*NamespaceDto)
	if !ok {
		logrus.Errorf("FindBeanNamespaceDto: failed to cast bean to *NamespaceDto")
		return nil
	}
	return bean

}

func LoadNamespaceDto() baseiface.ISingleton {
	var s = NewNamespaceDto()
	InjectNamespaceDto(s)
	return s

}

func InjectNamespaceDto(s *NamespaceDto) {

	logrus.Debug("inject")
}
