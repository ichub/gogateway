package dto

import (
	"gitee.com/ichub/goconfig/common/base/baseiface"
	"gitee.com/ichub/goconfig/common/basedi"
	"github.com/sirupsen/logrus"
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: app_dto_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: LEIJMDAS@163.COM 时间(2024-06-01 10:25:52)
	@Update 作者: LEIJMDAS@163.COM 时间(2024-06-01 10:25:52)

* *********************************************************/

const singleNameAppDto = "dto.AppDto"

// init center load
func init() {
	registerBeanAppDto()
}

// center AppDto
func registerBeanAppDto() {
	basedi.RegisterLoadBean(singleNameAppDto, LoadAppDto)
}

func FindBeanAppDto() *AppDto {
	bean, ok := basedi.FindBean(singleNameAppDto).(*AppDto)
	if !ok {
		logrus.Errorf("FindBeanAppDto: failed to cast bean to *AppDto")
		return nil
	}
	return bean

}

func LoadAppDto() baseiface.ISingleton {
	var s = NewAppDto()
	InjectAppDto(s)
	return s

}

func InjectAppDto(s *AppDto) {

	logrus.Debug("inject")
}
