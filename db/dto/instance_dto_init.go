package dto

import (
	"gitee.com/ichub/goconfig/common/base/baseiface"
	"gitee.com/ichub/goconfig/common/basedi"
	"github.com/sirupsen/logrus"
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: instance_dto_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: LEIJMDAS@163.COM 时间(2024-06-01 10:25:52)
	@Update 作者: LEIJMDAS@163.COM 时间(2024-06-01 10:25:52)

* *********************************************************/

const singleNameInstanceDto = "dto.InstanceDto"

// init center load
func init() {
	registerBeanInstanceDto()
}

// center InstanceDto
func registerBeanInstanceDto() {
	basedi.RegisterLoadBean(singleNameInstanceDto, LoadInstanceDto)
}

func FindBeanInstanceDto() *InstanceDto {
	bean, ok := basedi.FindBean(singleNameInstanceDto).(*InstanceDto)
	if !ok {
		logrus.Errorf("FindBeanInstanceDto: failed to cast bean to *InstanceDto")
		return nil
	}
	return bean

}

func LoadInstanceDto() baseiface.ISingleton {
	var s = NewInstanceDto()
	InjectInstanceDto(s)
	return s

}

func InjectInstanceDto(s *InstanceDto) {

	logrus.Debug("inject")
}
