package dto

import (
	"gitee.com/ichub/goconfig/common/base/baseiface"
	"gitee.com/ichub/goconfig/common/basedi"
	"github.com/sirupsen/logrus"
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: item_dto_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: LEIJMDAS@163.COM 时间(2024-06-01 10:25:52)
	@Update 作者: LEIJMDAS@163.COM 时间(2024-06-01 10:25:52)

* *********************************************************/

const singleNameItemDto = "dto.ItemDto"

// init center load
func init() {
	registerBeanItemDto()
}

// center ItemDto
func registerBeanItemDto() {
	basedi.RegisterLoadBean(singleNameItemDto, LoadItemDto)
}

func FindBeanItemDto() *ItemDto {
	bean, ok := basedi.FindBean(singleNameItemDto).(*ItemDto)
	if !ok {
		logrus.Errorf("FindBeanItemDto: failed to cast bean to *ItemDto")
		return nil
	}
	return bean

}

func LoadItemDto() baseiface.ISingleton {
	var s = NewItemDto()
	InjectItemDto(s)
	return s

}

func InjectItemDto(s *ItemDto) {

	logrus.Debug("inject")
}
