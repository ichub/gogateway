package dto

import (
	// "encoding/json"
	"gitee.com/ichub/goconfig/common/base/basedto"
	"gitee.com/ichub/gogateway/db/model"
)

/*
   @Title  文件名称: Envdto.go
   @Description 描述: 服务Envdto

   @Author  作者: leijianming@163.com  时间(2024-06-01 09:24:34)
   @Update  作者: leijianming@163.com  时间(2024-06-01 09:24:34)

*/

/*
 */
type EnvDto struct {
	basedto.BaseEntity
	model.Env
}

func NewEnvDto() *EnvDto {
	var m = &EnvDto{}
	m.InitProxy(m)
	return m
}

type EnvParamsDto struct {
	model.EnvParams
}
type EnvEntityDto struct {
	model.EnvEntity
}

func NewEnvEntityDto() *EnvEntityDto {
	var m = &EnvEntityDto{}
	//m.InitProxy(m)
	return m
}

func NewEnvParamsDto() *EnvParamsDto {
	var m = &EnvParamsDto{}
	//m.InitProxy(m)
	return m
}
