package dto

import (
	// "encoding/json"
	"gitee.com/ichub/goconfig/common/base/basedto"
	"gitee.com/ichub/gogateway/db/model"
)

/*
   @Title  文件名称: Itemdto.go
   @Description 描述: 服务Itemdto

   @Author  作者: leijianming@163.com  时间(2024-06-01 09:24:47)
   @Update  作者: leijianming@163.com  时间(2024-06-01 09:24:47)

*/

/*
配置项目
*/
type ItemDto struct {
	basedto.BaseEntity
	model.Item
}

func NewItemDto() *ItemDto {
	var m = &ItemDto{}
	m.InitProxy(m)
	return m
}

type ItemParamsDto struct {
	model.ItemParams
}
type ItemEntityDto struct {
	model.ItemEntity
}

func NewItemEntityDto() *ItemEntityDto {
	var m = &ItemEntityDto{}
	//m.InitProxy(m)
	return m
}

func NewItemParamsDto() *ItemParamsDto {
	var m = &ItemParamsDto{}
	//m.InitProxy(m)
	return m
}
