package dto

import (
	// "encoding/json"
	"gitee.com/ichub/goconfig/common/base/basedto"
	"gitee.com/ichub/gogateway/db/model"
)

/*
   @Title  文件名称: Instancedto.go
   @Description 描述: 服务Instancedto

   @Author  作者: leijianming@163.com  时间(2024-06-01 09:24:40)
   @Update  作者: leijianming@163.com  时间(2024-06-01 09:24:40)

*/

/*
使用配置的应用实例
*/
type InstanceDto struct {
	basedto.BaseEntity
	model.Instance
}

func NewInstanceDto() *InstanceDto {
	var m = &InstanceDto{}
	m.InitProxy(m)
	return m
}

type InstanceParamsDto struct {
	model.InstanceParams
}
type InstanceEntityDto struct {
	model.InstanceEntity
}

func NewInstanceEntityDto() *InstanceEntityDto {
	var m = &InstanceEntityDto{}
	//m.InitProxy(m)
	return m
}

func NewInstanceParamsDto() *InstanceParamsDto {
	var m = &InstanceParamsDto{}
	//m.InitProxy(m)
	return m
}
