package dto

import (
	// "encoding/json"
	"gitee.com/ichub/goconfig/common/base/basedto"
	"gitee.com/ichub/gogateway/db/model"
)

/*
   @Title  文件名称: ServiceInfodto.go
   @Description 描述: 服务ServiceInfodto

   @Author  作者: leijianming@163.com  时间(2024-06-01 09:25:06)
   @Update  作者: leijianming@163.com  时间(2024-06-01 09:25:06)

*/

/*
服务信息
*/
type ServiceInfoDto struct {
	basedto.BaseEntity
	model.ServiceInfo
}

func NewServiceInfoDto() *ServiceInfoDto {
	var m = &ServiceInfoDto{}
	m.InitProxy(m)
	return m
}

type ServiceInfoParamsDto struct {
	model.ServiceInfoParams
}
type ServiceInfoEntityDto struct {
	model.ServiceInfoEntity
}

func NewServiceInfoEntityDto() *ServiceInfoEntityDto {
	var m = &ServiceInfoEntityDto{}
	//m.InitProxy(m)
	return m
}

func NewServiceInfoParamsDto() *ServiceInfoParamsDto {
	var m = &ServiceInfoParamsDto{}
	//m.InitProxy(m)
	return m
}
