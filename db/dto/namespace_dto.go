package dto

import (
	// "encoding/json"
	"gitee.com/ichub/goconfig/common/base/basedto"
	"gitee.com/ichub/gogateway/db/model"
)

/*
   @Title  文件名称: Namespacedto.go
   @Description 描述: 服务Namespacedto

   @Author  作者: leijianming@163.com  时间(2024-06-01 09:24:53)
   @Update  作者: leijianming@163.com  时间(2024-06-01 09:24:53)

*/

/*
命名空间
*/
type NamespaceDto struct {
	basedto.BaseEntity
	model.Namespace
}

func NewNamespaceDto() *NamespaceDto {
	var m = &NamespaceDto{}
	m.InitProxy(m)
	return m
}

type NamespaceParamsDto struct {
	model.NamespaceParams
}
type NamespaceEntityDto struct {
	model.NamespaceEntity
}

func NewNamespaceEntityDto() *NamespaceEntityDto {
	var m = &NamespaceEntityDto{}
	//m.InitProxy(m)
	return m
}

func NewNamespaceParamsDto() *NamespaceParamsDto {
	var m = &NamespaceParamsDto{}
	//m.InitProxy(m)
	return m
}
