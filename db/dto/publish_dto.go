package dto

import (
	// "encoding/json"
	"gitee.com/ichub/goconfig/common/base/basedto"
	"gitee.com/ichub/gogateway/db/model"
)

/*
   @Title  文件名称: Publishdto.go
   @Description 描述: 服务Publishdto

   @Author  作者: leijianming@163.com  时间(2024-06-01 09:25:00)
   @Update  作者: leijianming@163.com  时间(2024-06-01 09:25:00)

*/

/*
发布
*/
type PublishDto struct {
	basedto.BaseEntity
	model.Publish
}

func NewPublishDto() *PublishDto {
	var m = &PublishDto{}
	m.InitProxy(m)
	return m
}

type PublishParamsDto struct {
	model.PublishParams
}
type PublishEntityDto struct {
	model.PublishEntity
}

func NewPublishEntityDto() *PublishEntityDto {
	var m = &PublishEntityDto{}
	//m.InitProxy(m)
	return m
}

func NewPublishParamsDto() *PublishParamsDto {
	var m = &PublishParamsDto{}
	//m.InitProxy(m)
	return m
}
