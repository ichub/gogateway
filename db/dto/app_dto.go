package dto

import (
	// "encoding/json"
	"gitee.com/ichub/goconfig/common/base/basedto"
	"gitee.com/ichub/gogateway/db/model"
)

/*
   @Title  文件名称: Appdto.go
   @Description 描述: 服务Appdto

   @Author  作者: leijianming@163.com  时间(2024-06-01 09:24:21)
   @Update  作者: leijianming@163.com  时间(2024-06-01 09:24:21)

*/

/*
应用表
*/
type AppDto struct {
	basedto.BaseEntity
	model.App
}

func NewAppDto() *AppDto {
	var m = &AppDto{}
	m.InitProxy(m)
	return m
}

type AppParamsDto struct {
	model.AppParams
}
type AppEntityDto struct {
	model.AppEntity
}

func NewAppEntityDto() *AppEntityDto {
	var m = &AppEntityDto{}
	//m.InitProxy(m)
	return m
}

func NewAppParamsDto() *AppParamsDto {
	var m = &AppParamsDto{}
	//m.InitProxy(m)
	return m
}
