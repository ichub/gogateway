package dto

import (
	// "encoding/json"
	"gitee.com/ichub/goconfig/common/base/basedto"
	"gitee.com/ichub/gogateway/db/model"
)

/*
   @Title  文件名称: AppNamespacedto.go
   @Description 描述: 服务AppNamespacedto

   @Author  作者: leijianming@163.com  时间(2024-06-01 09:24:28)
   @Update  作者: leijianming@163.com  时间(2024-06-01 09:24:28)

*/

/*
应用namespace定义
*/
type AppNamespaceDto struct {
	basedto.BaseEntity
	model.AppNamespace
}

func NewAppNamespaceDto() *AppNamespaceDto {
	var m = &AppNamespaceDto{}
	m.InitProxy(m)
	return m
}

type AppNamespaceParamsDto struct {
	model.AppNamespaceParams
}
type AppNamespaceEntityDto struct {
	model.AppNamespaceEntity
}

func NewAppNamespaceEntityDto() *AppNamespaceEntityDto {
	var m = &AppNamespaceEntityDto{}
	//m.InitProxy(m)
	return m
}

func NewAppNamespaceParamsDto() *AppNamespaceParamsDto {
	var m = &AppNamespaceParamsDto{}
	//m.InitProxy(m)
	return m
}
