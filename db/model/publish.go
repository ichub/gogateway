package model

/*
    @Title    文件名称: Publishmodel.gomodel
    @Description  描述: 服务Publishmodel

    @Author  作者: leijianming@163.com  时间(2024-06-01 09:24:58)
    @Update  作者: leijianming@163.com  时间(2024-06-01 09:24:58)

*/

import (
    "github.com/jinzhu/gorm"
    // "gitee.com/ichub/goconfig/common/base/basemodel"
    "encoding/json"
    "fmt"
	"gitee.com/ichub/goconfig/common/base/basedto"
	"time"
)


/*
 发布
 */
type Publish struct {

  	basedto.BaseEntity `gorm:"-"`
    

	/*  自增主键  */
	Id int32 `gorm:"column:id;type:int(10) unsigned;PRIMARY_KEY;comment:'自增主键'" json:"id"`
	/*  发布的Key  */
	PublishKey string `gorm:"column:publish_key;type:varchar(64);comment:'发布的Key'" json:"publish_key"`
	/*  发布名字  */
	Name string `gorm:"column:name;type:varchar(64);comment:'发布名字';default:\'default\'" json:"name"`
	/*  发布说明  */
	Comment string `gorm:"column:comment;type:varchar(256);comment:'发布说明'" json:"comment"`
	/*  AppID  */
	AppId string `gorm:"column:app_id;type:varchar(64);comment:'AppID';default:\'default\'" json:"app_id"`
	/*  ClusterName  */
	ClusterName string `gorm:"column:cluster_name;type:varchar(500);comment:'ClusterName';default:\'default\'" json:"cluster_name"`
	/*  namespaceName  */
	NamespaceName string `gorm:"column:namespace_name;type:varchar(500);comment:'namespaceName';default:\'default\'" json:"namespace_name"`
	/*  发布配置  */
	Configs string `gorm:"column:configs;type:longtext;comment:'发布配置'" json:"configs"`
	/*  是否废弃  */
	IsAbandoned byte `gorm:"column:is_abandoned;type:bit(1);comment:'是否废弃'" json:"is_abandoned"`
	/*  1: deleted, 0: normal  */
	IsDeleted byte `gorm:"column:is_deleted;type:bit(1);comment:'1: deleted, 0: normal'" json:"is_deleted"`
	/*  Delete timestamp based on milliseconds  */
	DeletedAt int64 `gorm:"column:deleted_at;type:bigint(20);comment:'Delete timestamp based on milliseconds';default:0" json:"deleted_at,string"`
	/*  创建人邮箱前缀  */
	CreatedBy string `gorm:"column:created_by;type:varchar(64);comment:'创建人邮箱前缀';default:\'default\'" json:"created_by"`
	/*  创建时间  */
	CreatedAt time.Time `gorm:"column:created_at;type:timestamp;comment:'创建时间'" json:"created_at"`
	/*  最后修改人邮箱前缀  */
	UpdatedBy string `gorm:"column:updated_by;type:varchar(64);comment:'最后修改人邮箱前缀'" json:"updated_by"`
	/*  最后修改时间  */
	UpdatedAt time.Time `gorm:"column:updated_at;type:timestamp;comment:'最后修改时间'" json:"updated_at"`

}


func NewPublish() *Publish {
	var m = &Publish{}
	m.InitProxy(m)
	return m
}

type PublishParams struct {

    

	/*  自增主键  */
	Id *int32 `gorm:"column:id;type:int(10) unsigned;PRIMARY_KEY;comment:'自增主键'" json:"id"`
	/*  发布的Key  */
	PublishKey *string `gorm:"column:publish_key;type:varchar(64);comment:'发布的Key'" json:"publish_key"`
	/*  发布名字  */
	Name *string `gorm:"column:name;type:varchar(64);comment:'发布名字';default:\'default\'" json:"name"`
	/*  发布说明  */
	Comment *string `gorm:"column:comment;type:varchar(256);comment:'发布说明'" json:"comment"`
	/*  AppID  */
	AppId *string `gorm:"column:app_id;type:varchar(64);comment:'AppID';default:\'default\'" json:"app_id"`
	/*  ClusterName  */
	ClusterName *string `gorm:"column:cluster_name;type:varchar(500);comment:'ClusterName';default:\'default\'" json:"cluster_name"`
	/*  namespaceName  */
	NamespaceName *string `gorm:"column:namespace_name;type:varchar(500);comment:'namespaceName';default:\'default\'" json:"namespace_name"`
	/*  发布配置  */
	Configs *string `gorm:"column:configs;type:longtext;comment:'发布配置'" json:"configs"`
	/*  是否废弃  */
	IsAbandoned *byte `gorm:"column:is_abandoned;type:bit(1);comment:'是否废弃'" json:"is_abandoned"`
	/*  1: deleted, 0: normal  */
	IsDeleted *byte `gorm:"column:is_deleted;type:bit(1);comment:'1: deleted, 0: normal'" json:"is_deleted"`
	/*  Delete timestamp based on milliseconds  */
	DeletedAt *int64 `gorm:"column:deleted_at;type:bigint(20);comment:'Delete timestamp based on milliseconds';default:0" json:"deleted_at,string"`
	/*  创建人邮箱前缀  */
	CreatedBy *string `gorm:"column:created_by;type:varchar(64);comment:'创建人邮箱前缀';default:\'default\'" json:"created_by"`
	/*  创建时间  */
	CreatedAt *time.Time `gorm:"column:created_at;type:timestamp;comment:'创建时间'" json:"created_at"`
	/*  最后修改人邮箱前缀  */
	UpdatedBy *string `gorm:"column:updated_by;type:varchar(64);comment:'最后修改人邮箱前缀'" json:"updated_by"`
	/*  最后修改时间  */
	UpdatedAt *time.Time `gorm:"column:updated_at;type:timestamp;comment:'最后修改时间'" json:"updated_at"`

}
type PublishEntity struct {

    

	/*  自增主键  */
	Id int32 `gorm:"column:id;type:int(10) unsigned;PRIMARY_KEY;comment:'自增主键'" json:"id"`
	/*  发布的Key  */
	PublishKey string `gorm:"column:publish_key;type:varchar(64);comment:'发布的Key'" json:"publish_key"`
	/*  发布名字  */
	Name string `gorm:"column:name;type:varchar(64);comment:'发布名字';default:\'default\'" json:"name"`
	/*  发布说明  */
	Comment string `gorm:"column:comment;type:varchar(256);comment:'发布说明'" json:"comment"`
	/*  AppID  */
	AppId string `gorm:"column:app_id;type:varchar(64);comment:'AppID';default:\'default\'" json:"app_id"`
	/*  ClusterName  */
	ClusterName string `gorm:"column:cluster_name;type:varchar(500);comment:'ClusterName';default:\'default\'" json:"cluster_name"`
	/*  namespaceName  */
	NamespaceName string `gorm:"column:namespace_name;type:varchar(500);comment:'namespaceName';default:\'default\'" json:"namespace_name"`
	/*  发布配置  */
	Configs string `gorm:"column:configs;type:longtext;comment:'发布配置'" json:"configs"`
	/*  是否废弃  */
	IsAbandoned byte `gorm:"column:is_abandoned;type:bit(1);comment:'是否废弃'" json:"is_abandoned"`
	/*  1: deleted, 0: normal  */
	IsDeleted byte `gorm:"column:is_deleted;type:bit(1);comment:'1: deleted, 0: normal'" json:"is_deleted"`
	/*  Delete timestamp based on milliseconds  */
	DeletedAt int64 `gorm:"column:deleted_at;type:bigint(20);comment:'Delete timestamp based on milliseconds';default:0" json:"deleted_at,string"`
	/*  创建人邮箱前缀  */
	CreatedBy string `gorm:"column:created_by;type:varchar(64);comment:'创建人邮箱前缀';default:\'default\'" json:"created_by"`
	/*  创建时间  */
	CreatedAt int64 `gorm:"column:created_at;type:timestamp;comment:'创建时间'" json:"created_at,string"`
	/*  最后修改人邮箱前缀  */
	UpdatedBy string `gorm:"column:updated_by;type:varchar(64);comment:'最后修改人邮箱前缀'" json:"updated_by"`
	/*  最后修改时间  */
	UpdatedAt int64 `gorm:"column:updated_at;type:timestamp;comment:'最后修改时间'" json:"updated_at,string"`

}
/*
	gorm默认生成的表名是结构名+'s',所以必须以结构方法指定！

*/
func (entity *Publish) TableName() string {

    return "publish"
}


/*
    迁移

*/
func (entity *Publish) AutoMigrate(db *gorm.DB) error {
    err := db.AutoMigrate(entity).Error
    // entity.execComment(db)

    return err
}


	/*
		exec pg comment sql
	*/
	func (entity * Publish) execComment(db *gorm.DB){

			sql := "comment on table publish is '发布';\n\t"
			sql = sql + ``
			db.Exec(sql)

	}

	/*
		指定生成结果转json字符串
	*/
	func (entity * Publish) String () string{
		s, err := json.Marshal(entity)
		if err != nil {
			fmt.Println(err.Error())
			return "{}"
		}
		return string(s)

	}

	func (entity * Publish) ToString () string{
		s, err := json.MarshalIndent(entity,""," ")
		if err != nil {
			fmt.Println(err.Error())
			return "{}"
		}
		return string(s)

	}

	func (entity * Publish) Unmarshal(body string) error {
		return json.Unmarshal([]byte(body),  entity)

	}

	func (entity * Publish) UnmarshalBy(body []byte) error {
		return json.Unmarshal( body ,  entity )

	}



/*
func (entity *Publish ) Model2PbMsg (pbentity *proto.PublishProto) * proto.PublishProto{

    entity.IniNil(true)
    

		pbentity.Id =  entity.GetId()
		pbentity.PublishKey =  entity.GetPublishKey()
		pbentity.Name =  entity.GetName()
		pbentity.Comment =  entity.GetComment()
		pbentity.AppId =  entity.GetAppId()
		pbentity.ClusterName =  entity.GetClusterName()
		pbentity.NamespaceName =  entity.GetNamespaceName()
		pbentity.Configs =  entity.GetConfigs()
		pbentity.IsAbandoned =  entity.GetIsAbandoned()
		pbentity.IsDeleted =  entity.GetIsDeleted()
		pbentity.DeletedAt =  utils.ToStr(entity.GetDeletedAt())
		pbentity.CreatedBy =  entity.GetCreatedBy()
		pbentity.CreatedAt =  entity.GetCreatedAt()
		pbentity.UpdatedBy =  entity.GetUpdatedBy()
		pbentity.UpdatedAt =  entity.GetUpdatedAt()

    return pbentity
}

func (entity *Publish ) PbMsg2Model (pbentity *proto.PublishProto) *Publish{
    entity.IniNil(true)
    

		 * entity.Id =  pbentity.GetId()
		 * entity.PublishKey =  pbentity.GetPublishKey()
		 * entity.Name =  pbentity.GetName()
		 * entity.Comment =  pbentity.GetComment()
		 * entity.AppId =  pbentity.GetAppId()
		 * entity.ClusterName =  pbentity.GetClusterName()
		 * entity.NamespaceName =  pbentity.GetNamespaceName()
		 * entity.Configs =  pbentity.GetConfigs()
		 * entity.IsAbandoned =  pbentity.GetIsAbandoned()
		 * entity.IsDeleted =  pbentity.GetIsDeleted()
		 * entity.DeletedAt =  utils.Str2Int64(pbentity.GetDeletedAt())
		 * entity.CreatedBy =  pbentity.GetCreatedBy()
		 * entity.CreatedAt =  pbentity.GetCreatedAt()
		 * entity.UpdatedBy =  pbentity.GetUpdatedBy()
		 * entity.UpdatedAt =  pbentity.GetUpdatedAt()
    return entity
}


func (entity *Publish) IniPbMsg() (pbentity *proto.PublishProto) {

	pbentity = &proto.PublishProto{}
    

		pbentity.Id =  0
		pbentity.PublishKey =  ""
		pbentity.Name =  ""
		pbentity.Comment =  ""
		pbentity.AppId =  ""
		pbentity.ClusterName =  ""
		pbentity.NamespaceName =  ""
		pbentity.Configs =  ""
		pbentity.IsAbandoned =  
		pbentity.IsDeleted =  
		pbentity.DeletedAt =  "0"
		pbentity.CreatedBy =  ""
		pbentity.CreatedAt =  
		pbentity.UpdatedBy =  ""
		pbentity.UpdatedAt =  
	return
}

*/
