package model

import (
	"gitee.com/ichub/goconfig/common/base/baseiface"
	"gitee.com/ichub/goconfig/common/basedi"
	"github.com/sirupsen/logrus"
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: service_info_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: LEIJMDAS@163.COM 时间(2024-06-01 10:25:52)
	@Update 作者: LEIJMDAS@163.COM 时间(2024-06-01 10:25:52)

* *********************************************************/

const singleNameServiceInfo = "model.ServiceInfo"

// init center load
func init() {
	registerBeanServiceInfo()
}

// center ServiceInfo
func registerBeanServiceInfo() {
	basedi.RegisterLoadBean(singleNameServiceInfo, LoadServiceInfo)
}

func FindBeanServiceInfo() *ServiceInfo {
	bean, ok := basedi.FindBean(singleNameServiceInfo).(*ServiceInfo)
	if !ok {
		logrus.Errorf("FindBeanServiceInfo: failed to cast bean to *ServiceInfo")
		return nil
	}
	return bean

}

func LoadServiceInfo() baseiface.ISingleton {
	var s = NewServiceInfo()
	InjectServiceInfo(s)
	return s

}

func InjectServiceInfo(s *ServiceInfo) {

	logrus.Debug("inject")
}
