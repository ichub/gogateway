package model

import (
	"gitee.com/ichub/goconfig/common/base/baseiface"
	"gitee.com/ichub/goconfig/common/basedi"
	"github.com/sirupsen/logrus"
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: publish_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: LEIJMDAS@163.COM 时间(2024-06-01 10:25:52)
	@Update 作者: LEIJMDAS@163.COM 时间(2024-06-01 10:25:52)

* *********************************************************/

const singleNamePublish = "model.Publish"

// init center load
func init() {
	registerBeanPublish()
}

// center Publish
func registerBeanPublish() {
	basedi.RegisterLoadBean(singleNamePublish, LoadPublish)
}

func FindBeanPublish() *Publish {
	bean, ok := basedi.FindBean(singleNamePublish).(*Publish)
	if !ok {
		logrus.Errorf("FindBeanPublish: failed to cast bean to *Publish")
		return nil
	}
	return bean

}

func LoadPublish() baseiface.ISingleton {
	var s = NewPublish()
	InjectPublish(s)
	return s

}

func InjectPublish(s *Publish) {

	logrus.Debug("inject")
}
