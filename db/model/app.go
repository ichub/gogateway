package model

/*
    @Title    文件名称: Appmodel.gomodel
    @Description  描述: 服务Appmodel

    @Author  作者: leijianming@163.com  时间(2024-06-01 09:24:20)
    @Update  作者: leijianming@163.com  时间(2024-06-01 09:24:20)

*/

import (
    "github.com/jinzhu/gorm"
    // "gitee.com/ichub/goconfig/common/base/basemodel"
    "encoding/json"
    "fmt"
	"gitee.com/ichub/goconfig/common/base/basedto"
	"time"
)


/*
 应用表
 */
type App struct {

  	basedto.BaseEntity `gorm:"-"`
    

	/*  主键  */
	Id int32 `gorm:"column:id;type:int(10) unsigned;PRIMARY_KEY;comment:'主键'" json:"id"`
	/*  AppID  */
	AppId string `gorm:"column:app_id;type:varchar(64);comment:'AppID';default:\'default\'" json:"app_id"`
	/*  应用名  */
	Name string `gorm:"column:name;type:varchar(500);comment:'应用名';default:\'default\'" json:"name"`
	/*  部门Id  */
	OrgId string `gorm:"column:org_id;type:varchar(32);comment:'部门Id';default:\'default\'" json:"org_id"`
	/*  部门名字  */
	OrgName string `gorm:"column:org_name;type:varchar(64);comment:'部门名字';default:\'default\'" json:"org_name"`
	/*  ownerName  */
	OwnerName string `gorm:"column:owner_name;type:varchar(500);comment:'ownerName';default:\'default\'" json:"owner_name"`
	/*  ownerEmail  */
	OwnerEmail string `gorm:"column:owner_email;type:varchar(500);comment:'ownerEmail';default:\'default\'" json:"owner_email"`
	/*  1: deleted, 0: normal  */
	IsDeleted bool `gorm:"column:is_deleted;type:tinyint(1);comment:'1: deleted, 0: normal'" json:"is_deleted"`
	/*  Delete timestamp based on milliseconds  */
	DeletedAt time.Time `gorm:"column:deleted_at;type:date;comment:'Delete timestamp based on milliseconds'" json:"deleted_at"`
	/*  创建人邮箱前缀  */
	CreatedBy string `gorm:"column:created_by;type:varchar(64);comment:'创建人邮箱前缀';default:\'default\'" json:"created_by"`
	/*  创建时间  */
	CreatedAt time.Time `gorm:"column:created_at;type:timestamp;comment:'创建时间'" json:"created_at"`
	/*  最后修改人邮箱前缀  */
	UpdatedBy string `gorm:"column:updated_by;type:varchar(64);comment:'最后修改人邮箱前缀'" json:"updated_by"`
	/*  最后修改时间  */
	UpdatedAt time.Time `gorm:"column:updated_at;type:timestamp;comment:'最后修改时间'" json:"updated_at"`

}


func NewApp() *App {
	var m = &App{}
	m.InitProxy(m)
	return m
}

type AppParams struct {

    

	/*  主键  */
	Id *int32 `gorm:"column:id;type:int(10) unsigned;PRIMARY_KEY;comment:'主键'" json:"id"`
	/*  AppID  */
	AppId *string `gorm:"column:app_id;type:varchar(64);comment:'AppID';default:\'default\'" json:"app_id"`
	/*  应用名  */
	Name *string `gorm:"column:name;type:varchar(500);comment:'应用名';default:\'default\'" json:"name"`
	/*  部门Id  */
	OrgId *string `gorm:"column:org_id;type:varchar(32);comment:'部门Id';default:\'default\'" json:"org_id"`
	/*  部门名字  */
	OrgName *string `gorm:"column:org_name;type:varchar(64);comment:'部门名字';default:\'default\'" json:"org_name"`
	/*  ownerName  */
	OwnerName *string `gorm:"column:owner_name;type:varchar(500);comment:'ownerName';default:\'default\'" json:"owner_name"`
	/*  ownerEmail  */
	OwnerEmail *string `gorm:"column:owner_email;type:varchar(500);comment:'ownerEmail';default:\'default\'" json:"owner_email"`
	/*  1: deleted, 0: normal  */
	IsDeleted *string `gorm:"column:is_deleted;type:tinyint(1);comment:'1: deleted, 0: normal'" json:"is_deleted"`
	/*  Delete timestamp based on milliseconds  */
	DeletedAt *time.Time `gorm:"column:deleted_at;type:date;comment:'Delete timestamp based on milliseconds'" json:"deleted_at"`
	/*  创建人邮箱前缀  */
	CreatedBy *string `gorm:"column:created_by;type:varchar(64);comment:'创建人邮箱前缀';default:\'default\'" json:"created_by"`
	/*  创建时间  */
	CreatedAt *time.Time `gorm:"column:created_at;type:timestamp;comment:'创建时间'" json:"created_at"`
	/*  最后修改人邮箱前缀  */
	UpdatedBy *string `gorm:"column:updated_by;type:varchar(64);comment:'最后修改人邮箱前缀'" json:"updated_by"`
	/*  最后修改时间  */
	UpdatedAt *time.Time `gorm:"column:updated_at;type:timestamp;comment:'最后修改时间'" json:"updated_at"`

}
type AppEntity struct {

    

	/*  主键  */
	Id int32 `gorm:"column:id;type:int(10) unsigned;PRIMARY_KEY;comment:'主键'" json:"id"`
	/*  AppID  */
	AppId string `gorm:"column:app_id;type:varchar(64);comment:'AppID';default:\'default\'" json:"app_id"`
	/*  应用名  */
	Name string `gorm:"column:name;type:varchar(500);comment:'应用名';default:\'default\'" json:"name"`
	/*  部门Id  */
	OrgId string `gorm:"column:org_id;type:varchar(32);comment:'部门Id';default:\'default\'" json:"org_id"`
	/*  部门名字  */
	OrgName string `gorm:"column:org_name;type:varchar(64);comment:'部门名字';default:\'default\'" json:"org_name"`
	/*  ownerName  */
	OwnerName string `gorm:"column:owner_name;type:varchar(500);comment:'ownerName';default:\'default\'" json:"owner_name"`
	/*  ownerEmail  */
	OwnerEmail string `gorm:"column:owner_email;type:varchar(500);comment:'ownerEmail';default:\'default\'" json:"owner_email"`
	/*  1: deleted, 0: normal  */
	IsDeleted string `gorm:"column:is_deleted;type:tinyint(1);comment:'1: deleted, 0: normal'" json:"is_deleted"`
	/*  Delete timestamp based on milliseconds  */
	DeletedAt int64 `gorm:"column:deleted_at;type:date;comment:'Delete timestamp based on milliseconds'" json:"deleted_at,string"`
	/*  创建人邮箱前缀  */
	CreatedBy string `gorm:"column:created_by;type:varchar(64);comment:'创建人邮箱前缀';default:\'default\'" json:"created_by"`
	/*  创建时间  */
	CreatedAt int64 `gorm:"column:created_at;type:timestamp;comment:'创建时间'" json:"created_at,string"`
	/*  最后修改人邮箱前缀  */
	UpdatedBy string `gorm:"column:updated_by;type:varchar(64);comment:'最后修改人邮箱前缀'" json:"updated_by"`
	/*  最后修改时间  */
	UpdatedAt int64 `gorm:"column:updated_at;type:timestamp;comment:'最后修改时间'" json:"updated_at,string"`

}
/*
	gorm默认生成的表名是结构名+'s',所以必须以结构方法指定！

*/
func (entity *App) TableName() string {

    return "app"
}


/*
    迁移

*/
func (entity *App) AutoMigrate(db *gorm.DB) error {
    err := db.AutoMigrate(entity).Error
    // entity.execComment(db)

    return err
}


	/*
		exec pg comment sql
	*/
	func (entity * App) execComment(db *gorm.DB){

			sql := "comment on table app is '应用表';\n\t"
			sql = sql + ``
			db.Exec(sql)

	}

	/*
		指定生成结果转json字符串
	*/
	func (entity * App) String () string{
		s, err := json.Marshal(entity)
		if err != nil {
			fmt.Println(err.Error())
			return "{}"
		}
		return string(s)

	}

	func (entity * App) ToString () string{
		s, err := json.MarshalIndent(entity,""," ")
		if err != nil {
			fmt.Println(err.Error())
			return "{}"
		}
		return string(s)

	}

	func (entity * App) Unmarshal(body string) error {
		return json.Unmarshal([]byte(body),  entity)

	}

	func (entity * App) UnmarshalBy(body []byte) error {
		return json.Unmarshal( body ,  entity )

	}



/*
func (entity *App ) Model2PbMsg (pbentity *proto.AppProto) * proto.AppProto{

    entity.IniNil(true)
    

		pbentity.Id =  entity.GetId()
		pbentity.AppId =  entity.GetAppId()
		pbentity.Name =  entity.GetName()
		pbentity.OrgId =  entity.GetOrgId()
		pbentity.OrgName =  entity.GetOrgName()
		pbentity.OwnerName =  entity.GetOwnerName()
		pbentity.OwnerEmail =  entity.GetOwnerEmail()
		pbentity.IsDeleted = utils.BoolPtr2Str( entity.IsDeleted )
		pbentity.DeletedAt =  entity.GetDeletedAt()
		pbentity.CreatedBy =  entity.GetCreatedBy()
		pbentity.CreatedAt =  entity.GetCreatedAt()
		pbentity.UpdatedBy =  entity.GetUpdatedBy()
		pbentity.UpdatedAt =  entity.GetUpdatedAt()

    return pbentity
}

func (entity *App ) PbMsg2Model (pbentity *proto.AppProto) *App{
    entity.IniNil(true)
    

		 * entity.Id =  pbentity.GetId()
		 * entity.AppId =  pbentity.GetAppId()
		 * entity.Name =  pbentity.GetName()
		 * entity.OrgId =  pbentity.GetOrgId()
		 * entity.OrgName =  pbentity.GetOrgName()
		 * entity.OwnerName =  pbentity.GetOwnerName()
		 * entity.OwnerEmail =  pbentity.GetOwnerEmail()
		 entity.IsDeleted =  utils.Str2BoolPtr( pbentity.GetIsDeleted() )
		 * entity.DeletedAt =  pbentity.GetDeletedAt()
		 * entity.CreatedBy =  pbentity.GetCreatedBy()
		 * entity.CreatedAt =  pbentity.GetCreatedAt()
		 * entity.UpdatedBy =  pbentity.GetUpdatedBy()
		 * entity.UpdatedAt =  pbentity.GetUpdatedAt()
    return entity
}


func (entity *App) IniPbMsg() (pbentity *proto.AppProto) {

	pbentity = &proto.AppProto{}
    

		pbentity.Id =  0
		pbentity.AppId =  ""
		pbentity.Name =  ""
		pbentity.OrgId =  ""
		pbentity.OrgName =  ""
		pbentity.OwnerName =  ""
		pbentity.OwnerEmail =  ""
		pbentity.IsDeleted =  "false"
		pbentity.DeletedAt =  
		pbentity.CreatedBy =  ""
		pbentity.CreatedAt =  
		pbentity.UpdatedBy =  ""
		pbentity.UpdatedAt =  
	return
}

*/
