package model

/*
   @Title    文件名称: TenantInfomodel.gomodel
   @Description  描述: 服务TenantInfomodel

   @Author  作者: leijianming@163.com  时间(2024-06-01 09:25:11)
   @Update  作者: leijianming@163.com  时间(2024-06-01 09:25:11)

*/

import (
	"github.com/jinzhu/gorm"
	// "gitee.com/ichub/goconfig/common/base/basemodel"
	"encoding/json"
	"fmt"
	"gitee.com/ichub/goconfig/common/base/basedto"
	//"time"
)

/*
tenant_info
*/
type TenantInfo struct {
	basedto.BaseEntity `gorm:"-"`

	/*  id  */
	Id int64 `gorm:"column:id;type:bigint(20);PRIMARY_KEY;comment:'id'" json:"id,string"`
	/*  kp  */
	Kp string `gorm:"column:kp;type:varchar(128);comment:'kp'" json:"kp"`
	/*  tenant_id  */
	TenantId string `gorm:"column:tenant_id;type:varchar(128);comment:'tenant_id'" json:"tenant_id"`
	/*  tenant_name  */
	TenantName string `gorm:"column:tenant_name;type:varchar(128);comment:'tenant_name'" json:"tenant_name"`
	/*  tenant_desc  */
	TenantDesc string `gorm:"column:tenant_desc;type:varchar(256);comment:'tenant_desc'" json:"tenant_desc"`
	/*  create_source  */
	CreateSource string `gorm:"column:create_source;type:varchar(32);comment:'create_source'" json:"create_source"`
	/*  创建时间  */
	GmtCreate int64 `gorm:"column:gmt_create;type:bigint(20);comment:'创建时间'" json:"gmt_create,string"`
	/*  修改时间  */
	GmtModified int64 `gorm:"column:gmt_modified;type:bigint(20);comment:'修改时间'" json:"gmt_modified,string"`
}

func NewTenantInfo() *TenantInfo {
	var m = &TenantInfo{}
	m.InitProxy(m)
	return m
}

type TenantInfoParams struct {

	/*  id  */
	Id *int64 `gorm:"column:id;type:bigint(20);PRIMARY_KEY;comment:'id'" json:"id,string"`
	/*  kp  */
	Kp *string `gorm:"column:kp;type:varchar(128);comment:'kp'" json:"kp"`
	/*  tenant_id  */
	TenantId *string `gorm:"column:tenant_id;type:varchar(128);comment:'tenant_id'" json:"tenant_id"`
	/*  tenant_name  */
	TenantName *string `gorm:"column:tenant_name;type:varchar(128);comment:'tenant_name'" json:"tenant_name"`
	/*  tenant_desc  */
	TenantDesc *string `gorm:"column:tenant_desc;type:varchar(256);comment:'tenant_desc'" json:"tenant_desc"`
	/*  create_source  */
	CreateSource *string `gorm:"column:create_source;type:varchar(32);comment:'create_source'" json:"create_source"`
	/*  创建时间  */
	GmtCreate *int64 `gorm:"column:gmt_create;type:bigint(20);comment:'创建时间'" json:"gmt_create,string"`
	/*  修改时间  */
	GmtModified *int64 `gorm:"column:gmt_modified;type:bigint(20);comment:'修改时间'" json:"gmt_modified,string"`
}
type TenantInfoEntity struct {

	/*  id  */
	Id int64 `gorm:"column:id;type:bigint(20);PRIMARY_KEY;comment:'id'" json:"id,string"`
	/*  kp  */
	Kp string `gorm:"column:kp;type:varchar(128);comment:'kp'" json:"kp"`
	/*  tenant_id  */
	TenantId string `gorm:"column:tenant_id;type:varchar(128);comment:'tenant_id'" json:"tenant_id"`
	/*  tenant_name  */
	TenantName string `gorm:"column:tenant_name;type:varchar(128);comment:'tenant_name'" json:"tenant_name"`
	/*  tenant_desc  */
	TenantDesc string `gorm:"column:tenant_desc;type:varchar(256);comment:'tenant_desc'" json:"tenant_desc"`
	/*  create_source  */
	CreateSource string `gorm:"column:create_source;type:varchar(32);comment:'create_source'" json:"create_source"`
	/*  创建时间  */
	GmtCreate int64 `gorm:"column:gmt_create;type:bigint(20);comment:'创建时间'" json:"gmt_create,string"`
	/*  修改时间  */
	GmtModified int64 `gorm:"column:gmt_modified;type:bigint(20);comment:'修改时间'" json:"gmt_modified,string"`
}

/*
gorm默认生成的表名是结构名+'s',所以必须以结构方法指定！
*/
func (entity *TenantInfo) TableName() string {

	return "tenant_info"
}

/*
迁移
*/
func (entity *TenantInfo) AutoMigrate(db *gorm.DB) error {
	err := db.AutoMigrate(entity).Error
	// entity.execComment(db)

	return err
}

/*
exec pg comment sql
*/
func (entity *TenantInfo) execComment(db *gorm.DB) {

	sql := "comment on table tenant_info is 'tenant_info';\n\t"
	sql = sql + ``
	db.Exec(sql)

}

/*
指定生成结果转json字符串
*/
func (entity *TenantInfo) String() string {
	s, err := json.Marshal(entity)
	if err != nil {
		fmt.Println(err.Error())
		return "{}"
	}
	return string(s)

}

func (entity *TenantInfo) ToString() string {
	s, err := json.MarshalIndent(entity, "", " ")
	if err != nil {
		fmt.Println(err.Error())
		return "{}"
	}
	return string(s)

}

func (entity *TenantInfo) Unmarshal(body string) error {
	return json.Unmarshal([]byte(body), entity)

}

func (entity *TenantInfo) UnmarshalBy(body []byte) error {
	return json.Unmarshal(body, entity)

}

/*
func (entity *TenantInfo ) Model2PbMsg (pbentity *proto.TenantInfoProto) * proto.TenantInfoProto{

    entity.IniNil(true)


		pbentity.Id =  utils.ToStr(entity.GetId())
		pbentity.Kp =  entity.GetKp()
		pbentity.TenantId =  entity.GetTenantId()
		pbentity.TenantName =  entity.GetTenantName()
		pbentity.TenantDesc =  entity.GetTenantDesc()
		pbentity.CreateSource =  entity.GetCreateSource()
		pbentity.GmtCreate =  utils.ToStr(entity.GetGmtCreate())
		pbentity.GmtModified =  utils.ToStr(entity.GetGmtModified())

    return pbentity
}

func (entity *TenantInfo ) PbMsg2Model (pbentity *proto.TenantInfoProto) *TenantInfo{
    entity.IniNil(true)


		 * entity.Id =  utils.Str2Int64(pbentity.GetId())
		 * entity.Kp =  pbentity.GetKp()
		 * entity.TenantId =  pbentity.GetTenantId()
		 * entity.TenantName =  pbentity.GetTenantName()
		 * entity.TenantDesc =  pbentity.GetTenantDesc()
		 * entity.CreateSource =  pbentity.GetCreateSource()
		 * entity.GmtCreate =  utils.Str2Int64(pbentity.GetGmtCreate())
		 * entity.GmtModified =  utils.Str2Int64(pbentity.GetGmtModified())
    return entity
}


func (entity *TenantInfo) IniPbMsg() (pbentity *proto.TenantInfoProto) {

	pbentity = &proto.TenantInfoProto{}


		pbentity.Id =  "0"
		pbentity.Kp =  ""
		pbentity.TenantId =  ""
		pbentity.TenantName =  ""
		pbentity.TenantDesc =  ""
		pbentity.CreateSource =  ""
		pbentity.GmtCreate =  "0"
		pbentity.GmtModified =  "0"
	return
}

*/
