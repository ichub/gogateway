package model

import (
	"gitee.com/ichub/goconfig/common/base/baseiface"
	"gitee.com/ichub/goconfig/common/basedi"
	"github.com/sirupsen/logrus"
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: instance_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: LEIJMDAS@163.COM 时间(2024-06-01 10:25:52)
	@Update 作者: LEIJMDAS@163.COM 时间(2024-06-01 10:25:52)

* *********************************************************/

const singleNameInstance = "model.Instance"

// init center load
func init() {
	registerBeanInstance()
}

// center Instance
func registerBeanInstance() {
	basedi.RegisterLoadBean(singleNameInstance, LoadInstance)
}

func FindBeanInstance() *Instance {
	bean, ok := basedi.FindBean(singleNameInstance).(*Instance)
	if !ok {
		logrus.Errorf("FindBeanInstance: failed to cast bean to *Instance")
		return nil
	}
	return bean

}

func LoadInstance() baseiface.ISingleton {
	var s = NewInstance()
	InjectInstance(s)
	return s

}

func InjectInstance(s *Instance) {

	logrus.Debug("inject")
}
