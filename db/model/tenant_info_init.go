package model

import (
	"gitee.com/ichub/goconfig/common/base/baseiface"
	"gitee.com/ichub/goconfig/common/basedi"
	"github.com/sirupsen/logrus"
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: tenant_info_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: LEIJMDAS@163.COM 时间(2024-06-01 10:25:52)
	@Update 作者: LEIJMDAS@163.COM 时间(2024-06-01 10:25:52)

* *********************************************************/

const singleNameTenantInfo = "model.TenantInfo"

// init center load
func init() {
	registerBeanTenantInfo()
}

// center TenantInfo
func registerBeanTenantInfo() {
	basedi.RegisterLoadBean(singleNameTenantInfo, LoadTenantInfo)
}

func FindBeanTenantInfo() *TenantInfo {
	bean, ok := basedi.FindBean(singleNameTenantInfo).(*TenantInfo)
	if !ok {
		logrus.Errorf("FindBeanTenantInfo: failed to cast bean to *TenantInfo")
		return nil
	}
	return bean

}

func LoadTenantInfo() baseiface.ISingleton {
	var s = NewTenantInfo()
	InjectTenantInfo(s)
	return s

}

func InjectTenantInfo(s *TenantInfo) {

	logrus.Debug("inject")
}
