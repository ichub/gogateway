package model

import (
	"gitee.com/ichub/goconfig/common/base/baseiface"
	"gitee.com/ichub/goconfig/common/basedi"
	"github.com/sirupsen/logrus"
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: item_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: LEIJMDAS@163.COM 时间(2024-06-01 10:25:52)
	@Update 作者: LEIJMDAS@163.COM 时间(2024-06-01 10:25:52)

* *********************************************************/

const singleNameItem = "model.Item"

// init center load
func init() {
	registerBeanItem()
}

// center Item
func registerBeanItem() {
	basedi.RegisterLoadBean(singleNameItem, LoadItem)
}

func FindBeanItem() *Item {
	bean, ok := basedi.FindBean(singleNameItem).(*Item)
	if !ok {
		logrus.Errorf("FindBeanItem: failed to cast bean to *Item")
		return nil
	}
	return bean

}

func LoadItem() baseiface.ISingleton {
	var s = NewItem()
	InjectItem(s)
	return s

}

func InjectItem(s *Item) {

	logrus.Debug("inject")
}
