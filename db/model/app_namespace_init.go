package model

import (
	"gitee.com/ichub/goconfig/common/base/baseiface"
	"gitee.com/ichub/goconfig/common/basedi"
	"github.com/sirupsen/logrus"
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: app_namespace_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: LEIJMDAS@163.COM 时间(2024-06-01 10:25:52)
	@Update 作者: LEIJMDAS@163.COM 时间(2024-06-01 10:25:52)

* *********************************************************/

const singleNameAppNamespace = "model.AppNamespace"

// init center load
func init() {
	registerBeanAppNamespace()
}

// center AppNamespace
func registerBeanAppNamespace() {
	basedi.RegisterLoadBean(singleNameAppNamespace, LoadAppNamespace)
}

func FindBeanAppNamespace() *AppNamespace {
	bean, ok := basedi.FindBean(singleNameAppNamespace).(*AppNamespace)
	if !ok {
		logrus.Errorf("FindBeanAppNamespace: failed to cast bean to *AppNamespace")
		return nil
	}
	return bean

}

func LoadAppNamespace() baseiface.ISingleton {
	var s = NewAppNamespace()
	InjectAppNamespace(s)
	return s

}

func InjectAppNamespace(s *AppNamespace) {

	logrus.Debug("inject")
}
