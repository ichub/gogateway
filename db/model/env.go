package model

/*
   @Title    文件名称: Envmodel.gomodel
   @Description  描述: 服务Envmodel

   @Author  作者: leijianming@163.com  时间(2024-06-01 09:24:33)
   @Update  作者: leijianming@163.com  时间(2024-06-01 09:24:33)

*/

import (
	"github.com/jinzhu/gorm"
	// "gitee.com/ichub/goconfig/common/base/basemodel"
	"encoding/json"
	"fmt"
	"gitee.com/ichub/goconfig/common/base/basedto"
	//"time"
)

/*
 */
type Env struct {
	basedto.BaseEntity `gorm:"-"`

	/*    */
	Id int32 `gorm:"column: id;type:int(11);PRIMARY_KEY;comment:''" json:" id"`
	/*  环境  */
	Name string `gorm:"column:name;type:varchar(255);comment:'环境'" json:"name"`
}

func NewEnv() *Env {
	var m = &Env{}
	m.InitProxy(m)
	return m
}

type EnvParams struct {

	/*    */
	Id *int32 `gorm:"column: id;type:int(11);PRIMARY_KEY;comment:''" json:" id"`
	/*  环境  */
	Name *string `gorm:"column:name;type:varchar(255);comment:'环境'" json:"name"`
}
type EnvEntity struct {

	/*    */
	Id int32 `gorm:"column: id;type:int(11);PRIMARY_KEY;comment:''" json:" id"`
	/*  环境  */
	Name string `gorm:"column:name;type:varchar(255);comment:'环境'" json:"name"`
}

/*
gorm默认生成的表名是结构名+'s',所以必须以结构方法指定！
*/
func (entity *Env) TableName() string {

	return "env"
}

/*
迁移
*/
func (entity *Env) AutoMigrate(db *gorm.DB) error {
	err := db.AutoMigrate(entity).Error
	// entity.execComment(db)

	return err
}

/*
exec pg comment sql
*/
func (entity *Env) execComment(db *gorm.DB) {

	sql := "comment on table env is '';\n\t"
	sql = sql + ``
	db.Exec(sql)

}

/*
指定生成结果转json字符串
*/
func (entity *Env) String() string {
	s, err := json.Marshal(entity)
	if err != nil {
		fmt.Println(err.Error())
		return "{}"
	}
	return string(s)

}

func (entity *Env) ToString() string {
	s, err := json.MarshalIndent(entity, "", " ")
	if err != nil {
		fmt.Println(err.Error())
		return "{}"
	}
	return string(s)

}

func (entity *Env) Unmarshal(body string) error {
	return json.Unmarshal([]byte(body), entity)

}

func (entity *Env) UnmarshalBy(body []byte) error {
	return json.Unmarshal(body, entity)

}

/*
func (entity *Env ) Model2PbMsg (pbentity *proto.EnvProto) * proto.EnvProto{

    entity.IniNil(true)


		pbentity.Id =  entity.GetId()
		pbentity.Name =  entity.GetName()

    return pbentity
}

func (entity *Env ) PbMsg2Model (pbentity *proto.EnvProto) *Env{
    entity.IniNil(true)


		 * entity.Id =  pbentity.GetId()
		 * entity.Name =  pbentity.GetName()
    return entity
}


func (entity *Env) IniPbMsg() (pbentity *proto.EnvProto) {

	pbentity = &proto.EnvProto{}


		pbentity.Id =  0
		pbentity.Name =  ""
	return
}

*/
