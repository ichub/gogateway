package model

import (
	"gitee.com/ichub/goconfig/common/base/baseiface"
	"gitee.com/ichub/goconfig/common/basedi"
	"github.com/sirupsen/logrus"
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: app_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: LEIJMDAS@163.COM 时间(2024-06-01 10:25:52)
	@Update 作者: LEIJMDAS@163.COM 时间(2024-06-01 10:25:52)

* *********************************************************/

const singleNameApp = "model.App"

// init center load
func init() {
	registerBeanApp()
}

// center App
func registerBeanApp() {
	basedi.RegisterLoadBean(singleNameApp, LoadApp)
}

func FindBeanApp() *App {
	bean, ok := basedi.FindBean(singleNameApp).(*App)
	if !ok {
		logrus.Errorf("FindBeanApp: failed to cast bean to *App")
		return nil
	}
	return bean

}

func LoadApp() baseiface.ISingleton {
	var s = NewApp()
	InjectApp(s)
	return s

}

func InjectApp(s *App) {

	logrus.Debug("inject")
}
