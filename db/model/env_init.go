package model

import (
	"gitee.com/ichub/goconfig/common/base/baseiface"
	"gitee.com/ichub/goconfig/common/basedi"
	"github.com/sirupsen/logrus"
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: env_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: LEIJMDAS@163.COM 时间(2024-06-01 10:25:52)
	@Update 作者: LEIJMDAS@163.COM 时间(2024-06-01 10:25:52)

* *********************************************************/

const singleNameEnv = "model.Env"

// init center load
func init() {
	registerBeanEnv()
}

// center Env
func registerBeanEnv() {
	basedi.RegisterLoadBean(singleNameEnv, LoadEnv)
}

func FindBeanEnv() *Env {
	bean, ok := basedi.FindBean(singleNameEnv).(*Env)
	if !ok {
		logrus.Errorf("FindBeanEnv: failed to cast bean to *Env")
		return nil
	}
	return bean

}

func LoadEnv() baseiface.ISingleton {
	var s = NewEnv()
	InjectEnv(s)
	return s

}

func InjectEnv(s *Env) {

	logrus.Debug("inject")
}
