package model

/*
    @Title    文件名称: Itemmodel.gomodel
    @Description  描述: 服务Itemmodel

    @Author  作者: leijianming@163.com  时间(2024-06-01 09:24:45)
    @Update  作者: leijianming@163.com  时间(2024-06-01 09:24:45)

*/

import (
    "github.com/jinzhu/gorm"
    // "gitee.com/ichub/goconfig/common/base/basemodel"
    "encoding/json"
    "fmt"
	"gitee.com/ichub/goconfig/common/base/basedto"
	"time"
)


/*
 配置项目
 */
type Item struct {

  	basedto.BaseEntity `gorm:"-"`
    

	/*  自增Id  */
	Id int32 `gorm:"column:id;type:int(10) unsigned;PRIMARY_KEY;comment:'自增Id'" json:"id"`
	/*  集群NamespaceId  */
	NamespaceId int32 `gorm:"column:namespace_id;type:int(10) unsigned;comment:'集群NamespaceId';default:0" json:"namespace_id"`
	/*  配置项Key  */
	Key string `gorm:"column:key;type:varchar(128);comment:'配置项Key';default:\'default\'" json:"key"`
	/*  配置项类型，0: String，1: Number，2: Boolean，3: JSON  */
	Type bool `gorm:"column:type;type:tinyint(3) unsigned;comment:'配置项类型，0: String，1: Number，2: Boolean，3: JSON'" json:"type"`
	/*  配置项值  */
	Value string `gorm:"column:value;type:longtext;comment:'配置项值'" json:"value"`
	/*  注释  */
	Comment string `gorm:"column:comment;type:varchar(1024);comment:'注释'" json:"comment"`
	/*  行号  */
	LineNum int32 `gorm:"column:line_num;type:int(10) unsigned;comment:'行号';default:0" json:"line_num"`
	/*  1: deleted, 0: normal  */
	IsDeleted byte `gorm:"column:is_deleted;type:bit(1);comment:'1: deleted, 0: normal'" json:"is_deleted"`
	/*  Delete timestamp based on milliseconds  */
	DeletedAt int64 `gorm:"column:deleted_at;type:bigint(20);comment:'Delete timestamp based on milliseconds';default:0" json:"deleted_at,string"`
	/*  创建人邮箱前缀  */
	CeatedBy string `gorm:"column:ceated_by;type:varchar(64);comment:'创建人邮箱前缀';default:\'default\'" json:"ceated_by"`
	/*  创建时间  */
	CreatedAt time.Time `gorm:"column:created_at;type:timestamp;comment:'创建时间'" json:"created_at"`
	/*  最后修改人邮箱前缀  */
	UpdatedBy string `gorm:"column:updated_by;type:varchar(64);comment:'最后修改人邮箱前缀'" json:"updated_by"`
	/*  最后修改时间  */
	UpdatedAt time.Time `gorm:"column:updated_at;type:timestamp;comment:'最后修改时间'" json:"updated_at"`

}


func NewItem() *Item {
	var m = &Item{}
	m.InitProxy(m)
	return m
}

type ItemParams struct {

    

	/*  自增Id  */
	Id *int32 `gorm:"column:id;type:int(10) unsigned;PRIMARY_KEY;comment:'自增Id'" json:"id"`
	/*  集群NamespaceId  */
	NamespaceId *int32 `gorm:"column:namespace_id;type:int(10) unsigned;comment:'集群NamespaceId';default:0" json:"namespace_id"`
	/*  配置项Key  */
	Key *string `gorm:"column:key;type:varchar(128);comment:'配置项Key';default:\'default\'" json:"key"`
	/*  配置项类型，0: String，1: Number，2: Boolean，3: JSON  */
	Type *string `gorm:"column:type;type:tinyint(3) unsigned;comment:'配置项类型，0: String，1: Number，2: Boolean，3: JSON'" json:"type"`
	/*  配置项值  */
	Value *string `gorm:"column:value;type:longtext;comment:'配置项值'" json:"value"`
	/*  注释  */
	Comment *string `gorm:"column:comment;type:varchar(1024);comment:'注释'" json:"comment"`
	/*  行号  */
	LineNum *int32 `gorm:"column:line_num;type:int(10) unsigned;comment:'行号';default:0" json:"line_num"`
	/*  1: deleted, 0: normal  */
	IsDeleted *byte `gorm:"column:is_deleted;type:bit(1);comment:'1: deleted, 0: normal'" json:"is_deleted"`
	/*  Delete timestamp based on milliseconds  */
	DeletedAt *int64 `gorm:"column:deleted_at;type:bigint(20);comment:'Delete timestamp based on milliseconds';default:0" json:"deleted_at,string"`
	/*  创建人邮箱前缀  */
	CeatedBy *string `gorm:"column:ceated_by;type:varchar(64);comment:'创建人邮箱前缀';default:\'default\'" json:"ceated_by"`
	/*  创建时间  */
	CreatedAt *time.Time `gorm:"column:created_at;type:timestamp;comment:'创建时间'" json:"created_at"`
	/*  最后修改人邮箱前缀  */
	UpdatedBy *string `gorm:"column:updated_by;type:varchar(64);comment:'最后修改人邮箱前缀'" json:"updated_by"`
	/*  最后修改时间  */
	UpdatedAt *time.Time `gorm:"column:updated_at;type:timestamp;comment:'最后修改时间'" json:"updated_at"`

}
type ItemEntity struct {

    

	/*  自增Id  */
	Id int32 `gorm:"column:id;type:int(10) unsigned;PRIMARY_KEY;comment:'自增Id'" json:"id"`
	/*  集群NamespaceId  */
	NamespaceId int32 `gorm:"column:namespace_id;type:int(10) unsigned;comment:'集群NamespaceId';default:0" json:"namespace_id"`
	/*  配置项Key  */
	Key string `gorm:"column:key;type:varchar(128);comment:'配置项Key';default:\'default\'" json:"key"`
	/*  配置项类型，0: String，1: Number，2: Boolean，3: JSON  */
	Type string `gorm:"column:type;type:tinyint(3) unsigned;comment:'配置项类型，0: String，1: Number，2: Boolean，3: JSON'" json:"type"`
	/*  配置项值  */
	Value string `gorm:"column:value;type:longtext;comment:'配置项值'" json:"value"`
	/*  注释  */
	Comment string `gorm:"column:comment;type:varchar(1024);comment:'注释'" json:"comment"`
	/*  行号  */
	LineNum int32 `gorm:"column:line_num;type:int(10) unsigned;comment:'行号';default:0" json:"line_num"`
	/*  1: deleted, 0: normal  */
	IsDeleted byte `gorm:"column:is_deleted;type:bit(1);comment:'1: deleted, 0: normal'" json:"is_deleted"`
	/*  Delete timestamp based on milliseconds  */
	DeletedAt int64 `gorm:"column:deleted_at;type:bigint(20);comment:'Delete timestamp based on milliseconds';default:0" json:"deleted_at,string"`
	/*  创建人邮箱前缀  */
	CeatedBy string `gorm:"column:ceated_by;type:varchar(64);comment:'创建人邮箱前缀';default:\'default\'" json:"ceated_by"`
	/*  创建时间  */
	CreatedAt int64 `gorm:"column:created_at;type:timestamp;comment:'创建时间'" json:"created_at,string"`
	/*  最后修改人邮箱前缀  */
	UpdatedBy string `gorm:"column:updated_by;type:varchar(64);comment:'最后修改人邮箱前缀'" json:"updated_by"`
	/*  最后修改时间  */
	UpdatedAt int64 `gorm:"column:updated_at;type:timestamp;comment:'最后修改时间'" json:"updated_at,string"`

}
/*
	gorm默认生成的表名是结构名+'s',所以必须以结构方法指定！

*/
func (entity *Item) TableName() string {

    return "item"
}


/*
    迁移

*/
func (entity *Item) AutoMigrate(db *gorm.DB) error {
    err := db.AutoMigrate(entity).Error
    // entity.execComment(db)

    return err
}


	/*
		exec pg comment sql
	*/
	func (entity * Item) execComment(db *gorm.DB){

			sql := "comment on table item is '配置项目';\n\t"
			sql = sql + ``
			db.Exec(sql)

	}

	/*
		指定生成结果转json字符串
	*/
	func (entity * Item) String () string{
		s, err := json.Marshal(entity)
		if err != nil {
			fmt.Println(err.Error())
			return "{}"
		}
		return string(s)

	}

	func (entity * Item) ToString () string{
		s, err := json.MarshalIndent(entity,""," ")
		if err != nil {
			fmt.Println(err.Error())
			return "{}"
		}
		return string(s)

	}

	func (entity * Item) Unmarshal(body string) error {
		return json.Unmarshal([]byte(body),  entity)

	}

	func (entity * Item) UnmarshalBy(body []byte) error {
		return json.Unmarshal( body ,  entity )

	}



/*
func (entity *Item ) Model2PbMsg (pbentity *proto.ItemProto) * proto.ItemProto{

    entity.IniNil(true)
    

		pbentity.Id =  entity.GetId()
		pbentity.NamespaceId =  entity.GetNamespaceId()
		pbentity.Key =  entity.GetKey()
		pbentity.Type = utils.BoolPtr2Str( entity.Type )
		pbentity.Value =  entity.GetValue()
		pbentity.Comment =  entity.GetComment()
		pbentity.LineNum =  entity.GetLineNum()
		pbentity.IsDeleted =  entity.GetIsDeleted()
		pbentity.DeletedAt =  utils.ToStr(entity.GetDeletedAt())
		pbentity.CeatedBy =  entity.GetCeatedBy()
		pbentity.CreatedAt =  entity.GetCreatedAt()
		pbentity.UpdatedBy =  entity.GetUpdatedBy()
		pbentity.UpdatedAt =  entity.GetUpdatedAt()

    return pbentity
}

func (entity *Item ) PbMsg2Model (pbentity *proto.ItemProto) *Item{
    entity.IniNil(true)
    

		 * entity.Id =  pbentity.GetId()
		 * entity.NamespaceId =  pbentity.GetNamespaceId()
		 * entity.Key =  pbentity.GetKey()
		 entity.Type =  utils.Str2BoolPtr( pbentity.GetType() )
		 * entity.Value =  pbentity.GetValue()
		 * entity.Comment =  pbentity.GetComment()
		 * entity.LineNum =  pbentity.GetLineNum()
		 * entity.IsDeleted =  pbentity.GetIsDeleted()
		 * entity.DeletedAt =  utils.Str2Int64(pbentity.GetDeletedAt())
		 * entity.CeatedBy =  pbentity.GetCeatedBy()
		 * entity.CreatedAt =  pbentity.GetCreatedAt()
		 * entity.UpdatedBy =  pbentity.GetUpdatedBy()
		 * entity.UpdatedAt =  pbentity.GetUpdatedAt()
    return entity
}


func (entity *Item) IniPbMsg() (pbentity *proto.ItemProto) {

	pbentity = &proto.ItemProto{}
    

		pbentity.Id =  0
		pbentity.NamespaceId =  0
		pbentity.Key =  ""
		pbentity.Type =  "false"
		pbentity.Value =  ""
		pbentity.Comment =  ""
		pbentity.LineNum =  0
		pbentity.IsDeleted =  
		pbentity.DeletedAt =  "0"
		pbentity.CeatedBy =  ""
		pbentity.CreatedAt =  
		pbentity.UpdatedBy =  ""
		pbentity.UpdatedAt =  
	return
}

*/
