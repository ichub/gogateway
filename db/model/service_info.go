package model

/*
    @Title    文件名称: ServiceInfomodel.gomodel
    @Description  描述: 服务ServiceInfomodel

    @Author  作者: leijianming@163.com  时间(2024-06-01 14:38:45)
    @Update  作者: leijianming@163.com  时间(2024-06-01 14:38:45)

*/

import (
    "github.com/jinzhu/gorm"
    // "gitee.com/ichub/goconfig/common/base/basemodel"
    "encoding/json"
    "fmt"
	"gitee.com/ichub/goconfig/common/base/basedto"
	"time"
)


/*
 服务信息
 */
type ServiceInfo struct {

  	basedto.BaseEntity `gorm:"-"`
    

	/*    */
	Id int32 `gorm:"column:id;type:int(11);PRIMARY_KEY;comment:''" json:"id"`
	/*  租户信息  */
	TenantId int64 `gorm:"column:tenant_id;type:bigint(20);comment:'租户信息'" json:"tenant_id,string"`
	/*    */
	ServiceId string `gorm:"column:service_id;type:varchar(512);comment:''" json:"service_id"`
	/*  服务名称  */
	ServiceName string `gorm:"column:service_name;type:varchar(512);comment:'服务名称'" json:"service_name"`
	/*    */
	Ip string `gorm:"column:ip;type:varchar(128);comment:''" json:"ip"`
	/*    */
	Port int32 `gorm:"column:port;type:int(11);comment:''" json:"port"`
	/*  服务元数据  */
	Metadata string `gorm:"column:metadata;type:mediumtext;comment:'服务元数据'" json:"metadata"`
	/*    */
	CheckUrl string `gorm:"column:check_url;type:varchar(512);comment:''" json:"check_url"`
	/*    */
	CheckInterval int32 `gorm:"column:check_interval;type:int(255);comment:''" json:"check_interval"`
	/*  启动、运行、停止  */
	Status string `gorm:"column:status;type:varchar(255);comment:'启动、运行、停止'" json:"status"`
	/*    */
	Version string `gorm:"column:version;type:varchar(255);comment:''" json:"version"`
	/*    */
	Weight float64 `gorm:"column:weight;type:double;comment:''" json:"weight"`
	/*    */
	BalancingStrategy string `gorm:"column:balancing_strategy;type:varchar(255);comment:''" json:"balancing_strategy"`
	/*    */
	RegisterInterval int32 `gorm:"column:register_interval;type:int(11);comment:'';default:20" json:"register_interval"`
	/*    */
	ExpireTime int32 `gorm:"column:expire_time;type:int(11);comment:'';default:30" json:"expire_time"`
	/*    */
	RegisterTime time.Time `gorm:"column:register_time;type:datetime;comment:''" json:"register_time"`

}


func NewServiceInfo() *ServiceInfo {
	var m = &ServiceInfo{}
	m.InitProxy(m)
	return m
}

type ServiceInfoParams struct {

    

	/*    */
	Id *int32 `gorm:"column:id;type:int(11);PRIMARY_KEY;comment:''" json:"id"`
	/*  租户信息  */
	TenantId *int64 `gorm:"column:tenant_id;type:bigint(20);comment:'租户信息'" json:"tenant_id,string"`
	/*    */
	ServiceId *string `gorm:"column:service_id;type:varchar(512);comment:''" json:"service_id"`
	/*  服务名称  */
	ServiceName *string `gorm:"column:service_name;type:varchar(512);comment:'服务名称'" json:"service_name"`
	/*    */
	Ip *string `gorm:"column:ip;type:varchar(128);comment:''" json:"ip"`
	/*    */
	Port *int32 `gorm:"column:port;type:int(11);comment:''" json:"port"`
	/*  服务元数据  */
	Metadata *string `gorm:"column:metadata;type:mediumtext;comment:'服务元数据'" json:"metadata"`
	/*    */
	CheckUrl *string `gorm:"column:check_url;type:varchar(512);comment:''" json:"check_url"`
	/*    */
	CheckInterval *int32 `gorm:"column:check_interval;type:int(255);comment:''" json:"check_interval"`
	/*  启动、运行、停止  */
	Status *string `gorm:"column:status;type:varchar(255);comment:'启动、运行、停止'" json:"status"`
	/*    */
	Version *string `gorm:"column:version;type:varchar(255);comment:''" json:"version"`
	/*    */
	Weight *float64 `gorm:"column:weight;type:double;comment:''" json:"weight"`
	/*    */
	BalancingStrategy *string `gorm:"column:balancing_strategy;type:varchar(255);comment:''" json:"balancing_strategy"`
	/*    */
	RegisterInterval *int32 `gorm:"column:register_interval;type:int(11);comment:'';default:20" json:"register_interval"`
	/*    */
	ExpireTime *int32 `gorm:"column:expire_time;type:int(11);comment:'';default:30" json:"expire_time"`
	/*    */
	RegisterTime *time.Time `gorm:"column:register_time;type:datetime;comment:''" json:"register_time"`

}
type ServiceInfoEntity struct {

    

	/*    */
	Id int32 `gorm:"column:id;type:int(11);PRIMARY_KEY;comment:''" json:"id"`
	/*  租户信息  */
	TenantId int64 `gorm:"column:tenant_id;type:bigint(20);comment:'租户信息'" json:"tenant_id,string"`
	/*    */
	ServiceId string `gorm:"column:service_id;type:varchar(512);comment:''" json:"service_id"`
	/*  服务名称  */
	ServiceName string `gorm:"column:service_name;type:varchar(512);comment:'服务名称'" json:"service_name"`
	/*    */
	Ip string `gorm:"column:ip;type:varchar(128);comment:''" json:"ip"`
	/*    */
	Port int32 `gorm:"column:port;type:int(11);comment:''" json:"port"`
	/*  服务元数据  */
	Metadata string `gorm:"column:metadata;type:mediumtext;comment:'服务元数据'" json:"metadata"`
	/*    */
	CheckUrl string `gorm:"column:check_url;type:varchar(512);comment:''" json:"check_url"`
	/*    */
	CheckInterval int32 `gorm:"column:check_interval;type:int(255);comment:''" json:"check_interval"`
	/*  启动、运行、停止  */
	Status string `gorm:"column:status;type:varchar(255);comment:'启动、运行、停止'" json:"status"`
	/*    */
	Version string `gorm:"column:version;type:varchar(255);comment:''" json:"version"`
	/*    */
	Weight float64 `gorm:"column:weight;type:double;comment:''" json:"weight"`
	/*    */
	BalancingStrategy string `gorm:"column:balancing_strategy;type:varchar(255);comment:''" json:"balancing_strategy"`
	/*    */
	RegisterInterval int32 `gorm:"column:register_interval;type:int(11);comment:'';default:20" json:"register_interval"`
	/*    */
	ExpireTime int32 `gorm:"column:expire_time;type:int(11);comment:'';default:30" json:"expire_time"`
	/*    */
	RegisterTime int64 `gorm:"column:register_time;type:datetime;comment:''" json:"register_time,string"`

}
/*
	gorm默认生成的表名是结构名+'s',所以必须以结构方法指定！

*/
func (entity *ServiceInfo) TableName() string {

    return "service_info"
}


/*
    迁移

*/
func (entity *ServiceInfo) AutoMigrate(db *gorm.DB) error {
    err := db.AutoMigrate(entity).Error
    // entity.execComment(db)

    return err
}


	/*
		exec pg comment sql
	*/
	func (entity * ServiceInfo) execComment(db *gorm.DB){

			sql := "comment on table service_info is '服务信息';\n\t"
			sql = sql + ``
			db.Exec(sql)

	}

	/*
		指定生成结果转json字符串
	*/
	func (entity * ServiceInfo) String () string{
		s, err := json.Marshal(entity)
		if err != nil {
			fmt.Println(err.Error())
			return "{}"
		}
		return string(s)

	}

	func (entity * ServiceInfo) ToString () string{
		s, err := json.MarshalIndent(entity,""," ")
		if err != nil {
			fmt.Println(err.Error())
			return "{}"
		}
		return string(s)

	}

	func (entity * ServiceInfo) Unmarshal(body string) error {
		return json.Unmarshal([]byte(body),  entity)

	}

	func (entity * ServiceInfo) UnmarshalBy(body []byte) error {
		return json.Unmarshal( body ,  entity )

	}



/*
func (entity *ServiceInfo ) Model2PbMsg (pbentity *proto.ServiceInfoProto) * proto.ServiceInfoProto{

    entity.IniNil(true)
    

		pbentity.Id =  entity.GetId()
		pbentity.TenantId =  utils.ToStr(entity.GetTenantId())
		pbentity.ServiceId =  entity.GetServiceId()
		pbentity.ServiceName =  entity.GetServiceName()
		pbentity.Ip =  entity.GetIp()
		pbentity.Port =  entity.GetPort()
		pbentity.Metadata =  entity.GetMetadata()
		pbentity.CheckUrl =  entity.GetCheckUrl()
		pbentity.CheckInterval =  entity.GetCheckInterval()
		pbentity.Status =  entity.GetStatus()
		pbentity.Version =  entity.GetVersion()
		pbentity.Weight =  entity.GetWeight()
		pbentity.BalancingStrategy =  entity.GetBalancingStrategy()
		pbentity.RegisterInterval =  entity.GetRegisterInterval()
		pbentity.ExpireTime =  entity.GetExpireTime()
		pbentity.RegisterTime =  entity.GetRegisterTime()

    return pbentity
}

func (entity *ServiceInfo ) PbMsg2Model (pbentity *proto.ServiceInfoProto) *ServiceInfo{
    entity.IniNil(true)
    

		 * entity.Id =  pbentity.GetId()
		 * entity.TenantId =  utils.Str2Int64(pbentity.GetTenantId())
		 * entity.ServiceId =  pbentity.GetServiceId()
		 * entity.ServiceName =  pbentity.GetServiceName()
		 * entity.Ip =  pbentity.GetIp()
		 * entity.Port =  pbentity.GetPort()
		 * entity.Metadata =  pbentity.GetMetadata()
		 * entity.CheckUrl =  pbentity.GetCheckUrl()
		 * entity.CheckInterval =  pbentity.GetCheckInterval()
		 * entity.Status =  pbentity.GetStatus()
		 * entity.Version =  pbentity.GetVersion()
		 * entity.Weight =  pbentity.GetWeight()
		 * entity.BalancingStrategy =  pbentity.GetBalancingStrategy()
		 * entity.RegisterInterval =  pbentity.GetRegisterInterval()
		 * entity.ExpireTime =  pbentity.GetExpireTime()
		 * entity.RegisterTime =  pbentity.GetRegisterTime()
    return entity
}


func (entity *ServiceInfo) IniPbMsg() (pbentity *proto.ServiceInfoProto) {

	pbentity = &proto.ServiceInfoProto{}
    

		pbentity.Id =  0
		pbentity.TenantId =  "0"
		pbentity.ServiceId =  ""
		pbentity.ServiceName =  ""
		pbentity.Ip =  ""
		pbentity.Port =  0
		pbentity.Metadata =  ""
		pbentity.CheckUrl =  ""
		pbentity.CheckInterval =  0
		pbentity.Status =  ""
		pbentity.Version =  ""
		pbentity.Weight =  0
		pbentity.BalancingStrategy =  ""
		pbentity.RegisterInterval =  0
		pbentity.ExpireTime =  0
		pbentity.RegisterTime =  
	return
}

*/
