package model

/*
    @Title    文件名称: AppNamespacemodel.gomodel
    @Description  描述: 服务AppNamespacemodel

    @Author  作者: leijianming@163.com  时间(2024-06-01 09:24:26)
    @Update  作者: leijianming@163.com  时间(2024-06-01 09:24:26)

*/

import (
    "github.com/jinzhu/gorm"
    // "gitee.com/ichub/goconfig/common/base/basemodel"
    "encoding/json"
    "fmt"
	"gitee.com/ichub/goconfig/common/base/basedto"
	"time"
)


/*
 应用namespace定义
 */
type AppNamespace struct {

  	basedto.BaseEntity `gorm:"-"`
    

	/*  自增主键  */
	Id int32 `gorm:"column:id;type:int(10) unsigned;PRIMARY_KEY;comment:'自增主键'" json:"id"`
	/*  namespace名字，注意，需要全局唯一  */
	Name string `gorm:"column:name;type:varchar(32);comment:'namespace名字，注意，需要全局唯一'" json:"name"`
	/*  app id  */
	AppId string `gorm:"column:app_id;type:varchar(64);comment:'app id'" json:"app_id"`
	/*  namespace的format类型  */
	Format string `gorm:"column:format;type:varchar(32);comment:'namespace的format类型';default:\'properties\'" json:"format"`
	/*  namespace是否为公共  */
	IsPublic byte `gorm:"column:is_public;type:bit(1);comment:'namespace是否为公共'" json:"is_public"`
	/*  注释  */
	Comment string `gorm:"column:comment;type:varchar(64);comment:'注释'" json:"comment"`
	/*  1: deleted, 0: normal  */
	IsDeleted byte `gorm:"column:is_deleted;type:bit(1);comment:'1: deleted, 0: normal'" json:"is_deleted"`
	/*  Delete timestamp based on milliseconds  */
	DeletedAt int64 `gorm:"column:deleted_at;type:bigint(20);comment:'Delete timestamp based on milliseconds';default:0" json:"deleted_at,string"`
	/*  创建人邮箱前缀  */
	CreatedBy string `gorm:"column:created_by;type:varchar(64);comment:'创建人邮箱前缀';default:\'default\'" json:"created_by"`
	/*  创建时间  */
	CreatedAt time.Time `gorm:"column:created_at;type:timestamp;comment:'创建时间'" json:"created_at"`
	/*  最后修改人邮箱前缀  */
	UpdatedBy string `gorm:"column:updated_by;type:varchar(64);comment:'最后修改人邮箱前缀'" json:"updated_by"`
	/*  最后修改时间  */
	UpdatedAt time.Time `gorm:"column:updated_at;type:timestamp;comment:'最后修改时间'" json:"updated_at"`

}


func NewAppNamespace() *AppNamespace {
	var m = &AppNamespace{}
	m.InitProxy(m)
	return m
}

type AppNamespaceParams struct {

    

	/*  自增主键  */
	Id *int32 `gorm:"column:id;type:int(10) unsigned;PRIMARY_KEY;comment:'自增主键'" json:"id"`
	/*  namespace名字，注意，需要全局唯一  */
	Name *string `gorm:"column:name;type:varchar(32);comment:'namespace名字，注意，需要全局唯一'" json:"name"`
	/*  app id  */
	AppId *string `gorm:"column:app_id;type:varchar(64);comment:'app id'" json:"app_id"`
	/*  namespace的format类型  */
	Format *string `gorm:"column:format;type:varchar(32);comment:'namespace的format类型';default:\'properties\'" json:"format"`
	/*  namespace是否为公共  */
	IsPublic *byte `gorm:"column:is_public;type:bit(1);comment:'namespace是否为公共'" json:"is_public"`
	/*  注释  */
	Comment *string `gorm:"column:comment;type:varchar(64);comment:'注释'" json:"comment"`
	/*  1: deleted, 0: normal  */
	IsDeleted *byte `gorm:"column:is_deleted;type:bit(1);comment:'1: deleted, 0: normal'" json:"is_deleted"`
	/*  Delete timestamp based on milliseconds  */
	DeletedAt *int64 `gorm:"column:deleted_at;type:bigint(20);comment:'Delete timestamp based on milliseconds';default:0" json:"deleted_at,string"`
	/*  创建人邮箱前缀  */
	CreatedBy *string `gorm:"column:created_by;type:varchar(64);comment:'创建人邮箱前缀';default:\'default\'" json:"created_by"`
	/*  创建时间  */
	CreatedAt *time.Time `gorm:"column:created_at;type:timestamp;comment:'创建时间'" json:"created_at"`
	/*  最后修改人邮箱前缀  */
	UpdatedBy *string `gorm:"column:updated_by;type:varchar(64);comment:'最后修改人邮箱前缀'" json:"updated_by"`
	/*  最后修改时间  */
	UpdatedAt *time.Time `gorm:"column:updated_at;type:timestamp;comment:'最后修改时间'" json:"updated_at"`

}
type AppNamespaceEntity struct {

    

	/*  自增主键  */
	Id int32 `gorm:"column:id;type:int(10) unsigned;PRIMARY_KEY;comment:'自增主键'" json:"id"`
	/*  namespace名字，注意，需要全局唯一  */
	Name string `gorm:"column:name;type:varchar(32);comment:'namespace名字，注意，需要全局唯一'" json:"name"`
	/*  app id  */
	AppId string `gorm:"column:app_id;type:varchar(64);comment:'app id'" json:"app_id"`
	/*  namespace的format类型  */
	Format string `gorm:"column:format;type:varchar(32);comment:'namespace的format类型';default:\'properties\'" json:"format"`
	/*  namespace是否为公共  */
	IsPublic byte `gorm:"column:is_public;type:bit(1);comment:'namespace是否为公共'" json:"is_public"`
	/*  注释  */
	Comment string `gorm:"column:comment;type:varchar(64);comment:'注释'" json:"comment"`
	/*  1: deleted, 0: normal  */
	IsDeleted byte `gorm:"column:is_deleted;type:bit(1);comment:'1: deleted, 0: normal'" json:"is_deleted"`
	/*  Delete timestamp based on milliseconds  */
	DeletedAt int64 `gorm:"column:deleted_at;type:bigint(20);comment:'Delete timestamp based on milliseconds';default:0" json:"deleted_at,string"`
	/*  创建人邮箱前缀  */
	CreatedBy string `gorm:"column:created_by;type:varchar(64);comment:'创建人邮箱前缀';default:\'default\'" json:"created_by"`
	/*  创建时间  */
	CreatedAt int64 `gorm:"column:created_at;type:timestamp;comment:'创建时间'" json:"created_at,string"`
	/*  最后修改人邮箱前缀  */
	UpdatedBy string `gorm:"column:updated_by;type:varchar(64);comment:'最后修改人邮箱前缀'" json:"updated_by"`
	/*  最后修改时间  */
	UpdatedAt int64 `gorm:"column:updated_at;type:timestamp;comment:'最后修改时间'" json:"updated_at,string"`

}
/*
	gorm默认生成的表名是结构名+'s',所以必须以结构方法指定！

*/
func (entity *AppNamespace) TableName() string {

    return "app_namespace"
}


/*
    迁移

*/
func (entity *AppNamespace) AutoMigrate(db *gorm.DB) error {
    err := db.AutoMigrate(entity).Error
    // entity.execComment(db)

    return err
}


	/*
		exec pg comment sql
	*/
	func (entity * AppNamespace) execComment(db *gorm.DB){

			sql := "comment on table app_namespace is '应用namespace定义';\n\t"
			sql = sql + ``
			db.Exec(sql)

	}

	/*
		指定生成结果转json字符串
	*/
	func (entity * AppNamespace) String () string{
		s, err := json.Marshal(entity)
		if err != nil {
			fmt.Println(err.Error())
			return "{}"
		}
		return string(s)

	}

	func (entity * AppNamespace) ToString () string{
		s, err := json.MarshalIndent(entity,""," ")
		if err != nil {
			fmt.Println(err.Error())
			return "{}"
		}
		return string(s)

	}

	func (entity * AppNamespace) Unmarshal(body string) error {
		return json.Unmarshal([]byte(body),  entity)

	}

	func (entity * AppNamespace) UnmarshalBy(body []byte) error {
		return json.Unmarshal( body ,  entity )

	}



/*
func (entity *AppNamespace ) Model2PbMsg (pbentity *proto.AppNamespaceProto) * proto.AppNamespaceProto{

    entity.IniNil(true)
    

		pbentity.Id =  entity.GetId()
		pbentity.Name =  entity.GetName()
		pbentity.AppId =  entity.GetAppId()
		pbentity.Format =  entity.GetFormat()
		pbentity.IsPublic =  entity.GetIsPublic()
		pbentity.Comment =  entity.GetComment()
		pbentity.IsDeleted =  entity.GetIsDeleted()
		pbentity.DeletedAt =  utils.ToStr(entity.GetDeletedAt())
		pbentity.CreatedBy =  entity.GetCreatedBy()
		pbentity.CreatedAt =  entity.GetCreatedAt()
		pbentity.UpdatedBy =  entity.GetUpdatedBy()
		pbentity.UpdatedAt =  entity.GetUpdatedAt()

    return pbentity
}

func (entity *AppNamespace ) PbMsg2Model (pbentity *proto.AppNamespaceProto) *AppNamespace{
    entity.IniNil(true)
    

		 * entity.Id =  pbentity.GetId()
		 * entity.Name =  pbentity.GetName()
		 * entity.AppId =  pbentity.GetAppId()
		 * entity.Format =  pbentity.GetFormat()
		 * entity.IsPublic =  pbentity.GetIsPublic()
		 * entity.Comment =  pbentity.GetComment()
		 * entity.IsDeleted =  pbentity.GetIsDeleted()
		 * entity.DeletedAt =  utils.Str2Int64(pbentity.GetDeletedAt())
		 * entity.CreatedBy =  pbentity.GetCreatedBy()
		 * entity.CreatedAt =  pbentity.GetCreatedAt()
		 * entity.UpdatedBy =  pbentity.GetUpdatedBy()
		 * entity.UpdatedAt =  pbentity.GetUpdatedAt()
    return entity
}


func (entity *AppNamespace) IniPbMsg() (pbentity *proto.AppNamespaceProto) {

	pbentity = &proto.AppNamespaceProto{}
    

		pbentity.Id =  0
		pbentity.Name =  ""
		pbentity.AppId =  ""
		pbentity.Format =  ""
		pbentity.IsPublic =  
		pbentity.Comment =  ""
		pbentity.IsDeleted =  
		pbentity.DeletedAt =  "0"
		pbentity.CreatedBy =  ""
		pbentity.CreatedAt =  
		pbentity.UpdatedBy =  ""
		pbentity.UpdatedAt =  
	return
}

*/
