package model

import (
	"gitee.com/ichub/goconfig/common/base/baseiface"
	"gitee.com/ichub/goconfig/common/basedi"
	"github.com/sirupsen/logrus"
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: namespace_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: LEIJMDAS@163.COM 时间(2024-06-01 10:25:52)
	@Update 作者: LEIJMDAS@163.COM 时间(2024-06-01 10:25:52)

* *********************************************************/

const singleNameNamespace = "model.Namespace"

// init center load
func init() {
	registerBeanNamespace()
}

// center Namespace
func registerBeanNamespace() {
	basedi.RegisterLoadBean(singleNameNamespace, LoadNamespace)
}

func FindBeanNamespace() *Namespace {
	bean, ok := basedi.FindBean(singleNameNamespace).(*Namespace)
	if !ok {
		logrus.Errorf("FindBeanNamespace: failed to cast bean to *Namespace")
		return nil
	}
	return bean

}

func LoadNamespace() baseiface.ISingleton {
	var s = NewNamespace()
	InjectNamespace(s)
	return s

}

func InjectNamespace(s *Namespace) {

	logrus.Debug("inject")
}
