package model

/*
    @Title    文件名称: Instancemodel.gomodel
    @Description  描述: 服务Instancemodel

    @Author  作者: leijianming@163.com  时间(2024-06-01 09:24:39)
    @Update  作者: leijianming@163.com  时间(2024-06-01 09:24:39)

*/

import (
    "github.com/jinzhu/gorm"
    // "gitee.com/ichub/goconfig/common/base/basemodel"
    "encoding/json"
    "fmt"
	"gitee.com/ichub/goconfig/common/base/basedto"
	"time"
)


/*
 使用配置的应用实例
 */
type Instance struct {

  	basedto.BaseEntity `gorm:"-"`
    

	/*  自增Id  */
	Id int32 `gorm:"column:id;type:int(11) unsigned;PRIMARY_KEY;comment:'自增Id'" json:"id"`
	/*  AppID  */
	AppId string `gorm:"column:app_id;type:varchar(64);comment:'AppID';default:\'default\'" json:"app_id"`
	/*  ClusterName  */
	ClusterName string `gorm:"column:cluster_name;type:varchar(32);comment:'ClusterName';default:\'default\'" json:"cluster_name"`
	/*  Data Center Name  */
	DataCenter string `gorm:"column:data_center;type:varchar(64);comment:'Data Center Name';default:\'default\'" json:"data_center"`
	/*  instance ip  */
	Ip string `gorm:"column:ip;type:varchar(32);comment:'instance ip'" json:"ip"`
	/*  创建时间  */
	CreatedAt time.Time `gorm:"column:created_at;type:timestamp;comment:'创建时间'" json:"created_at"`
	/*  最后修改时间  */
	UpdatedAt time.Time `gorm:"column:updated_at;type:timestamp;comment:'最后修改时间'" json:"updated_at"`
	/*    */
	EnvId int32 `gorm:"column:env_id;type:int(11);comment:''" json:"env_id"`
	/*  租户  */
	TenantId int64 `gorm:"column:tenant_id;type:bigint(20);comment:'租户'" json:"tenant_id,string"`

}


func NewInstance() *Instance {
	var m = &Instance{}
	m.InitProxy(m)
	return m
}

type InstanceParams struct {

    

	/*  自增Id  */
	Id *int32 `gorm:"column:id;type:int(11) unsigned;PRIMARY_KEY;comment:'自增Id'" json:"id"`
	/*  AppID  */
	AppId *string `gorm:"column:app_id;type:varchar(64);comment:'AppID';default:\'default\'" json:"app_id"`
	/*  ClusterName  */
	ClusterName *string `gorm:"column:cluster_name;type:varchar(32);comment:'ClusterName';default:\'default\'" json:"cluster_name"`
	/*  Data Center Name  */
	DataCenter *string `gorm:"column:data_center;type:varchar(64);comment:'Data Center Name';default:\'default\'" json:"data_center"`
	/*  instance ip  */
	Ip *string `gorm:"column:ip;type:varchar(32);comment:'instance ip'" json:"ip"`
	/*  创建时间  */
	CreatedAt *time.Time `gorm:"column:created_at;type:timestamp;comment:'创建时间'" json:"created_at"`
	/*  最后修改时间  */
	UpdatedAt *time.Time `gorm:"column:updated_at;type:timestamp;comment:'最后修改时间'" json:"updated_at"`
	/*    */
	EnvId *int32 `gorm:"column:env_id;type:int(11);comment:''" json:"env_id"`
	/*  租户  */
	TenantId *int64 `gorm:"column:tenant_id;type:bigint(20);comment:'租户'" json:"tenant_id,string"`

}
type InstanceEntity struct {

    

	/*  自增Id  */
	Id int32 `gorm:"column:id;type:int(11) unsigned;PRIMARY_KEY;comment:'自增Id'" json:"id"`
	/*  AppID  */
	AppId string `gorm:"column:app_id;type:varchar(64);comment:'AppID';default:\'default\'" json:"app_id"`
	/*  ClusterName  */
	ClusterName string `gorm:"column:cluster_name;type:varchar(32);comment:'ClusterName';default:\'default\'" json:"cluster_name"`
	/*  Data Center Name  */
	DataCenter string `gorm:"column:data_center;type:varchar(64);comment:'Data Center Name';default:\'default\'" json:"data_center"`
	/*  instance ip  */
	Ip string `gorm:"column:ip;type:varchar(32);comment:'instance ip'" json:"ip"`
	/*  创建时间  */
	CreatedAt int64 `gorm:"column:created_at;type:timestamp;comment:'创建时间'" json:"created_at,string"`
	/*  最后修改时间  */
	UpdatedAt int64 `gorm:"column:updated_at;type:timestamp;comment:'最后修改时间'" json:"updated_at,string"`
	/*    */
	EnvId int32 `gorm:"column:env_id;type:int(11);comment:''" json:"env_id"`
	/*  租户  */
	TenantId int64 `gorm:"column:tenant_id;type:bigint(20);comment:'租户'" json:"tenant_id,string"`

}
/*
	gorm默认生成的表名是结构名+'s',所以必须以结构方法指定！

*/
func (entity *Instance) TableName() string {

    return "instance"
}


/*
    迁移

*/
func (entity *Instance) AutoMigrate(db *gorm.DB) error {
    err := db.AutoMigrate(entity).Error
    // entity.execComment(db)

    return err
}


	/*
		exec pg comment sql
	*/
	func (entity * Instance) execComment(db *gorm.DB){

			sql := "comment on table instance is '使用配置的应用实例';\n\t"
			sql = sql + ``
			db.Exec(sql)

	}

	/*
		指定生成结果转json字符串
	*/
	func (entity * Instance) String () string{
		s, err := json.Marshal(entity)
		if err != nil {
			fmt.Println(err.Error())
			return "{}"
		}
		return string(s)

	}

	func (entity * Instance) ToString () string{
		s, err := json.MarshalIndent(entity,""," ")
		if err != nil {
			fmt.Println(err.Error())
			return "{}"
		}
		return string(s)

	}

	func (entity * Instance) Unmarshal(body string) error {
		return json.Unmarshal([]byte(body),  entity)

	}

	func (entity * Instance) UnmarshalBy(body []byte) error {
		return json.Unmarshal( body ,  entity )

	}



/*
func (entity *Instance ) Model2PbMsg (pbentity *proto.InstanceProto) * proto.InstanceProto{

    entity.IniNil(true)
    

		pbentity.Id =  entity.GetId()
		pbentity.AppId =  entity.GetAppId()
		pbentity.ClusterName =  entity.GetClusterName()
		pbentity.DataCenter =  entity.GetDataCenter()
		pbentity.Ip =  entity.GetIp()
		pbentity.CreatedAt =  entity.GetCreatedAt()
		pbentity.UpdatedAt =  entity.GetUpdatedAt()
		pbentity.EnvId =  entity.GetEnvId()
		pbentity.TenantId =  utils.ToStr(entity.GetTenantId())

    return pbentity
}

func (entity *Instance ) PbMsg2Model (pbentity *proto.InstanceProto) *Instance{
    entity.IniNil(true)
    

		 * entity.Id =  pbentity.GetId()
		 * entity.AppId =  pbentity.GetAppId()
		 * entity.ClusterName =  pbentity.GetClusterName()
		 * entity.DataCenter =  pbentity.GetDataCenter()
		 * entity.Ip =  pbentity.GetIp()
		 * entity.CreatedAt =  pbentity.GetCreatedAt()
		 * entity.UpdatedAt =  pbentity.GetUpdatedAt()
		 * entity.EnvId =  pbentity.GetEnvId()
		 * entity.TenantId =  utils.Str2Int64(pbentity.GetTenantId())
    return entity
}


func (entity *Instance) IniPbMsg() (pbentity *proto.InstanceProto) {

	pbentity = &proto.InstanceProto{}
    

		pbentity.Id =  0
		pbentity.AppId =  ""
		pbentity.ClusterName =  ""
		pbentity.DataCenter =  ""
		pbentity.Ip =  ""
		pbentity.CreatedAt =  
		pbentity.UpdatedAt =  
		pbentity.EnvId =  0
		pbentity.TenantId =  "0"
	return
}

*/
