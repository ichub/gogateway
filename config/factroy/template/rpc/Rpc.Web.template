package rpcweb

/*
   @Title  文件名称 : {{.FileName}}
   @Description 描述: {{.Description}}

   @Author  作者: {{.Author}}  时间({{.DATETIME}})
   @Update  作者: {{.Author}}  时间({{.DATETIME}})

*/


import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/micro/go-micro/v2"
	"github.com/micro/go-micro/v2/logger"
	"github.com/micro/go-micro/v2/registry"
	"github.com/micro/go-micro/v2/registry/etcd"
	"gitee.com/ichub/goconfig/common/base/basedto"
	"gitee.com/ichub/goconfig/common/base/basemodel"
	"gitee.com/ichub/goweb/common/base/baseutils"
	"ichubengine-server/shop/dto"
	_ "ichubengine-server/shop/iface"
	"ichubengine-server/shop/model"
	{{.LcModelName}}Proto "ichubengine-server/shop/proto/{{.LcModelName}}"

	"io/ioutil"
	"strconv"
	"time"
)

type {{.ModelName}}RpcClientInfo struct {
	Hostip, ServerName, ClientName string
}
type {{.ModelName}}RpcController struct {
	iniFlag bool

	rpcService {{.LcModelName}}Proto.{{.ModelName}}Service
}

var Inst{{.ModelName}}RpcController {{.ModelName}}RpcController

func (cInst *{{.ModelName}}RpcController) getRpcService() {{.LcModelName}}Proto.{{.ModelName}}Service {
	rpcClientInfo := {{.ModelName}}RpcClientInfo{
		"192.168.4.119:2379",
		"shop.server",
		"shop.client",
	}
	if cInst.iniFlag == false {
		cInst.registerClient(rpcClientInfo)
		cInst.iniFlag = true
	}
	return cInst.rpcService
}

func (cInst *{{.ModelName}}RpcController) registerClient(rpcClientInfo {{.ModelName}}RpcClientInfo) {

	reg := etcd.NewRegistry(func(options *registry.Options) {
		options.Addrs = []string{
			rpcClientInfo.Hostip,
		}
	})

	service := micro.NewService(
		micro.Registry(reg),
		micro.Name(rpcClientInfo.ClientName),
	)
	// Initialise the client and parse command line flags
	service.Init()

	cInst.rpcService = {{.LcModelName}}Proto.New{{.ModelName}}Service(rpcClientInfo.ServerName, service.Client())

}

func (cInst *{{.ModelName}}RpcController) Query(ctx *gin.Context) {
	ctx.Header("Content-Type", "application/json")
	result := dto.{{.ModelName}}PageResult{}

	defer ctx.Request.Body.Close()
	var bodyBytes []byte
	bodyBytes, err := ioutil.ReadAll(ctx.Request.Body)

	if err != nil {
		logger.Error(err)
		result.FailMessage(err.Error())
		ctx.String(basedto.CODE_REQUEST_BAD, result.String())
		return
	}

	var param dto.{{.ModelName}}QueryParam
	err = json.Unmarshal(bodyBytes, &param)
	if err != nil {
		logger.Error(err)
		ctx.String(basedto.CODE_REQUEST_BAD, "bad request!")
		return
	}
	rpcParam := &{{.LcModelName}}Proto.{{.ModelName}}QueryParam{}

	rpcParam.PageSize = param.PageSize
	rpcParam.Current = param.Current
	rpcParam.OrderBys = param.OrderBys
    rpcParam.FuzzyQuery = utils.Bool2Str(param.FuzzyQuery)
	rpcParam.EsQuery = utils.Bool2Str(param.EsQuery)

    if param.Param != nil {
		rpcParam.Param = &{{.LcModelName}}Proto.{{.ModelName}}Message{}
	 	pp := rpcParam.Param

        pp.InRanges=make( map[string]string)
		pp.DateRanges=make( map[string]*{{.LcModelName}}Proto.{{.ModelName}}IntList)
		pp.IntRanges=make( map[string]*{{.LcModelName}}Proto.{{.ModelName}}IntList)
		pp.StringRanges=make( map[string]*{{.LcModelName}}Proto.{{.ModelName}}StringList)

		pp.InRanges = param.Param.InRanges
		for k, v := range param.Param.DateRanges {
			pp.DateRanges[k] =  &{{.LcModelName}}Proto.{{.ModelName}}IntList{Value: v}
		}
		for k, v := range param.Param.IntRanges {
			pp.IntRanges[k] = &{{.LcModelName}}Proto.{{.ModelName}}IntList{Value: v}

		}
		for k, v := range param.Param.StringRanges {
			pp.StringRanges[k] = &{{.LcModelName}}Proto.{{.ModelName}}StringList{Value: v}
		}

		cInst.CopyModel2PbMsg(&param.Param.{{.ModelName}},pp)
	}
	rpcResult, err := cInst.getRpcService().Query(context.TODO(), rpcParam)

	if err != nil {
		logger.Error(err)
		result.FailMessage(err.Error())
		ctx.String(basedto.CODE_SUCCESS, result.String())
		return
	}
	result.Code = rpcResult.Code
	result.Msg = rpcResult.Msg
	result.Page.Current = rpcResult.Page.Current
	result.Page.PageSize = rpcResult.Page.PageSize
	result.Page.Count = int(rpcResult.Page.Count)
	result.Page.Total = rpcResult.Page.Total
	if result.Code == 200 {
		cInst.CopyPbMsg2Models(&result.Data, &rpcResult.Data)
	}

	ctx.String(basedto.CODE_SUCCESS, result.String())
}

func (cInst *{{.ModelName}}RpcController) FindById(ctx *gin.Context) {
	jsonResult := &dto.{{.ModelName}}JsonResult{}
	ctx.Header("Content-Type", "application/json")

	sid := ctx.DefaultQuery("id", "0")

	result, err := cInst.getRpcService().FindById(context.TODO(), &{{.LcModelName}}Proto.{{.ModelName}}IDRequest{ {{.pkey}}: sid })

	if err != nil {
		jsonResult.FailMessage(err.Error())
		ctx.Header("Content-Type", "application/json")
		ctx.String(basedto.CODE_SUCCESS, jsonResult.ToString())
		return
	}
	jsonResult.Code = result.Code
	jsonResult.Msg = result.Msg
	if result.Data != nil {
		jsonResult.Data = new(model.{{.ModelName}})
		cInst.CopyPbMsg2Model(jsonResult.Data, result.Data)
	}

	ctx.Header("Content-Type", "application/json")
	ctx.String(basedto.CODE_SUCCESS, jsonResult.ToString())

}

func (cInst *{{.ModelName}}RpcController) FindByIds(ctx *gin.Context) {
	result := &dto.{{.ModelName}}PageResult{}
	ctx.Header("Content-Type", "application/json")

	ids := ctx.DefaultQuery("ids", "0")

	rpcResult, err := cInst.getRpcService().FindByIds(context.TODO(), &{{.LcModelName}}Proto.{{.ModelName}}ByIdsRequest{Ids: ids})

	if err != nil {
		logger.Error(err)
		result.FailMessage(err.Error())
		ctx.String(basedto.CODE_SUCCESS, result.String())
		return
	}
	result.Code = rpcResult.Code
	result.Msg = rpcResult.Msg

    if rpcResult.Code == 200 {
        result.Page.Current = 1
        result.Page.PageSize = int32(len(rpcResult.Data))
        result.Page.Count = len(rpcResult.Data)
        result.Page.Total = result.Page.PageSize
        cInst.CopyPbMsg2Models(&result.Data, &rpcResult.Data)
    }


	ctx.String(basedto.CODE_SUCCESS, result.String())

}

func (cInst *{{.ModelName}}RpcController) DeleteById(ctx *gin.Context) {
	jsonResult := &basedto.JsonResult{}
	ctx.Header("Content-Type", "application/json")

	sid := ctx.DefaultQuery("id", "0")

	result, err := cInst.getRpcService().DeleteById(context.TODO(), &{{.LcModelName}}Proto.{{.ModelName}}IDRequest{ {{.pkey}}: sid })
	if err != nil {
		logger.Error(err)
		jsonResult.FailMessage(err.Error())
		ctx.String(basedto.CODE_SUCCESS, jsonResult.String())
		return
	}

	jsonResult.Code = result.Code
	jsonResult.Msg = result.Msg
	jsonResult.Data = result.Data

	ctx.String(basedto.CODE_SUCCESS, jsonResult.String())

}

func (cInst *{{.ModelName}}RpcController) Save(ctx *gin.Context) {
	jsonResult := &basedto.JsonResult{}
	ctx.Header("Content-Type", "application/json")

	defer ctx.Request.Body.Close()
	var bodyBytes []byte
	bodyBytes, err := ioutil.ReadAll(ctx.Request.Body)
	if err != nil {
		ctx.String(basedto.CODE_REQUEST_BAD, "bad request!")
		return
	}

	// 新建缓冲区并替换原有Request.body
	//ctx.Request.Body = ioutil.NopCloser(bytes.NewBuffer(bodyBytes))
	var entity model.{{.ModelName}}
	err = json.Unmarshal(bodyBytes, &entity)
	if err != nil {
		ctx.String(basedto.CODE_REQUEST_BAD, err.Error())
		return
	}
	fmt.Println(entity.ToString())
	var in *{{.LcModelName}}Proto.{{.ModelName}}Message = &{{.LcModelName}}Proto.{{.ModelName}}Message{}
	cInst.CopyModel2PbMsg(&entity, in)
	result, err := cInst.getRpcService().Save(context.TODO(), in)

	if err != nil {
		jsonResult.FailMessage(err.Error())
		ctx.String(basedto.CODE_SUCCESS, jsonResult.ToString())

		return
	}
	jsonResult.Code = result.Code
	jsonResult.Msg = result.Msg
	jsonResult.Data = result.Data

	ctx.String(basedto.CODE_SUCCESS, jsonResult.String())

}

func (cInst *{{.ModelName}}RpcController) UpdateNotNullProps(ctx *gin.Context) {
	jsonResult := &basedto.JsonResult{}
	ctx.Header("Content-Type", "application/json")

	defer ctx.Request.Body.Close()
	var bodyBytes []byte
	bodyBytes, err := ioutil.ReadAll(ctx.Request.Body)
	if err != nil {
		ctx.String(basedto.CODE_REQUEST_BAD, "bad request!")
		return
	}

	// 新建缓冲区并替换原有Request.body
	//ctx.Request.Body = ioutil.NopCloser(bytes.NewBuffer(bodyBytes))
	var entity model.{{.ModelName}}
	err = json.Unmarshal(bodyBytes, &entity)

	if err != nil {
		ctx.String(basedto.CODE_REQUEST_BAD, "bad request!")
		return
	}

	var {{.LcModelName}}MessageData {{.LcModelName}}Proto.{{.ModelName}}MessageData
	{{.LcModelName}}MessageData.Data = entity.String()

	result, err := cInst.getRpcService().UpdateNotNullProps(context.TODO(), &{{.LcModelName}}MessageData)

	if err != nil {
		jsonResult.FailMessage(err.Error())
		ctx.String(basedto.CODE_SUCCESS, jsonResult.ToString())
		return
	}
	jsonResult.Code = result.Code
	jsonResult.Msg = result.Msg
	jsonResult.Data = result.Data

	ctx.String(basedto.CODE_SUCCESS, jsonResult.String())
}

func (cInst *{{.ModelName}}RpcController) CopyModel2PbMsgs(entities []model.{{.ModelName}}, pbentities *[]*{{.LcModelName}}Proto.{{.ModelName}}Message) {

	for _, v := range entities {
		out := &{{.LcModelName}}Proto.{{.ModelName}}Message{}
		cInst.CopyModel2PbMsg(&v, out)
		*pbentities = append(*pbentities, out)
	}

}

func (cInst *{{.ModelName}}RpcController) CopyModel2PbMsg(entity *model.{{.ModelName}}, pbentity *{{.LcModelName}}Proto.{{.ModelName}}Message) {

	 {{.Model2PbMsg}}

}

func (cInst *{{.ModelName}}RpcController) IniPbMsg() (pbentity *{{.LcModelName}}Proto.{{.ModelName}}Message) {

    pbentity = &{{.LcModelName}}Proto.{{.ModelName}}Message{}
    {{.IniPbMsg}}

    return
}

func (cInst *{{.ModelName}}RpcController) CopyPbMsg2Models(entities *[]model.{{.ModelName}}, pbentities *[]*{{.LcModelName}}Proto.{{.ModelName}}Message) {

	for _, v := range *pbentities {
		out := &model.{{.ModelName}}{}
		cInst.CopyPbMsg2Model(out, v)
		*entities = append(*entities, *out)
	}

}

func (cInst *{{.ModelName}}RpcController) CopyPbMsg2Model(entity *model.{{.ModelName}}, pbentity *{{.LcModelName}}Proto.{{.ModelName}}Message) {

	entity.Ini(true)
	{{.PbMsg2Model}}

}

func (cInst *{{.ModelName}}RpcController) IniRanges(pp * {{.LcModelName}}Proto.{{.ModelName}}Message) {

        pp.InRanges = make( map[string]string )
		pp.DateRanges=make( map[string]*{{.LcModelName}}Proto.{{.ModelName}}IntList)
		pp.IntRanges=make( map[string]*{{.LcModelName}}Proto.{{.ModelName}}IntList)
		pp.StringRanges=make( map[string]*{{.LcModelName}}Proto.{{.ModelName}}StringList)


}


func (cInst *{{.ModelName}}RpcController) UpdateNotNil(ctx *gin.Context) {
	  cInst.Update(ctx,nil)
}


func (cInst *{{.ModelName}}RpcController) UpdateActive(ctx *gin.Context) {
	  cInst.Update(ctx,cInst.TransActive)
}


func (cInst *{{.ModelName}}RpcController) TransActive(entity* model.{{.ModelName}}) *model.{{.ModelName}} {
	rentity := entity
	/*rentity = &model.{{.ModelName}}{}
	rentity.ColumnId = entity.ColumnId
	rentity.Active = entity.Active
	fmt.Println("TransUpdateActive")
	if rentity.ColumnId == nil || rentity.Active == nil {

		return nil
	}*/

	return rentity
}

func (cInst *{{.ModelName}}RpcController) Update(ctx *gin.Context,funTrans func(entity *model.{{.ModelName}}) *model.{{.ModelName}}) {
	jsonResult := &basedto.JsonResult{}
	ctx.Header("Content-Type", "application/json")

	defer ctx.Request.Body.Close()
	var bodyBytes []byte
	bodyBytes, err := ioutil.ReadAll(ctx.Request.Body)
	if err != nil {
		ctx.String(basedto.CODE_REQUEST_BAD, "bad request!")
		return
	}

	// 新建缓冲区并替换原有Request.body
	//ctx.Request.Body = ioutil.NopCloser(bytes.NewBuffer(bodyBytes))
	var entity model.{{.ModelName}}
	err = json.Unmarshal(bodyBytes, &entity)

	if err != nil {
		ctx.String(basedto.CODE_REQUEST_BAD, "bad request!")
		return
	}
    if funTrans != nil {
		pentity  := funTrans(&entity)
		if pentity == nil   {

			jsonResult.FailMessage("param is nil")
			ctx.String(basedto.CODE_SUCCESS, jsonResult.ToString())
			return
		}
		entity = *pentity
	}
	var {{.LcModelName}}MessageData {{.LcModelName}}Proto.{{.ModelName}}MessageData
	{{.LcModelName}}MessageData.Data = entity.String()

	result, err := cInst.getRpcService().UpdateNotNullProps(context.TODO(), &{{.LcModelName}}MessageData)

	if err != nil {
		jsonResult.FailMessage(err.Error())
		ctx.String(basedto.CODE_SUCCESS, jsonResult.ToString())
		return
	}
	jsonResult.Code = result.Code
	jsonResult.Msg = result.Msg
	jsonResult.Data = result.Data

	ctx.String(basedto.CODE_SUCCESS, jsonResult.String())
}
