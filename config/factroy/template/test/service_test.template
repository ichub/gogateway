package service

/*
   @Title  文件名称: Test{{.ModelName}}ServiceSuite.go
   @Description 描述: 测试服务Test{{.ModelName}}ServiceSuite

   @Author  作者: {{.Author}} 时间: {{.DATETIME}}
   @Update  作者: {{.Author}} 时间: {{.DATETIME}}

*/

import (
	"fmt"
	"gitee.com/ichub/goconfig/common/base/baseutils/jsonutils"
	"gitee.com/ichub/gowebcode/code/mysql/db/model"
	"gitee.com/ichub/gowebcode/code/mysql/db/service"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/suite"

	"github.com/gogf/gf/v2/util/gconv"
	"testing"
)

type Test{{.ModelName}}ServiceSuite struct {
	suite.Suite
    // basedao.BaseDao
	service * service.{{.ModelName}}Service
}

func Test{{.ModelName}}ServiceSuites(t *testing.T) {
	suite.Run(t, new(Test{{.ModelName}}ServiceSuite))
}

/*
@title     函数名称: Setup
@description      : 测试套前置脚本
@auth      作者: {{.Author}}
@date      时间: {{.DATETIME}}
@param     输入参数名:        无
@return    返回参数名:        无
*/
func (this *Test{{.ModelName}}ServiceSuite)  SetupTest() {

	logrus.Info("setUp all tests")
	// mysql postgres cockroach
    this.service = service.New{{.ModelName}}Service()
}
func (this *Test{{.ModelName}}ServiceSuite)  SetupSuite() {

	logrus.Info("setUp all tests")
	// mysql postgres cockroach
}

/*
@title           : 测试套后置脚本
@auth      作者: {{.Author}}
@description 函数名称: Teardown
@date      时间: {{.DATETIME}}
@param     输入参数名:        无
@return    返回参数名:        无
*/
func (this *Test{{.ModelName}}ServiceSuite)  TearDownTest() {
	logrus.Info("tearDown all tests")
}
func (this *Test{{.ModelName}}ServiceSuite)  TearDownSuite() {
	logrus.Info("tearDown all tests")
}

/*
@title     函数名称: Save
@description      : 保存接口   
@auth      作者    :  {{.Author}}
@date      时间    :  {{.DATETIME}}
@param     输入参数名:  t *testing.T
@return    返回参数名:  无
*/
func (this *Test{{.ModelName}}ServiceSuite) Save() {{.pkeyType}} {

	var entity model.{{.ModelName}}
	result, err := this.service.Save(&entity)
	logrus.Info(result)
	if err != nil {
		logrus.Errorf("save error: %s", err.Error())
		return entity.PkeyValue()
	}
	return entity.PkeyValue()
}

/*
@title     函数名称: SaveEntity
@description : 保存接口
@auth      作者    : {{.Author}}
@date      时间    : {{.DATETIME}}
@param     输入参数名: 无
@return    返回参数名: 无
*/
func (this *Test{{.ModelName}}ServiceSuite) SaveEntity(entity *model.{{.ModelName}}) {{.pkeyType}} {

	result , err  := this.service.Save(entity)
	logrus.Info(result)
	if err!=nil {
		logrus.Errorf("save error: %s", err.Error())
		return -1
	}
	return entity.PkeyValue()
}

/*
@title     函数名称: UpdateNotNull
@description      : 公共函数：更新功能
@auth      作者     :  {{.Author}}
@时间: {{.DATETIME}}
@param     输入参数名:  model.{{.ModelName}}
@return    返回参数名:  pkey {{.pkeyType}}
*/
func (this *Test{{.ModelName}}ServiceSuite) UpdateNotNull(entity *model.{{.ModelName}}) {{.pkeyType}} {

	//result := this.service.UpdateNotNull(&entity)
	//fmt.Println(result.String())
	return entity.{{.pkey}}

}

/*
@title     函数名称: Test001_Save
@description      : 测试接口-保存记录, 主键%s为nil或者为0新增, !=nil修改

@auth      作者    : {{.Author}}
@date      时间    : {{.DATETIME}}
@param     输入参数名:
@return    返回参数名:  无
*/
func (this *Test{{.ModelName}}ServiceSuite) Test001_Save() {
	logrus.Info("start Test001_Save...")
	var entity model.{{.ModelName}}
	pkey := this.SaveEntity(&entity)

	logrus.Info("pkey=", pkey)
	this.Equal(true, pkey == entity.PkeyValue())

}

/*
@title     函数名称: Test002_UpdateNotNull
@description      : 测试接口-更新非空字段,条件为主键Id

@auth      作者    :   {{.Author}}
@时间: {{.DATETIME}}
@param     输入参数名:
@return    返回参数名:  无
*/
func (this *Test{{.ModelName}}ServiceSuite) Test002_UpdateNotNull() {
	logrus.Info("start Test001_Save...")
	var entity model.{{.ModelName}}
	pkey := this.SaveEntity(&entity)

	logrus.Info("pkey=", pkey)
	this.Equal(true, pkey == entity.PkeyValue())
	this.service.Update(&entity)

}

/*
@title     函数名称: Test003_DeleteById
@description      : 测试接口-根据主键Id删除记录
@auth      作者    :   {{.Author}}
@时间: {{.DATETIME}}
@param     输入参数名:
@return    返回参数名:  无
*/
func (this *Test{{.ModelName}}ServiceSuite) Test003_DeleteById() {
	logrus.Info("start Test003_DeleteById ...")

	var pkey = this.Save()
	var err = this.service.DeleteById(pkey)
	if err != nil {
		logrus.Error("Fail to delete! ", err.Error())
	}
	this.Equal(false, err != nil)
	_, _notfound, err := this.service.FindById(pkey)
	logrus.Error(err)
	this.Equal(true, _notfound)

}

/*
@title     函数名称: Test004_FindById
@description      : 测试接口-根据主键Id查找记录
@auth      作者    : {{.Author}}
@date      时间    : {{.DATETIME}}
@param     输入参数名:
@return    返回参数名:  无
*/
func (this *Test{{.ModelName}}ServiceSuite) Test004_FindById() {

	logrus.Info("start Test004_FindById...")

	var entity = model.New{{.ModelName}}()
	this.SaveEntity(entity)

	res, notfound, err := this.service.FindById(entity.PkeyValue())
	if err != nil {
		fmt.Errorf(err.Error())
	}
	logrus.Info(jsonutils.ToJsonPretty(res))
	this.Equal(true, err == nil)
	this.Equal(false, notfound)

}

/*
@title     函数名称: Test005_FindByIds
@description      : 测试接口-根据主键查询多条记录, FindByIds("1,36,39")
@auth      作者    : {{.Author}}
@date      时间    : {{.DATETIME}}
@param     输入参数名:
@return    返回参数名:  无
*/
func (this *Test{{.ModelName}}ServiceSuite) Test005_FindByIds() {

	logrus.Info("start Test005_FindByIds...")

	var entity = model.New{{.ModelName}}()
	this.SaveEntity(entity)

	res, err := this.service.FindByIds(gconv.String(entity.PkeyValue()))
	if err != nil {
		fmt.Errorf(err.Error())
	}
	logrus.Info(jsonutils.ToJsonPretty(res))
	this.Equal(true, err == nil)
	this.Equal(1, len(*res))

}

/*
@title     函数名称: Test006_Query
@description      : 测试接口-通用查询
@auth      作者    :   {{.Author}}
@时间: {{.DATETIME}}
@param     输入参数名:
@return    返回参数名:  无
*/
func (this *Test{{.ModelName}}ServiceSuite) Test006_QueryTable() {
	logrus.Info("start Test006_Query...")

	entity := model.New{{.ModelName}}()

    this.SaveEntity(entity)
    var req = this.service.PageRequest
    req.PageSize = 2
    req.Eq(entity.PkeyName(), entity.PkeyValue())

    pageResult := this.service.QueryModel()
    logrus.Info(pageResult)
    this.Equal(200, pageResult.Code)
    this.Equal(1, len(pageResult.Data))

}

func (this *Test{{.ModelName}}ServiceSuite) Test007_QueryView() {
	logrus.Info("start Test007_QueryView...")


    pageResult := this.service.QueryModel()
    logrus.Info(pageResult)
    this.Equal(200, pageResult.Code)

}
