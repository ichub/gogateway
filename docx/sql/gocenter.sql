/*
Navicat MySQL Data Transfer

Source Server         : huawei.akunlong.top
Source Server Version : 50733
Source Host           : huawei.akunlong.top:13306
Source Database       : gocenter

Target Server Type    : MYSQL
Target Server Version : 50733
File Encoding         : 65001

Date: 2024-06-01 21:00:39
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for app
-- ----------------------------
DROP TABLE IF EXISTS `app`;
CREATE TABLE `app` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `app_id` varchar(64) NOT NULL DEFAULT 'default' COMMENT 'AppID',
  `name` varchar(500) NOT NULL DEFAULT 'default' COMMENT '应用名',
  `org_id` varchar(32) NOT NULL DEFAULT 'default' COMMENT '部门Id',
  `org_name` varchar(64) NOT NULL DEFAULT 'default' COMMENT '部门名字',
  `owner_name` varchar(500) NOT NULL DEFAULT 'default' COMMENT 'ownerName',
  `owner_email` varchar(500) NOT NULL DEFAULT 'default' COMMENT 'ownerEmail',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1: deleted, 0: normal',
  `deleted_at` date DEFAULT '0000-00-00' COMMENT 'Delete timestamp based on milliseconds',
  `created_by` varchar(64) NOT NULL DEFAULT 'default' COMMENT '创建人邮箱前缀',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `updated_by` varchar(64) DEFAULT '' COMMENT '最后修改人邮箱前缀',
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '最后修改时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_AppId_DeletedAt` (`app_id`,`deleted_at`),
  KEY `DataChange_LastTime` (`updated_by`),
  KEY `IX_Name` (`name`(191))
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4 COMMENT='应用表';

-- ----------------------------
-- Records of app
-- ----------------------------
INSERT INTO `app` VALUES ('1', 'dxp', 'bip-dxp', 'dev', '开发部门1', 'apollo', 'apollo@acme.com', '1', null, 'apollo', '2023-02-19 03:12:05', 'apollo', '2024-05-31 21:15:51');
INSERT INTO `app` VALUES ('2', 'bip-dxp', 'bip-dxp', 'dev', '样例部门1', 'apollo', 'apollo@acme.com', '1', null, 'apollo', '2023-02-22 07:05:58', 'apollo', '2024-05-31 21:15:51');
INSERT INTO `app` VALUES ('3', 'dxp-manager', 'dxp-manager', 'dev', '样例部门1', 'apollo', 'apollo@acme.com', '0', null, 'apollo', '2023-02-22 07:27:16', 'apollo', '2024-05-31 21:15:51');
INSERT INTO `app` VALUES ('4', 'dxp-openapi', 'dxp-openapi', 'dev', '样例部门1', 'apollo', 'apollo@acme.com', '1', null, 'apollo', '2023-02-22 08:18:13', 'apollo', '2024-05-31 21:15:51');
INSERT INTO `app` VALUES ('5', 'dxp-fileserver', 'dxp-fileserver', 'dev', '样例部门1', 'apollo', 'apollo@acme.com', '0', null, 'apollo', '2023-02-22 08:18:49', 'apollo', '2024-05-31 21:15:51');
INSERT INTO `app` VALUES ('6', 'dxp-icpserver', 'dxp-icpserver', 'dev', '样例部门1', 'apollo', 'apollo@acme.com', '0', null, 'apollo', '2023-02-22 08:19:28', 'apollo', '2024-05-31 21:15:51');
INSERT INTO `app` VALUES ('7', 'dxp-zuul', 'dxp-zuul', 'dev', '样例部门1', 'apollo', 'apollo@acme.com', '0', null, 'apollo', '2023-02-22 08:24:42', 'apollo', '2024-05-31 21:15:51');
INSERT INTO `app` VALUES ('8', 'dxp-none', 'dxp-none', 'dev', '样例部门1', 'apollo', 'apollo@acme.com', '1', null, 'apollo', '2023-02-22 08:35:00', 'apollo', '2024-05-31 21:15:51');
INSERT INTO `app` VALUES ('9', 'dxp-xxl-job-client', 'dxp-xxl-job-client', 'dev', '样例部门1', 'apollo', 'apollo@acme.com', '1', null, 'apollo', '2023-02-23 03:53:06', 'apollo', '2024-05-31 21:15:51');
INSERT INTO `app` VALUES ('10', 'dxp-mysql', 'dxp-mysql', 'dev', '样例部门1', 'apollo', 'apollo@acme.com', '1', null, 'apollo', '2023-02-24 07:08:44', 'apollo', '2024-05-31 21:15:51');
INSERT INTO `app` VALUES ('11', 'ruoyi-mysql', 'ruoyi-mysql', 'dev', '样例部门1', 'apollo', 'apollo@acme.com', '0', null, 'apollo', '2023-02-24 07:20:10', 'apollo', '2024-05-31 21:15:51');
INSERT INTO `app` VALUES ('12', 'c', 'c', 'dev', '开发部门1', 'apollo', 'apollo@acme.com', '0', null, 'apollo', '2023-03-31 10:32:17', 'apollo', '2024-05-31 21:09:19');
INSERT INTO `app` VALUES ('13', 'A', 'A', 'TEST1', '样例部门1', 'apollo', 'apollo@acme.com', '0', null, 'apollo', '2023-03-31 10:24:51', 'apollo', '2024-05-31 21:09:18');
INSERT INTO `app` VALUES ('14', '', '', '', '', '', '', '0', null, '', '2024-05-31 20:58:07', '', '2024-05-31 21:09:16');
INSERT INTO `app` VALUES ('18', '', '', '', '', '', '', '0', '0000-00-00', '', '2024-06-01 09:25:55', '', '2024-06-01 09:25:55');

-- ----------------------------
-- Table structure for app_namespace
-- ----------------------------
DROP TABLE IF EXISTS `app_namespace`;
CREATE TABLE `app_namespace` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增主键',
  `name` varchar(32) NOT NULL DEFAULT '' COMMENT 'namespace名字，注意，需要全局唯一',
  `app_id` varchar(64) NOT NULL DEFAULT '' COMMENT 'app id',
  `format` varchar(32) NOT NULL DEFAULT 'properties' COMMENT 'namespace的format类型',
  `is_public` bit(1) NOT NULL DEFAULT b'0' COMMENT 'namespace是否为公共',
  `comment` varchar(64) NOT NULL DEFAULT '' COMMENT '注释',
  `is_deleted` bit(1) NOT NULL DEFAULT b'0' COMMENT '1: deleted, 0: normal',
  `deleted_at` bigint(20) NOT NULL DEFAULT '0' COMMENT 'Delete timestamp based on milliseconds',
  `created_by` varchar(64) NOT NULL DEFAULT 'default' COMMENT '创建人邮箱前缀',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `updated_by` varchar(64) DEFAULT '' COMMENT '最后修改人邮箱前缀',
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '最后修改时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_AppId_Name_DeletedAt` (`app_id`,`name`,`deleted_at`),
  KEY `Name_AppId` (`name`,`app_id`),
  KEY `DataChange_LastTime` (`updated_at`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COMMENT='应用namespace定义';

-- ----------------------------
-- Records of app_namespace
-- ----------------------------
INSERT INTO `app_namespace` VALUES ('1', 'application', 'dxp', 'properties', '\0', 'default app namespace', '', '0', 'apollo', '2023-02-19 03:12:05', 'apollo', '2023-02-24 00:02:18');
INSERT INTO `app_namespace` VALUES ('2', 'TEST1.dxp', 'dxp', 'properties', '', '', '', '0', 'apollo', '2023-02-20 01:55:47', 'apollo', '2023-02-24 00:02:18');
INSERT INTO `app_namespace` VALUES ('3', 'bip', 'dxp', 'properties', '\0', '', '', '0', 'apollo', '2023-02-21 07:15:15', 'apollo', '2023-02-24 00:02:18');
INSERT INTO `app_namespace` VALUES ('4', 'kunlong.yml', 'dxp', 'yml', '', '', '', '0', 'apollo', '2023-02-21 07:15:55', 'apollo', '2023-02-24 00:02:18');
INSERT INTO `app_namespace` VALUES ('5', 'application', 'bip-dxp', 'properties', '\0', 'default app namespace', '', '0', 'apollo', '2023-02-22 07:05:58', 'apollo', '2023-02-24 00:01:52');
INSERT INTO `app_namespace` VALUES ('6', 'application', 'dxp-manager', 'properties', '\0', 'default app namespace', '\0', '0', 'apollo', '2023-02-22 07:27:16', 'apollo', '2023-02-22 07:27:16');
INSERT INTO `app_namespace` VALUES ('7', 'application', 'dxp-openapi', 'properties', '\0', 'default app namespace', '', '0', 'apollo', '2023-02-22 08:18:13', 'apollo', '2023-02-26 22:23:27');
INSERT INTO `app_namespace` VALUES ('8', 'application', 'dxp-fileserver', 'properties', '\0', 'default app namespace', '\0', '0', 'apollo', '2023-02-22 08:18:49', 'apollo', '2023-02-22 08:18:49');
INSERT INTO `app_namespace` VALUES ('9', 'application', 'dxp-icpserver', 'properties', '\0', 'default app namespace', '\0', '0', 'apollo', '2023-02-22 08:19:28', 'apollo', '2023-02-22 08:19:28');
INSERT INTO `app_namespace` VALUES ('10', 'application', 'dxp-zuul', 'properties', '\0', 'default app namespace', '\0', '0', 'apollo', '2023-02-22 08:24:42', 'apollo', '2023-02-22 08:24:42');
INSERT INTO `app_namespace` VALUES ('11', 'application', 'dxp-none', 'properties', '\0', 'default app namespace', '', '0', 'apollo', '2023-02-22 08:35:00', 'apollo', '2023-02-24 00:03:28');
INSERT INTO `app_namespace` VALUES ('12', 'application', 'dxp-xxl-job-client', 'properties', '\0', 'default app namespace', '', '0', 'apollo', '2023-02-23 03:53:06', 'apollo', '2023-02-24 00:02:56');
INSERT INTO `app_namespace` VALUES ('13', 'xxl-job-client', 'dxp-manager', 'properties', '', '', '\0', '0', 'apollo', '2023-02-23 06:00:58', 'apollo', '2023-02-23 06:00:58');
INSERT INTO `app_namespace` VALUES ('14', 'application', 'dxp-mysql', 'properties', '\0', 'default app namespace', '', '0', 'apollo', '2023-02-24 07:08:44', 'apollo', '2023-02-24 21:19:44');
INSERT INTO `app_namespace` VALUES ('15', 'ruoyi-mysql', 'dxp-mysql', 'properties', '', '', '', '0', 'apollo', '2023-02-24 07:12:20', 'apollo', '2023-02-24 21:19:44');
INSERT INTO `app_namespace` VALUES ('16', 'application', 'ruoyi-mysql', 'properties', '\0', 'default app namespace', '\0', '0', 'apollo', '2023-02-24 07:20:10', 'apollo', '2023-02-24 07:20:10');
INSERT INTO `app_namespace` VALUES ('17', 'ruoyi-mysql', 'ruoyi-mysql', 'properties', '', '', '\0', '0', 'apollo', '2023-02-24 07:20:33', 'apollo', '2023-02-24 07:20:33');
INSERT INTO `app_namespace` VALUES ('18', 'application', 'c', 'properties', '\0', 'default app namespace', '\0', '0', 'apollo', '2023-03-31 10:32:17', 'apollo', '2023-03-31 10:32:17');
INSERT INTO `app_namespace` VALUES ('19', 'application', 'A', 'properties', '\0', 'default app namespace', '\0', '0', 'apollo', '2023-03-31 10:37:53', 'apollo', '2023-03-31 10:37:53');
INSERT INTO `app_namespace` VALUES ('20', 'a', 'c', 'properties', '', '', '\0', '0', 'apollo', '2023-03-31 10:38:41', 'apollo', '2023-03-31 10:38:41');

-- ----------------------------
-- Table structure for env
-- ----------------------------
DROP TABLE IF EXISTS `env`;
CREATE TABLE `env` (
  ` id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL COMMENT '环境',
  PRIMARY KEY (` id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of env
-- ----------------------------
INSERT INTO `env` VALUES ('1', '   test');

-- ----------------------------
-- Table structure for instance
-- ----------------------------
DROP TABLE IF EXISTS `instance`;
CREATE TABLE `instance` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增Id',
  `app_id` varchar(64) NOT NULL DEFAULT 'default' COMMENT 'AppID',
  `cluster_name` varchar(32) NOT NULL DEFAULT 'default' COMMENT 'ClusterName',
  `data_center` varchar(64) NOT NULL DEFAULT 'default' COMMENT 'Data Center Name',
  `ip` varchar(32) NOT NULL DEFAULT '' COMMENT 'instance ip',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '最后修改时间',
  `env_id` int(11) DEFAULT NULL,
  `tenant_id` bigint(20) DEFAULT NULL COMMENT '租户',
  PRIMARY KEY (`id`),
  UNIQUE KEY `IX_UNIQUE_KEY` (`app_id`,`cluster_name`,`ip`,`data_center`),
  KEY `IX_IP` (`ip`),
  KEY `IX_updated_at` (`updated_at`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COMMENT='使用配置的应用实例';

-- ----------------------------
-- Records of instance
-- ----------------------------

-- ----------------------------
-- Table structure for item
-- ----------------------------
DROP TABLE IF EXISTS `item`;
CREATE TABLE `item` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增Id',
  `namespace_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '集群NamespaceId',
  `key` varchar(128) NOT NULL DEFAULT 'default' COMMENT '配置项Key',
  `type` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '配置项类型，0: String，1: Number，2: Boolean，3: JSON',
  `value` longtext NOT NULL COMMENT '配置项值',
  `comment` varchar(1024) DEFAULT '' COMMENT '注释',
  `line_num` int(10) unsigned DEFAULT '0' COMMENT '行号',
  `is_deleted` bit(1) NOT NULL DEFAULT b'0' COMMENT '1: deleted, 0: normal',
  `deleted_at` bigint(20) NOT NULL DEFAULT '0' COMMENT 'Delete timestamp based on milliseconds',
  `ceated_by` varchar(64) NOT NULL DEFAULT 'default' COMMENT '创建人邮箱前缀',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `updated_by` varchar(64) DEFAULT '' COMMENT '最后修改人邮箱前缀',
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '最后修改时间',
  PRIMARY KEY (`id`),
  KEY `IX_GroupId` (`namespace_id`)
) ENGINE=InnoDB AUTO_INCREMENT=141 DEFAULT CHARSET=utf8mb4 COMMENT='配置项目';

-- ----------------------------
-- Records of item
-- ----------------------------
INSERT INTO `item` VALUES ('1', '1', 'app.id', '0', 'dxp', null, '1', '', '0', 'apollo', '2023-02-19 03:12:26', 'apollo', '2023-02-20 07:43:13');
INSERT INTO `item` VALUES ('2', '1', 'spring.cloud.sentinel.enabled', '0', 'true', '', '2', '', '0', 'apollo', '2023-02-19 03:15:31', 'apollo', '2023-02-20 07:39:54');
INSERT INTO `item` VALUES ('3', '1', 'spring.cloud.sentinel.transport.port', '0', '8719', '', '3', '', '0', 'apollo', '2023-02-19 03:15:31', 'apollo', '2023-02-20 07:39:54');
INSERT INTO `item` VALUES ('4', '1', 'spring.cloud.sentinel.transport.dashboard', '0', '${SENTINEL_HOST:192.168.1.92}:8858', '', '4', '', '0', 'apollo', '2023-02-19 03:15:31', 'apollo', '2023-02-20 07:39:54');
INSERT INTO `item` VALUES ('5', '1', '', '0', '', '', '5', '', '0', 'apollo', '2023-02-19 03:15:31', 'apollo', '2023-02-22 06:29:08');
INSERT INTO `item` VALUES ('6', '1', '', '0', '', '#feign.httpclient.enabled=true', '6', '', '0', 'apollo', '2023-02-19 03:15:31', 'apollo', '2023-02-20 07:39:54');
INSERT INTO `item` VALUES ('7', '1', 'server.port', '0', '9610', '', '7', '', '0', 'apollo', '2023-02-19 03:15:31', 'apollo', '2023-02-20 07:27:38');
INSERT INTO `item` VALUES ('8', '1', 'spring.application.name', '0', 'pf-baseinfo', '', '8', '', '0', 'apollo', '2023-02-19 03:15:31', 'apollo', '2023-02-20 07:39:54');
INSERT INTO `item` VALUES ('9', '1', 'spring.profiles.active', '0', '${PROFILE:dev}', '', '3', '', '0', 'apollo', '2023-02-19 03:15:31', 'apollo', '2023-02-20 07:43:23');
INSERT INTO `item` VALUES ('10', '1', 'spring.cloud.nacos.discovery.server-addr', '0', '${NACOS_HOST:192.168.1.92}:8848', '', '10', '', '0', 'apollo', '2023-02-19 03:15:31', 'apollo', '2023-02-20 07:39:54');
INSERT INTO `item` VALUES ('11', '1', 'spring.cloud.nacos.discovery.register-enabled', '0', 'true', '', '11', '', '0', 'apollo', '2023-02-19 03:15:31', 'apollo', '2023-02-20 07:39:54');
INSERT INTO `item` VALUES ('12', '1', 'spring.cloud.nacos.config.server-addr', '0', '${NACOS_HOST:192.168.1.92}:8848', '', '12', '', '0', 'apollo', '2023-02-19 03:15:31', 'apollo', '2023-02-20 07:39:54');
INSERT INTO `item` VALUES ('13', '1', '', '0', '', '', '13', '', '0', 'apollo', '2023-02-19 03:15:31', 'apollo', '2023-02-24 00:00:15');
INSERT INTO `item` VALUES ('14', '1', '', '0', '', '', '14', '', '0', 'apollo', '2023-02-19 03:15:31', 'apollo', '2023-02-24 00:00:15');
INSERT INTO `item` VALUES ('15', '1', '', '0', '', '#nacosä¸æå°å®¢æ·ç«¯å¿è·³æ¥å¿', '15', '', '0', 'apollo', '2023-02-19 03:15:31', 'apollo', '2023-02-20 07:39:54');
INSERT INTO `item` VALUES ('16', '1', 'logging.level.com.alibaba.nacos.client.naming', '0', 'error', '', '16', '', '0', 'apollo', '2023-02-19 03:15:31', 'apollo', '2023-02-20 07:39:54');
INSERT INTO `item` VALUES ('17', '1', '', '0', '', '#å½éå°ç¸ååå­çæ¶åæ¯å¦åè®¸è¦çæ³¨å', '17', '', '0', 'apollo', '2023-02-19 03:15:31', 'apollo', '2023-02-20 07:39:54');
INSERT INTO `item` VALUES ('18', '1', 'spring.main.allow-bean-definition-overriding', '0', 'true', '', '18', '', '0', 'apollo', '2023-02-19 03:15:31', 'apollo', '2023-02-20 07:39:54');
INSERT INTO `item` VALUES ('19', '1', '', '0', '', '', '19', '', '0', 'apollo', '2023-02-19 03:15:31', 'apollo', '2023-02-24 00:00:15');
INSERT INTO `item` VALUES ('20', '1', 'mybatis-plus.global-config.sql-parser-cache', '0', 'true', '', '20', '', '0', 'apollo', '2023-02-19 03:15:31', 'apollo', '2023-02-20 07:39:54');
INSERT INTO `item` VALUES ('21', '1', 'mybatis-plus.configuration.log-impl', '0', 'org.apache.ibatis.logging.stdout.StdOutImpl', '', '21', '', '0', 'apollo', '2023-02-19 03:15:31', 'apollo', '2023-02-20 07:39:54');
INSERT INTO `item` VALUES ('22', '1', 'spring.jta.log-dir', '0', 'data/logs/jta/base/${spring.application.name}-\'%d{yyyy-MM-dd}\'.%i.log', '', '22', '', '0', 'apollo', '2023-02-19 03:15:31', 'apollo', '2023-02-20 07:39:54');
INSERT INTO `item` VALUES ('23', '1', 'ocean.mysql.column', '0', 'tenant_id', '', '23', '', '0', 'apollo', '2023-02-19 03:15:31', 'apollo', '2023-02-20 07:39:54');
INSERT INTO `item` VALUES ('24', '1', 'ocean.mysql.enable', '0', 'true', '', '24', '', '0', 'apollo', '2023-02-19 03:15:31', 'apollo', '2023-02-20 07:39:54');
INSERT INTO `item` VALUES ('25', '1', 'spring.jta.atomikos.properties.log-base-name', '0', '${spring.application.name}', '', '25', '', '0', 'apollo', '2023-02-19 03:15:31', 'apollo', '2023-02-20 07:39:54');
INSERT INTO `item` VALUES ('26', '1', 'management.endpoints.web.exposure.include', '0', '*', '', '26', '', '0', 'apollo', '2023-02-19 03:15:31', 'apollo', '2023-02-20 07:39:54');
INSERT INTO `item` VALUES ('27', '1', 'management.endpoint.health.show-details', '0', 'always', '', '27', '', '0', 'apollo', '2023-02-19 03:15:31', 'apollo', '2023-02-20 07:39:54');
INSERT INTO `item` VALUES ('28', '1', '', '0', '', '', '28', '', '0', 'apollo', '2023-02-19 03:15:31', 'apollo', '2023-02-24 00:00:15');
INSERT INTO `item` VALUES ('29', '1', 'fdfs.resHost', '0', 'http://14.29.241.30', '', '29', '', '0', 'apollo', '2023-02-19 03:15:31', 'apollo', '2023-02-20 07:39:54');
INSERT INTO `item` VALUES ('30', '1', 'fdfs.storagePort', '0', '9999', '', '30', '', '0', 'apollo', '2023-02-19 03:15:31', 'apollo', '2023-02-20 07:39:54');
INSERT INTO `item` VALUES ('31', '2', 'dxp.server.ip', '0', '127.0.0.1', '', '1', '', '0', 'apollo', '2023-02-20 01:56:36', 'apollo', '2023-02-24 00:00:50');
INSERT INTO `item` VALUES ('32', '2', 'dxp.server.port', '0', '12345', '', '2', '', '0', 'apollo', '2023-02-20 01:56:36', 'apollo', '2023-02-24 00:00:50');
INSERT INTO `item` VALUES ('33', '1', '', '0', '', '#feign.httpclient.enabled=true', '2', '', '0', 'apollo', '2023-02-20 07:39:54', 'apollo', '2023-02-21 06:46:12');
INSERT INTO `item` VALUES ('34', '1', '', '0', '', '', '4', '', '0', 'apollo', '2023-02-20 07:39:54', 'apollo', '2023-02-21 06:46:12');
INSERT INTO `item` VALUES ('35', '1', 'test', '0', '11', null, '7', '', '0', 'apollo', '2023-02-20 07:43:36', 'apollo', '2023-02-24 00:00:15');
INSERT INTO `item` VALUES ('36', '1', '', '0', '', '#feign.httpclient.enabled=true', '1', '', '0', 'apollo', '2023-02-21 06:46:12', 'apollo', '2023-02-24 00:00:15');
INSERT INTO `item` VALUES ('37', '1', '', '0', '', '', '2', '', '0', 'apollo', '2023-02-21 06:46:12', 'apollo', '2023-02-24 00:00:15');
INSERT INTO `item` VALUES ('38', '1', 'dxp.server.ip', '0', '127.0.0.1', '', '3', '', '0', 'apollo', '2023-02-21 06:46:12', 'apollo', '2023-02-24 00:00:15');
INSERT INTO `item` VALUES ('39', '1', 'dxp.server.port', '0', '12345', '', '4', '', '0', 'apollo', '2023-02-21 06:46:12', 'apollo', '2023-02-24 00:00:15');
INSERT INTO `item` VALUES ('40', '4', 'dxp.server.port', '0', '12345', '', '1', '', '0', 'apollo', '2023-02-21 07:03:43', 'apollo', '2023-02-24 00:02:18');
INSERT INTO `item` VALUES ('41', '4', 'dxp.server.ip', '0', '127.0.0.1', '', '2', '', '0', 'apollo', '2023-02-21 07:03:43', 'apollo', '2023-02-24 00:02:18');
INSERT INTO `item` VALUES ('42', '3', 'dxp.server.port', '0', '12345', '', '1', '', '0', 'apollo', '2023-02-21 07:04:49', 'apollo', '2023-02-24 00:02:18');
INSERT INTO `item` VALUES ('43', '3', 'dxp.server.ip', '0', '127.0.0.1', '', '2', '', '0', 'apollo', '2023-02-21 07:04:49', 'apollo', '2023-02-24 00:02:18');
INSERT INTO `item` VALUES ('44', '1', 'dxp.ftp.ip', '0', '127.0.0.1', '', '5', '', '0', 'apollo', '2023-02-22 06:29:08', 'apollo', '2023-02-24 00:00:15');
INSERT INTO `item` VALUES ('45', '1', 'dxp.ftp.port', '0', '22', '', '6', '', '0', 'apollo', '2023-02-22 06:29:08', 'apollo', '2023-02-24 00:00:15');
INSERT INTO `item` VALUES ('46', '9', '', '0', '', '#feign.httpclient.enabled=true', '1', '', '0', 'apollo', '2023-02-22 07:06:33', 'apollo', '2023-02-24 00:01:52');
INSERT INTO `item` VALUES ('47', '9', '', '0', '', '', '2', '', '0', 'apollo', '2023-02-22 07:06:33', 'apollo', '2023-02-22 07:21:37');
INSERT INTO `item` VALUES ('48', '9', 'dxp.server.ip', '0', '127.0.0.1', '', '3', '', '0', 'apollo', '2023-02-22 07:06:33', 'apollo', '2023-02-23 09:57:24');
INSERT INTO `item` VALUES ('49', '9', 'dxp.server.port', '0', '2345', '', '4', '', '0', 'apollo', '2023-02-22 07:06:33', 'apollo', '2023-02-23 09:57:24');
INSERT INTO `item` VALUES ('50', '9', 'dxp.ftp.ip', '0', '127.0.0.1', '', '5', '', '0', 'apollo', '2023-02-22 07:06:33', 'apollo', '2023-02-23 09:57:24');
INSERT INTO `item` VALUES ('51', '9', 'dxp.ftp.port', '0', '22', '', '6', '', '0', 'apollo', '2023-02-22 07:06:33', 'apollo', '2023-02-23 09:57:24');
INSERT INTO `item` VALUES ('52', '9', 'test', '0', '11', '', '7', '', '0', 'apollo', '2023-02-22 07:06:33', 'apollo', '2023-02-22 07:21:37');
INSERT INTO `item` VALUES ('53', '10', 'dxp.server.ip', '0', '127.0.0.1', '', '1', '', '0', 'apollo', '2023-02-22 07:08:29', 'apollo', '2023-02-24 00:01:52');
INSERT INTO `item` VALUES ('54', '10', 'dxp.server.port', '0', '12345', '', '2', '', '0', 'apollo', '2023-02-22 07:08:29', 'apollo', '2023-02-24 00:01:52');
INSERT INTO `item` VALUES ('55', '10', 'dxp.ftp.ip', '0', '127.0.0.1', '', '3', '', '0', 'apollo', '2023-02-22 07:08:29', 'apollo', '2023-02-24 00:01:52');
INSERT INTO `item` VALUES ('56', '10', 'dxp.ftp.port', '0', '22', '', '4', '', '0', 'apollo', '2023-02-22 07:08:29', 'apollo', '2023-02-24 00:01:52');
INSERT INTO `item` VALUES ('57', '10', 'test', '0', '11', '', '5', '', '0', 'apollo', '2023-02-22 07:08:29', 'apollo', '2023-02-24 00:01:52');
INSERT INTO `item` VALUES ('58', '9', 'apollo.meta', '0', 'http://${APOLLO_META:192.168.1.192:8080}', '', '2', '', '0', 'apollo', '2023-02-22 07:21:37', 'apollo', '2023-02-24 00:01:52');
INSERT INTO `item` VALUES ('59', '9', '', '0', '', '#test = 11', '7', '', '0', 'apollo', '2023-02-22 07:21:37', 'apollo', '2023-02-24 00:01:52');
INSERT INTO `item` VALUES ('60', '11', 'server.port', '0', '9701', '', '2', '\0', '0', 'apollo', '2023-02-22 07:29:26', 'apollo', '2023-03-08 08:46:12');
INSERT INTO `item` VALUES ('61', '11', 'server.servlet.context-path', '0', '/', '', '3', '\0', '0', 'apollo', '2023-02-22 07:29:26', 'apollo', '2023-03-08 08:46:12');
INSERT INTO `item` VALUES ('62', '11', 'dxp.server.ip', '0', '127.0.0.1', '', '3', '', '0', 'apollo', '2023-02-22 07:30:49', 'apollo', '2023-02-23 09:52:26');
INSERT INTO `item` VALUES ('63', '11', 'dxp.server.port', '0', '6666', '', '4', '', '0', 'apollo', '2023-02-22 07:30:49', 'apollo', '2023-02-23 09:52:26');
INSERT INTO `item` VALUES ('64', '11', 'dxp.ftp.ip', '0', '127.0.0.1', '', '5', '', '0', 'apollo', '2023-02-22 07:37:48', 'apollo', '2023-02-23 09:52:26');
INSERT INTO `item` VALUES ('65', '11', 'dxp.ftp.port', '0', '22', '', '6', '', '0', 'apollo', '2023-02-22 07:37:48', 'apollo', '2023-02-23 09:52:26');
INSERT INTO `item` VALUES ('66', '12', 'server.port', '0', '9701', '', '1', '\0', '0', 'apollo', '2023-02-22 07:46:41', 'apollo', '2023-02-22 07:46:41');
INSERT INTO `item` VALUES ('67', '12', 'dxp.ftp.ip', '0', '127.0.0.1', '', '2', '\0', '0', 'apollo', '2023-02-22 07:46:41', 'apollo', '2023-02-22 07:46:41');
INSERT INTO `item` VALUES ('68', '12', 'dxp.ftp.port', '0', '22', '', '3', '\0', '0', 'apollo', '2023-02-22 07:46:41', 'apollo', '2023-02-22 07:46:41');
INSERT INTO `item` VALUES ('69', '12', 'dxp.server.ip', '0', '127.0.0.1', '', '4', '\0', '0', 'apollo', '2023-02-22 07:46:41', 'apollo', '2023-02-22 07:46:41');
INSERT INTO `item` VALUES ('70', '12', 'dxp.server.port', '0', '6666', '', '5', '\0', '0', 'apollo', '2023-02-22 07:46:41', 'apollo', '2023-02-22 07:46:41');
INSERT INTO `item` VALUES ('71', '12', 'server.servlet.context-path', '0', '/', '', '6', '\0', '0', 'apollo', '2023-02-22 07:46:41', 'apollo', '2023-02-22 08:21:19');
INSERT INTO `item` VALUES ('72', '16', 'ignore_urls', '0', '/', '', '1', '\0', '0', 'apollo', '2023-02-22 08:25:20', 'apollo', '2023-02-22 08:25:20');
INSERT INTO `item` VALUES ('73', '18', 'xxl.job.admin.addresses', '0', 'http://${XXL_JOB_ADMIN_HOST:192.168.1.92}:${XXL_JOB_ADMIN_PORT:9680}/xxl-job-admin', '', '1', '', '0', 'apollo', '2023-02-23 03:53:44', 'apollo', '2023-02-24 00:02:56');
INSERT INTO `item` VALUES ('74', '18', '', '0', '', '### xxl-job, access token', '2', '', '0', 'apollo', '2023-02-23 03:53:44', 'apollo', '2023-02-24 00:02:56');
INSERT INTO `item` VALUES ('75', '18', 'xxl.job.accessToken', '0', 'default_token', '', '3', '', '0', 'apollo', '2023-02-23 03:53:44', 'apollo', '2023-02-24 00:02:56');
INSERT INTO `item` VALUES ('76', '18', '', '0', '', '', '4', '', '0', 'apollo', '2023-02-23 03:53:44', 'apollo', '2023-02-24 00:02:56');
INSERT INTO `item` VALUES ('77', '18', '', '0', '', '### xxl-job executor appname', '5', '', '0', 'apollo', '2023-02-23 03:53:44', 'apollo', '2023-02-24 00:02:56');
INSERT INTO `item` VALUES ('78', '18', 'xxl.job.executor.appname', '0', 'xxl-job-manager', '', '6', '', '0', 'apollo', '2023-02-23 03:53:44', 'apollo', '2023-02-24 00:02:56');
INSERT INTO `item` VALUES ('79', '18', '', '0', '', '### xxl-job executor registry-address: default use address to registry , otherwise use ip:port if address is null', '7', '', '0', 'apollo', '2023-02-23 03:53:44', 'apollo', '2023-02-24 00:02:56');
INSERT INTO `item` VALUES ('80', '18', 'xxl.job.executor.address', '0', '', '', '8', '', '0', 'apollo', '2023-02-23 03:53:44', 'apollo', '2023-02-24 00:02:56');
INSERT INTO `item` VALUES ('81', '18', '', '0', '', '### xxl-job executor server-info', '9', '', '0', 'apollo', '2023-02-23 03:53:44', 'apollo', '2023-02-24 00:02:56');
INSERT INTO `item` VALUES ('82', '18', 'xxl.job.executor.ip', '0', '', '', '10', '', '0', 'apollo', '2023-02-23 03:53:44', 'apollo', '2023-02-24 00:02:56');
INSERT INTO `item` VALUES ('83', '18', 'xxl.job.executor.port', '0', '9980', '', '11', '', '0', 'apollo', '2023-02-23 03:53:44', 'apollo', '2023-02-24 00:02:56');
INSERT INTO `item` VALUES ('84', '18', '', '0', '', '### xxl-job executor log-path', '12', '', '0', 'apollo', '2023-02-23 03:53:44', 'apollo', '2023-02-24 00:02:56');
INSERT INTO `item` VALUES ('85', '18', 'xxl.job.executor.logpath', '0', '/data/applogs/xxl-job/jobhandler', '', '13', '', '0', 'apollo', '2023-02-23 03:53:44', 'apollo', '2023-02-24 00:02:56');
INSERT INTO `item` VALUES ('86', '18', '', '0', '', '### xxl-job executor log-retention-days', '14', '', '0', 'apollo', '2023-02-23 03:53:44', 'apollo', '2023-02-24 00:02:56');
INSERT INTO `item` VALUES ('87', '18', 'xxl.job.executor.logretentiondays', '0', '30', '', '15', '', '0', 'apollo', '2023-02-23 03:53:44', 'apollo', '2023-02-24 00:02:56');
INSERT INTO `item` VALUES ('88', '19', 'xxl.job.admin.addresses', '0', 'http://${XXL_JOB_ADMIN_HOST:10.18.41.192}:${XXL_JOB_ADMIN_PORT:9680}/xxl-job-admin', '', '1', '\0', '0', 'apollo', '2023-02-23 06:01:58', 'apollo', '2023-02-23 07:27:11');
INSERT INTO `item` VALUES ('89', '19', '', '0', '', '### xxl-job, access token', '2', '\0', '0', 'apollo', '2023-02-23 06:01:58', 'apollo', '2023-02-23 06:01:58');
INSERT INTO `item` VALUES ('90', '19', 'xxl.job.accessToken', '0', 'default_token', '', '3', '\0', '0', 'apollo', '2023-02-23 06:01:58', 'apollo', '2023-02-23 06:01:58');
INSERT INTO `item` VALUES ('91', '19', '', '0', '', '', '4', '\0', '0', 'apollo', '2023-02-23 06:01:58', 'apollo', '2023-02-23 06:01:58');
INSERT INTO `item` VALUES ('92', '19', '', '0', '', '### xxl-job executor appname', '5', '\0', '0', 'apollo', '2023-02-23 06:01:58', 'apollo', '2023-02-23 06:01:58');
INSERT INTO `item` VALUES ('93', '19', 'xxl.job.executor.appname', '0', 'xxl-job-manager', '', '6', '\0', '0', 'apollo', '2023-02-23 06:01:58', 'apollo', '2023-02-23 06:01:58');
INSERT INTO `item` VALUES ('94', '19', '', '0', '', '### xxl-job executor registry-address: default use address to registry , otherwise use ip:port if address is null', '7', '\0', '0', 'apollo', '2023-02-23 06:01:58', 'apollo', '2023-02-23 06:01:58');
INSERT INTO `item` VALUES ('95', '19', 'xxl.job.executor.address', '0', '', '', '8', '\0', '0', 'apollo', '2023-02-23 06:01:58', 'apollo', '2023-02-23 06:01:58');
INSERT INTO `item` VALUES ('96', '19', '', '0', '', '### xxl-job executor server-info', '9', '\0', '0', 'apollo', '2023-02-23 06:01:58', 'apollo', '2023-02-23 06:01:58');
INSERT INTO `item` VALUES ('97', '19', 'xxl.job.executor.ip', '0', '', '', '10', '\0', '0', 'apollo', '2023-02-23 06:01:58', 'apollo', '2023-02-23 06:01:58');
INSERT INTO `item` VALUES ('98', '19', 'xxl.job.executor.port', '0', '9771', '', '11', '\0', '0', 'apollo', '2023-02-23 06:01:58', 'apollo', '2023-02-23 07:48:35');
INSERT INTO `item` VALUES ('99', '19', '', '0', '', '### xxl-job executor log-path', '12', '\0', '0', 'apollo', '2023-02-23 06:01:58', 'apollo', '2023-02-23 06:01:58');
INSERT INTO `item` VALUES ('100', '19', 'xxl.job.executor.logpath', '0', './logs/xxl-job/jobhandler', '', '13', '\0', '0', 'apollo', '2023-02-23 06:01:58', 'apollo', '2023-02-23 07:43:36');
INSERT INTO `item` VALUES ('101', '19', '', '0', '', '### xxl-job executor log-retention-days', '14', '\0', '0', 'apollo', '2023-02-23 06:01:58', 'apollo', '2023-02-23 06:01:58');
INSERT INTO `item` VALUES ('102', '19', 'xxl.job.executor.logretentiondays', '0', '30', '', '15', '\0', '0', 'apollo', '2023-02-23 06:01:58', 'apollo', '2023-02-23 06:01:58');
INSERT INTO `item` VALUES ('103', '11', 'dxp.properties.server.ip', '0', '127.0.0.1', '', '3', '', '0', 'apollo', '2023-02-23 09:52:26', 'apollo', '2023-02-26 08:24:55');
INSERT INTO `item` VALUES ('104', '11', 'dxp.properties.server.port', '0', '6666', '', '4', '', '0', 'apollo', '2023-02-23 09:52:26', 'apollo', '2023-02-26 08:24:55');
INSERT INTO `item` VALUES ('105', '11', 'dxp.properties.ftp.ip', '0', '127.0.0.1', '', '5', '', '0', 'apollo', '2023-02-23 09:52:26', 'apollo', '2023-02-26 08:24:55');
INSERT INTO `item` VALUES ('106', '11', 'dxp.properties.ftp.port', '0', '22', '', '6', '', '0', 'apollo', '2023-02-23 09:52:26', 'apollo', '2023-02-26 08:24:55');
INSERT INTO `item` VALUES ('107', '9', 'dxp.properties.server.ip', '0', '127.0.0.1', '', '3', '', '0', 'apollo', '2023-02-23 09:57:24', 'apollo', '2023-02-24 00:01:52');
INSERT INTO `item` VALUES ('108', '9', 'dxp.properties.server.port', '0', '2345', '', '4', '', '0', 'apollo', '2023-02-23 09:57:24', 'apollo', '2023-02-24 00:01:52');
INSERT INTO `item` VALUES ('109', '9', 'dxp.properties.ftp.ip', '0', '127.0.0.1', '', '5', '', '0', 'apollo', '2023-02-23 09:57:24', 'apollo', '2023-02-24 00:01:52');
INSERT INTO `item` VALUES ('110', '9', 'dxp.properties.ftp.port', '0', '22', '', '6', '', '0', 'apollo', '2023-02-23 09:57:24', 'apollo', '2023-02-24 00:01:52');
INSERT INTO `item` VALUES ('111', '24', '', '0', '', '', '1', '\0', '0', 'apollo', '2023-02-24 07:22:34', 'apollo', '2023-02-24 07:22:34');
INSERT INTO `item` VALUES ('112', '24', 'spring.datasource.druid.ruoyi.url', '0', 'jdbc:mysql://192.168.1.128:3306/ry-vue?autoReconnect=true&autoReconnectForPools=true&failOverReadOnly=false&useUnicode=true&characterEncoding=UTF-8&useSSL=false&allowMultiQueries=true&serverTimezone=GMT%2B8', '', '2', '\0', '0', 'apollo', '2023-02-24 07:22:34', 'apollo', '2023-02-24 07:22:34');
INSERT INTO `item` VALUES ('113', '24', 'spring.datasource.druid.ruoyi.driverClass', '0', 'com.mysql.jdbc.Driver', '', '3', '\0', '0', 'apollo', '2023-02-24 07:22:34', 'apollo', '2023-02-24 07:22:34');
INSERT INTO `item` VALUES ('114', '24', 'spring.datasource.druid.ruoyi.username', '0', 'root', '', '4', '\0', '0', 'apollo', '2023-02-24 07:22:34', 'apollo', '2023-02-24 07:22:34');
INSERT INTO `item` VALUES ('115', '24', 'spring.datasource.druid.ruoyi.password', '0', 'root', '', '5', '\0', '0', 'apollo', '2023-02-24 07:22:34', 'apollo', '2023-02-24 07:22:34');
INSERT INTO `item` VALUES ('116', '24', 'spring.datasource.druid.ruoyi.testOnBorrow', '0', 'false', '', '6', '\0', '0', 'apollo', '2023-02-24 07:22:34', 'apollo', '2023-02-24 07:22:34');
INSERT INTO `item` VALUES ('117', '24', 'spring.datasource.druid.ruoyi.testWhileIdle', '0', 'true', '', '7', '\0', '0', 'apollo', '2023-02-24 07:22:34', 'apollo', '2023-02-24 07:22:34');
INSERT INTO `item` VALUES ('118', '24', '', '0', '', '', '8', '\0', '0', 'apollo', '2023-02-24 07:22:34', 'apollo', '2023-02-24 07:22:34');
INSERT INTO `item` VALUES ('119', '24', 'spring.datasource.druid.ruoyi.initialSize', '0', '2', '', '9', '\0', '0', 'apollo', '2023-02-24 07:22:34', 'apollo', '2023-02-24 07:22:34');
INSERT INTO `item` VALUES ('120', '24', 'spring.datasource.druid.ruoyi.minIdle', '0', '2', '', '10', '\0', '0', 'apollo', '2023-02-24 07:22:34', 'apollo', '2023-02-24 07:22:34');
INSERT INTO `item` VALUES ('121', '24', 'spring.datasource.druid.ruoyi.maxActive', '0', '50', '', '11', '\0', '0', 'apollo', '2023-02-24 07:22:34', 'apollo', '2023-02-24 07:22:34');
INSERT INTO `item` VALUES ('122', '24', 'spring.datasource.druid.ruoyi.testOnReturn', '0', 'false', '', '12', '\0', '0', 'apollo', '2023-02-24 07:22:34', 'apollo', '2023-02-24 07:22:34');
INSERT INTO `item` VALUES ('123', '24', 'spring.datasource.druid.ruoyi.validationQuery', '0', 'SELECT 1', '', '13', '\0', '0', 'apollo', '2023-02-24 07:22:34', 'apollo', '2023-02-24 07:22:34');
INSERT INTO `item` VALUES ('124', '24', 'spring.datasource.druid.ruoyi.timeBetweenEvictionRunsMillis', '0', '20000', '', '14', '\0', '0', 'apollo', '2023-02-24 07:22:34', 'apollo', '2023-02-24 07:22:34');
INSERT INTO `item` VALUES ('125', '11', 'dxp.props.server.ip', '0', '127.0.0.1', '', '4', '\0', '0', 'apollo', '2023-02-26 08:24:55', 'apollo', '2023-03-08 08:46:12');
INSERT INTO `item` VALUES ('126', '11', 'dxp.props.server.port', '0', '6666', '', '5', '\0', '0', 'apollo', '2023-02-26 08:24:55', 'apollo', '2023-03-08 08:46:12');
INSERT INTO `item` VALUES ('127', '11', 'dxp.props.ftp.ip', '0', '10.18.41.192', '', '6', '\0', '0', 'apollo', '2023-02-26 08:24:55', 'apollo', '2023-03-08 08:46:12');
INSERT INTO `item` VALUES ('128', '11', 'dxp.props.ftp.port', '0', '21', '', '7', '\0', '0', 'apollo', '2023-02-26 08:24:55', 'apollo', '2023-03-08 08:46:12');
INSERT INTO `item` VALUES ('129', '11', 'dxp.props.ftp.user', '0', 'leijmdas', '', '8', '\0', '0', 'apollo', '2023-03-02 05:57:09', 'apollo', '2023-03-08 08:46:12');
INSERT INTO `item` VALUES ('130', '11', 'dxp.props.ftp.password', '0', 's19976823', '', '9', '\0', '0', 'apollo', '2023-03-02 05:57:09', 'apollo', '2023-03-08 08:46:12');
INSERT INTO `item` VALUES ('131', '11', 'spring.application.name', '0', 'cninfo-manager', '', '1', '\0', '0', 'apollo', '2023-03-08 08:46:12', 'apollo', '2023-03-08 08:46:12');
INSERT INTO `item` VALUES ('132', '15', 'spring.application.name', '0', 'cninfo-icpserver', '', '1', '\0', '0', 'apollo', '2023-03-10 06:16:52', 'apollo', '2023-03-10 06:16:52');
INSERT INTO `item` VALUES ('133', '15', 'server.port', '0', '9801', '', '2', '\0', '0', 'apollo', '2023-03-10 06:16:52', 'apollo', '2023-03-10 06:16:52');
INSERT INTO `item` VALUES ('134', '15', 'server.servlet.context-path', '0', '/', '', '3', '\0', '0', 'apollo', '2023-03-10 06:16:52', 'apollo', '2023-03-10 06:16:52');
INSERT INTO `item` VALUES ('135', '15', 'dxp.props.server.ip', '0', '127.0.0.1', '', '4', '\0', '0', 'apollo', '2023-03-10 06:16:52', 'apollo', '2023-03-10 06:16:52');
INSERT INTO `item` VALUES ('136', '15', 'dxp.props.server.port', '0', '6666', '', '5', '\0', '0', 'apollo', '2023-03-10 06:16:52', 'apollo', '2023-03-10 06:16:52');
INSERT INTO `item` VALUES ('137', '15', 'dxp.props.ftp.ip', '0', '10.18.41.192', '', '6', '\0', '0', 'apollo', '2023-03-10 06:16:52', 'apollo', '2023-03-10 06:16:52');
INSERT INTO `item` VALUES ('138', '15', 'dxp.props.ftp.port', '0', '21', '', '7', '\0', '0', 'apollo', '2023-03-10 06:16:52', 'apollo', '2023-03-10 06:16:52');
INSERT INTO `item` VALUES ('139', '15', 'dxp.props.ftp.user', '0', 'leijmdas', '', '8', '\0', '0', 'apollo', '2023-03-10 06:16:52', 'apollo', '2023-03-10 06:16:52');
INSERT INTO `item` VALUES ('140', '15', 'dxp.props.ftp.password', '0', 's19976823', '', '9', '\0', '0', 'apollo', '2023-03-10 06:16:52', 'apollo', '2023-03-10 06:16:52');

-- ----------------------------
-- Table structure for namespace
-- ----------------------------
DROP TABLE IF EXISTS `namespace`;
CREATE TABLE `namespace` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增主键',
  `app_id` varchar(64) NOT NULL DEFAULT 'default' COMMENT 'AppID',
  `cluster_name` varchar(500) NOT NULL DEFAULT 'default' COMMENT 'Cluster Name',
  `namespace_name` varchar(500) NOT NULL DEFAULT 'default' COMMENT 'Namespace Name',
  `is_deleted` bit(1) NOT NULL DEFAULT b'0' COMMENT '1: deleted, 0: normal',
  `deleted_at` bigint(20) NOT NULL DEFAULT '0' COMMENT 'Delete timestamp based on milliseconds',
  `created_by` varchar(64) NOT NULL DEFAULT 'default' COMMENT '创建人邮箱前缀',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `updated_by` varchar(64) DEFAULT '' COMMENT '最后修改人邮箱前缀',
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '最后修改时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_AppId_ClusterName_NamespaceName_DeletedAt` (`app_id`,`cluster_name`(191),`namespace_name`(191),`deleted_at`),
  KEY `DataChange_LastTime` (`updated_at`),
  KEY `IX_NamespaceName` (`namespace_name`(191))
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8mb4 COMMENT='命名空间';

-- ----------------------------
-- Records of namespace
-- ----------------------------
INSERT INTO `namespace` VALUES ('1', 'dxp', 'default', 'application', '', '0', 'apollo', '2023-02-19 03:12:05', 'apollo', '2023-02-23 10:00:15');
INSERT INTO `namespace` VALUES ('2', 'dxp', 'default', 'TEST1.dxp', '', '0', 'apollo', '2023-02-20 01:55:47', 'apollo', '2023-02-23 10:00:50');
INSERT INTO `namespace` VALUES ('3', 'dxp', 'farben', 'application', '', '0', 'apollo', '2023-02-21 07:02:54', 'apollo', '2023-02-23 10:02:18');
INSERT INTO `namespace` VALUES ('4', 'dxp', 'farben', 'TEST1.dxp', '', '0', 'apollo', '2023-02-21 07:02:54', 'apollo', '2023-02-23 10:02:18');
INSERT INTO `namespace` VALUES ('5', 'dxp', 'default', 'bip', '', '0', 'apollo', '2023-02-21 07:15:15', 'apollo', '2023-02-23 10:02:18');
INSERT INTO `namespace` VALUES ('6', 'dxp', 'farben', 'bip', '', '0', 'apollo', '2023-02-21 07:15:15', 'apollo', '2023-02-23 10:02:18');
INSERT INTO `namespace` VALUES ('7', 'dxp', 'default', 'kunlong.yml', '', '0', 'apollo', '2023-02-21 07:15:55', 'apollo', '2023-02-23 10:02:18');
INSERT INTO `namespace` VALUES ('8', 'dxp', 'farben', 'kunlong.yml', '', '0', 'apollo', '2023-02-21 07:15:55', 'apollo', '2023-02-23 10:02:18');
INSERT INTO `namespace` VALUES ('9', 'bip-dxp', 'default', 'application', '', '0', 'apollo', '2023-02-22 07:05:58', 'apollo', '2023-02-23 10:01:53');
INSERT INTO `namespace` VALUES ('10', 'bip-dxp', 'bip-notice', 'application', '', '0', 'apollo', '2023-02-22 07:07:07', 'apollo', '2023-02-23 10:01:53');
INSERT INTO `namespace` VALUES ('11', 'dxp-manager', 'default', 'application', '\0', '0', 'apollo', '2023-02-22 07:27:16', 'apollo', '2023-02-22 07:27:16');
INSERT INTO `namespace` VALUES ('12', 'dxp-manager', 'bip-notice', 'application', '\0', '0', 'apollo', '2023-02-22 07:45:13', 'apollo', '2023-02-22 07:45:13');
INSERT INTO `namespace` VALUES ('13', 'dxp-openapi', 'default', 'application', '', '0', 'apollo', '2023-02-22 08:18:13', 'apollo', '2023-02-26 08:23:28');
INSERT INTO `namespace` VALUES ('14', 'dxp-fileserver', 'default', 'application', '\0', '0', 'apollo', '2023-02-22 08:18:49', 'apollo', '2023-02-22 08:18:49');
INSERT INTO `namespace` VALUES ('15', 'dxp-icpserver', 'default', 'application', '\0', '0', 'apollo', '2023-02-22 08:19:28', 'apollo', '2023-02-22 08:19:28');
INSERT INTO `namespace` VALUES ('16', 'dxp-zuul', 'default', 'application', '\0', '0', 'apollo', '2023-02-22 08:24:42', 'apollo', '2023-02-22 08:24:42');
INSERT INTO `namespace` VALUES ('17', 'dxp-none', 'default', 'application', '', '0', 'apollo', '2023-02-22 08:35:00', 'apollo', '2023-02-23 10:03:28');
INSERT INTO `namespace` VALUES ('18', 'dxp-xxl-job-client', 'default', 'application', '', '0', 'apollo', '2023-02-23 03:53:06', 'apollo', '2023-02-23 10:02:57');
INSERT INTO `namespace` VALUES ('19', 'dxp-manager', 'default', 'xxl-job-client', '\0', '0', 'apollo', '2023-02-23 06:00:58', 'apollo', '2023-02-23 06:00:58');
INSERT INTO `namespace` VALUES ('20', 'dxp-manager', 'bip-notice', 'xxl-job-client', '\0', '0', 'apollo', '2023-02-23 06:00:58', 'apollo', '2023-02-23 06:00:58');
INSERT INTO `namespace` VALUES ('21', 'dxp-mysql', 'default', 'application', '', '0', 'apollo', '2023-02-24 07:08:44', 'apollo', '2023-02-24 07:11:46');
INSERT INTO `namespace` VALUES ('22', 'dxp-mysql', 'default', 'ruoyi-mysql', '', '0', 'apollo', '2023-02-24 07:12:20', 'apollo', '2023-02-24 07:19:45');
INSERT INTO `namespace` VALUES ('23', 'ruoyi-mysql', 'default', 'application', '\0', '0', 'apollo', '2023-02-24 07:20:10', 'apollo', '2023-02-24 07:20:10');
INSERT INTO `namespace` VALUES ('24', 'ruoyi-mysql', 'default', 'ruoyi-mysql', '\0', '0', 'apollo', '2023-02-24 07:20:33', 'apollo', '2023-02-24 07:20:33');
INSERT INTO `namespace` VALUES ('25', 'dxp-manager', 'default', 'ruoyi-mysql', '\0', '0', 'apollo', '2023-02-24 07:23:18', 'apollo', '2023-02-24 07:23:18');
INSERT INTO `namespace` VALUES ('26', 'dxp-manager', 'bip-notice', 'ruoyi-mysql', '\0', '0', 'apollo', '2023-02-24 07:23:18', 'apollo', '2023-02-24 07:23:18');
INSERT INTO `namespace` VALUES ('27', 'c', 'default', 'application', '', '0', 'apollo', '2023-03-31 10:32:17', 'apollo', '2023-03-31 10:38:09');
INSERT INTO `namespace` VALUES ('28', 'A', 'default', 'application', '\0', '0', 'apollo', '2023-03-31 10:37:53', 'apollo', '2023-03-31 10:37:53');
INSERT INTO `namespace` VALUES ('29', 'c', 'default', 'a', '\0', '0', 'apollo', '2023-03-31 10:38:41', 'apollo', '2023-03-31 10:38:41');

-- ----------------------------
-- Table structure for publish
-- ----------------------------
DROP TABLE IF EXISTS `publish`;
CREATE TABLE `publish` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增主键',
  `publish_key` varchar(64) NOT NULL DEFAULT '' COMMENT '发布的Key',
  `name` varchar(64) NOT NULL DEFAULT 'default' COMMENT '发布名字',
  `comment` varchar(256) DEFAULT NULL COMMENT '发布说明',
  `app_id` varchar(64) NOT NULL DEFAULT 'default' COMMENT 'AppID',
  `cluster_name` varchar(500) NOT NULL DEFAULT 'default' COMMENT 'ClusterName',
  `namespace_name` varchar(500) NOT NULL DEFAULT 'default' COMMENT 'namespaceName',
  `configs` longtext NOT NULL COMMENT '发布配置',
  `is_abandoned` bit(1) NOT NULL DEFAULT b'0' COMMENT '是否废弃',
  `is_deleted` bit(1) NOT NULL DEFAULT b'0' COMMENT '1: deleted, 0: normal',
  `deleted_at` bigint(20) NOT NULL DEFAULT '0' COMMENT 'Delete timestamp based on milliseconds',
  `created_by` varchar(64) NOT NULL DEFAULT 'default' COMMENT '创建人邮箱前缀',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `updated_by` varchar(64) DEFAULT '' COMMENT '最后修改人邮箱前缀',
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '最后修改时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_ReleaseKey_DeletedAt` (`publish_key`,`deleted_at`),
  KEY `AppId_ClusterName_GroupName` (`app_id`,`cluster_name`(191),`namespace_name`(191)),
  KEY `DataChange_LastTime` (`updated_at`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8mb4 COMMENT='发布';

-- ----------------------------
-- Records of publish
-- ----------------------------
INSERT INTO `publish` VALUES ('1', '20230219171234-caea2f5af0884f63', '20230219171241-release', '', 'dxp', 'default', 'application', '{\"app.id\":\"1\"}', '\0', '', '0', 'apollo', '2023-02-19 03:12:34', 'apollo', '2023-02-24 00:00:15');
INSERT INTO `publish` VALUES ('2', '20230219171605-caea2f5af0884f64', '20230219171607-release', '', 'dxp', 'default', 'application', '{\"app.id\":\"1feign.baseinfo.url\\u003d${FEIGN_PARK_BASEINFO_URL:}\",\"spring.cloud.sentinel.enabled\":\"true\",\"spring.cloud.sentinel.transport.port\":\"8719\",\"spring.cloud.sentinel.transport.dashboard\":\"${SENTINEL_HOST:192.168.1.92}:8858\",\"server.port\":\"9610\",\"spring.application.name\":\"pf-baseinfo\",\"spring.profiles.active\":\"${PROFILE:dev}\",\"spring.cloud.nacos.discovery.server-addr\":\"${NACOS_HOST:192.168.1.92}:8848\",\"spring.cloud.nacos.discovery.register-enabled\":\"true\",\"spring.cloud.nacos.config.server-addr\":\"${NACOS_HOST:192.168.1.92}:8848\",\"logging.level.com.alibaba.nacos.client.naming\":\"error\",\"spring.main.allow-bean-definition-overriding\":\"true\",\"mybatis-plus.global-config.sql-parser-cache\":\"true\",\"mybatis-plus.configuration.log-impl\":\"org.apache.ibatis.logging.stdout.StdOutImpl\",\"spring.jta.log-dir\":\"data/logs/jta/base/${spring.application.name}-\\u0027%d{yyyy-MM-dd}\\u0027.%i.log\",\"ocean.mysql.column\":\"tenant_id\",\"ocean.mysql.enable\":\"true\",\"spring.jta.atomikos.properties.log-base-name\":\"${spring.application.name}\",\"management.endpoints.web.exposure.include\":\"*\",\"management.endpoint.health.show-details\":\"always\",\"fdfs.resHost\":\"http://14.29.241.30\",\"fdfs.storagePort\":\"9999\"}', '\0', '', '0', 'apollo', '2023-02-19 03:16:05', 'apollo', '2023-02-24 00:00:15');
INSERT INTO `publish` VALUES ('3', '20230220155639-83678dfae498c1f7', '20230220155647-release', '', 'dxp', 'default', 'TEST1.dxp', '{\"dxp.server.ip\":\"127.0.0.1\",\"dxp.server.port\":\"9899\"}', '\0', '', '0', 'apollo', '2023-02-20 01:56:40', 'apollo', '2023-02-24 00:00:50');
INSERT INTO `publish` VALUES ('4', '20230220161251-83678dfae498c1f8', '20230220161258-release', '', 'dxp', 'default', 'TEST1.dxp', '{\"dxp.server.ip\":\"127.0.0.1\",\"dxp.server.port\":\"49899\"}', '\0', '', '0', 'apollo', '2023-02-20 02:12:52', 'apollo', '2023-02-24 00:00:50');
INSERT INTO `publish` VALUES ('5', '20230220161542-83678dfae498c1f9', '20230220161549-release', '', 'dxp', 'default', 'TEST1.dxp', '{\"dxp.server.ip\":\"127.0.0.1\",\"dxp.server.port\":\"12345\"}', '\0', '', '0', 'apollo', '2023-02-20 02:15:42', 'apollo', '2023-02-24 00:00:50');
INSERT INTO `publish` VALUES ('6', '20230220162301-83678dfae498c1fa', '20230220162309-release', '', 'dxp', 'default', 'TEST1.dxp', '{\"dxp.server.ip\":\"127.0.0.1\",\"dxp.server.port\":\"22345\"}', '\0', '', '0', 'apollo', '2023-02-20 02:23:01', 'apollo', '2023-02-24 00:00:50');
INSERT INTO `publish` VALUES ('7', '20230220212935-caea8dfae498c1fb', '20230220212942-release', '', 'dxp', 'default', 'application', '{\"app.id\":\"1feign.baseinfo.url\\u003d${FEIGN_PARK_BASEINFO_URL:}\",\"spring.cloud.sentinel.enabled\":\"true\",\"spring.cloud.sentinel.transport.port\":\"8719\",\"spring.cloud.sentinel.transport.dashboard\":\"${SENTINEL_HOST:192.168.1.92}:8858\",\"spring.application.name\":\"pf-baseinfo\",\"spring.profiles.active\":\"${PROFILE:dev}\",\"spring.cloud.nacos.discovery.server-addr\":\"${NACOS_HOST:192.168.1.92}:8848\",\"spring.cloud.nacos.discovery.register-enabled\":\"true\",\"spring.cloud.nacos.config.server-addr\":\"${NACOS_HOST:192.168.1.92}:8848\",\"logging.level.com.alibaba.nacos.client.naming\":\"error\",\"spring.main.allow-bean-definition-overriding\":\"true\",\"mybatis-plus.global-config.sql-parser-cache\":\"true\",\"mybatis-plus.configuration.log-impl\":\"org.apache.ibatis.logging.stdout.StdOutImpl\",\"spring.jta.log-dir\":\"data/logs/jta/base/${spring.application.name}-\\u0027%d{yyyy-MM-dd}\\u0027.%i.log\",\"ocean.mysql.column\":\"tenant_id\",\"ocean.mysql.enable\":\"true\",\"spring.jta.atomikos.properties.log-base-name\":\"${spring.application.name}\",\"management.endpoints.web.exposure.include\":\"*\",\"management.endpoint.health.show-details\":\"always\",\"fdfs.resHost\":\"http://14.29.241.30\",\"fdfs.storagePort\":\"9999\"}', '\0', '', '0', 'apollo', '2023-02-20 07:29:36', 'apollo', '2023-02-24 00:00:15');
INSERT INTO `publish` VALUES ('8', '20230220214005-caea8dfae498c1fc', '20230220214007-release', '', 'dxp', 'default', 'application', '{\"app.id\":\"dxp\",\"spring.profiles.active\":\"${PROFILE:dev}\"}', '\0', '', '0', 'apollo', '2023-02-20 07:40:05', 'apollo', '2023-02-24 00:00:15');
INSERT INTO `publish` VALUES ('9', '20230220214556-caea8dfae498c1fd', '20230220214602-release', '', 'dxp', 'default', 'application', '{\"test\":\"11\"}', '\0', '', '0', 'apollo', '2023-02-20 07:45:56', 'apollo', '2023-02-24 00:00:15');
INSERT INTO `publish` VALUES ('10', '20230220231843-83678dfae498c1fe', '20230220231850-release', '', 'dxp', 'default', 'TEST1.dxp', '{\"dxp.server.ip\":\"127.0.0.1\",\"dxp.server.port\":\"22345\"}', '\0', '', '0', 'apollo', '2023-02-20 09:18:44', 'apollo', '2023-02-24 00:00:50');
INSERT INTO `publish` VALUES ('11', '20230220231849-caea8dfae498c1ff', '20230220231856-release', '', 'dxp', 'default', 'application', '{\"test\":\"11\"}', '\0', '', '0', 'apollo', '2023-02-20 09:18:50', 'apollo', '2023-02-24 00:00:15');
INSERT INTO `publish` VALUES ('12', '20230220232527-83678dfae498c200', '20230220232535-release', '', 'dxp', 'default', 'TEST1.dxp', '{\"dxp.server.ip\":\"127.0.0.1\",\"dxp.server.port\":\"12345\"}', '\0', '', '0', 'apollo', '2023-02-20 09:25:28', 'apollo', '2023-02-24 00:00:50');
INSERT INTO `publish` VALUES ('13', '20230221204619-caea8dfae498c201', '20230221204628-release', '', 'dxp', 'default', 'application', '{\"dxp.server.ip\":\"127.0.0.1\",\"dxp.server.port\":\"12345\",\"test\":\"11\"}', '\0', '', '0', 'apollo', '2023-02-21 06:46:20', 'apollo', '2023-02-24 00:00:15');
INSERT INTO `publish` VALUES ('14', '20230221210409-15778dfae498c202', '20230221210416-release', '', 'dxp', 'farben', 'application', '{}', '\0', '', '0', 'apollo', '2023-02-21 07:04:09', 'apollo', '2023-02-24 00:02:18');
INSERT INTO `publish` VALUES ('15', '20230221210430-cdf48dfae498c203', '20230221210434-release', '', 'dxp', 'farben', 'TEST1.dxp', '{\"dxp.server.port\":\"12345\",\"dxp.server.ip\":\"127.0.0.1\"}', '\0', '', '0', 'apollo', '2023-02-21 07:04:30', 'apollo', '2023-02-24 00:02:18');
INSERT INTO `publish` VALUES ('16', '20230221210456-15778dfae498c204', '20230221210502-release', '', 'dxp', 'farben', 'application', '{\"dxp.server.port\":\"12345\",\"dxp.server.ip\":\"127.0.0.1\"}', '\0', '', '0', 'apollo', '2023-02-21 07:04:56', 'apollo', '2023-02-24 00:02:18');
INSERT INTO `publish` VALUES ('17', '20230222202923-caea8dfae498c205', '20230222202923-release', '', 'dxp', 'default', 'application', '{\"dxp.server.ip\":\"127.0.0.1\",\"dxp.server.port\":\"12345\",\"dxp.ftp.ip\":\"127.0.0.1\",\"dxp.ftp.port\":\"22\",\"test\":\"11\"}', '\0', '', '0', 'apollo', '2023-02-22 06:29:23', 'apollo', '2023-02-24 00:00:15');
INSERT INTO `publish` VALUES ('18', '20230222210644-b1268dfae498c206', '20230222210649-release', '', 'bip-dxp', 'default', 'application', '{\"dxp.server.ip\":\"127.0.0.1\",\"dxp.server.port\":\"12345\",\"dxp.ftp.ip\":\"127.0.0.1\",\"dxp.ftp.port\":\"22\",\"test\":\"11\"}', '\0', '', '0', 'apollo', '2023-02-22 07:06:44', 'apollo', '2023-02-24 00:01:52');
INSERT INTO `publish` VALUES ('19', '20230222210847-4aab8dfae498c207', '20230222210854-release', '', 'bip-dxp', 'bip-notice', 'application', '{\"dxp.server.ip\":\"127.0.0.1\",\"dxp.server.port\":\"12345\",\"dxp.ftp.ip\":\"127.0.0.1\",\"dxp.ftp.port\":\"22\",\"test\":\"11\"}', '\0', '', '0', 'apollo', '2023-02-22 07:08:47', 'apollo', '2023-02-24 00:01:52');
INSERT INTO `publish` VALUES ('20', '20230222211742-b1268dfae498c208', '20230222211752-release', '', 'bip-dxp', 'default', 'application', '{\"dxp.server.ip\":\"127.0.0.1\",\"dxp.server.port\":\"2345\",\"dxp.ftp.ip\":\"127.0.0.1\",\"dxp.ftp.port\":\"22\",\"test\":\"11\"}', '\0', '', '0', 'apollo', '2023-02-22 07:17:43', 'apollo', '2023-02-24 00:01:52');
INSERT INTO `publish` VALUES ('21', '20230222212144-b1268dfae498c209', '20230222212151-release', '', 'bip-dxp', 'default', 'application', '{\"apollo.meta\":\"http://${APOLLO_META:192.168.1.192:8080}\",\"dxp.server.ip\":\"127.0.0.1\",\"dxp.server.port\":\"2345\",\"dxp.ftp.ip\":\"127.0.0.1\",\"dxp.ftp.port\":\"22\"}', '\0', '', '0', 'apollo', '2023-02-22 07:21:45', 'apollo', '2023-02-24 00:01:52');
INSERT INTO `publish` VALUES ('22', '20230222212931-69ca8dfae498c20a', '20230222212939-release', '', 'dxp-manager', 'default', 'application', '{\"server.port\":\"9711\",\"server.servlet.context-path\":\"/manager\"}', '\0', '\0', '0', 'apollo', '2023-02-22 07:29:31', 'apollo', '2023-02-22 07:29:31');
INSERT INTO `publish` VALUES ('23', '20230222213203-69ca8dfae498c20b', '20230222213211-release', '', 'dxp-manager', 'default', 'application', '{\"server.port\":\"9711\",\"server.servlet.context-path\":\"/manager\",\"dxp.server.ip\":\"127.0.0.1\",\"dxp.server.port\":\"6666\"}', '\0', '\0', '0', 'apollo', '2023-02-22 07:32:04', 'apollo', '2023-02-22 07:32:04');
INSERT INTO `publish` VALUES ('24', '20230222213756-69ca8dfae498c20c', '20230222213803-release', '', 'dxp-manager', 'default', 'application', '{\"server.port\":\"9711\",\"server.servlet.context-path\":\"/manager\",\"dxp.server.ip\":\"127.0.0.1\",\"dxp.server.port\":\"6666\",\"dxp.ftp.ip\":\"127.0.0.1\",\"dxp.ftp.port\":\"22\"}', '\0', '\0', '0', 'apollo', '2023-02-22 07:37:57', 'apollo', '2023-02-22 07:37:57');
INSERT INTO `publish` VALUES ('25', '20230222214232-69ca8dfae498c20d', '20230222214241-release', '', 'dxp-manager', 'default', 'application', '{\"server.port\":\"9701\",\"server.servlet.context-path\":\"/manager\",\"dxp.server.ip\":\"127.0.0.1\",\"dxp.server.port\":\"6666\",\"dxp.ftp.ip\":\"127.0.0.1\",\"dxp.ftp.port\":\"22\"}', '\0', '\0', '0', 'apollo', '2023-02-22 07:42:33', 'apollo', '2023-02-22 07:42:33');
INSERT INTO `publish` VALUES ('26', '20230222214658-034f8dfae498c20e', '20230222214704-release', '', 'dxp-manager', 'bip-notice', 'application', '{\"server.port\":\"9701\",\"dxp.ftp.ip\":\"127.0.0.1\",\"dxp.ftp.port\":\"22\",\"dxp.server.ip\":\"127.0.0.1\",\"dxp.server.port\":\"6666\",\"server.servlet.context-path\":\"/manager\"}', '\0', '\0', '0', 'apollo', '2023-02-22 07:46:59', 'apollo', '2023-02-22 07:46:59');
INSERT INTO `publish` VALUES ('27', '20230222215830-69ca8dfae498c20f', '20230222215839-release', '', 'dxp-manager', 'default', 'application', '{\"server.port\":\"9701\",\"server.servlet.context-path\":\"/common\",\"dxp.server.ip\":\"127.0.0.1\",\"dxp.server.port\":\"6666\",\"dxp.ftp.ip\":\"127.0.0.1\",\"dxp.ftp.port\":\"22\"}', '\0', '\0', '0', 'apollo', '2023-02-22 07:58:30', 'apollo', '2023-02-22 07:58:30');
INSERT INTO `publish` VALUES ('28', '20230222220300-69ca8dfae498c210', '20230222220305-release', '', 'dxp-manager', 'default', 'application', '{\"server.port\":\"9701\",\"server.servlet.context-path\":\"/\",\"dxp.server.ip\":\"127.0.0.1\",\"dxp.server.port\":\"6666\",\"dxp.ftp.ip\":\"127.0.0.1\",\"dxp.ftp.port\":\"22\"}', '\0', '\0', '0', 'apollo', '2023-02-22 08:03:00', 'apollo', '2023-02-22 08:03:00');
INSERT INTO `publish` VALUES ('29', '20230222222151-24ed8dfae498c211', '20230222222200-release', '', 'dxp-openapi', 'default', 'application', '{}', '\0', '', '0', 'apollo', '2023-02-22 08:21:52', 'apollo', '2023-02-26 22:23:27');
INSERT INTO `publish` VALUES ('30', '20230222222527-6eb18dfae498c212', '20230222222535-release', '', 'dxp-zuul', 'default', 'application', '{\"ignore_urls\":\"/\"}', '\0', '\0', '0', 'apollo', '2023-02-22 08:25:27', 'apollo', '2023-02-22 08:25:27');
INSERT INTO `publish` VALUES ('31', '20230222222828-635e8dfae498c213', '20230222222837-release', '', 'dxp-fileserver', 'default', 'application', '{}', '\0', '\0', '0', 'apollo', '2023-02-22 08:28:29', 'apollo', '2023-02-22 08:28:29');
INSERT INTO `publish` VALUES ('32', '20230222223225-69ca8dfae498c214', '20230222223235-release', '', 'dxp-manager', 'default', 'application', '{\"server.port\":\"9711\",\"server.servlet.context-path\":\"/\",\"dxp.server.ip\":\"127.0.0.1\",\"dxp.server.port\":\"6666\",\"dxp.ftp.ip\":\"127.0.0.1\",\"dxp.ftp.port\":\"22\"}', '\0', '\0', '0', 'apollo', '2023-02-22 08:32:26', 'apollo', '2023-02-22 08:32:26');
INSERT INTO `publish` VALUES ('33', '20230222223510-71578dfae498c215', '20230222223520-release', '', 'dxp-none', 'default', 'application', '{}', '\0', '', '0', 'apollo', '2023-02-22 08:35:10', 'apollo', '2023-02-24 00:03:28');
INSERT INTO `publish` VALUES ('34', '20230222224559-69ca8dfae498c216', '20230222224609-release', '', 'dxp-manager', 'default', 'application', '{\"server.port\":\"9701\",\"server.servlet.context-path\":\"/\",\"dxp.server.ip\":\"127.0.0.1\",\"dxp.server.port\":\"6666\",\"dxp.ftp.ip\":\"127.0.0.1\",\"dxp.ftp.port\":\"22\"}', '\0', '\0', '0', 'apollo', '2023-02-22 08:46:00', 'apollo', '2023-02-22 08:46:00');
INSERT INTO `publish` VALUES ('35', '20230223175400-eabb8dfae498c217', '20230223175409-release', '', 'dxp-xxl-job-client', 'default', 'application', '{\"xxl.job.admin.addresses\":\"http://${XXL_JOB_ADMIN_HOST:192.168.1.92}:${XXL_JOB_ADMIN_PORT:9680}/xxl-job-admin\",\"xxl.job.accessToken\":\"default_token\",\"xxl.job.executor.appname\":\"xxl-job-manager\",\"xxl.job.executor.address\":\"\",\"xxl.job.executor.ip\":\"\",\"xxl.job.executor.port\":\"9980\",\"xxl.job.executor.logpath\":\"/data/applogs/xxl-job/jobhandler\",\"xxl.job.executor.logretentiondays\":\"30\"}', '\0', '', '0', 'apollo', '2023-02-23 03:54:01', 'apollo', '2023-02-24 00:02:56');
INSERT INTO `publish` VALUES ('36', '20230223200203-a7568dfae498c218', '20230223200212-release', '', 'dxp-manager', 'default', 'xxl-job-client', '{\"xxl.job.admin.addresses\":\"http://${XXL_JOB_ADMIN_HOST:192.168.1.92}:${XXL_JOB_ADMIN_PORT:9680}/xxl-job-admin\",\"xxl.job.accessToken\":\"default_token\",\"xxl.job.executor.appname\":\"xxl-job-manager\",\"xxl.job.executor.address\":\"\",\"xxl.job.executor.ip\":\"\",\"xxl.job.executor.port\":\"9980\",\"xxl.job.executor.logpath\":\"/data/applogs/xxl-job/jobhandler\",\"xxl.job.executor.logretentiondays\":\"30\"}', '\0', '\0', '0', 'apollo', '2023-02-23 06:02:03', 'apollo', '2023-02-23 06:02:03');
INSERT INTO `publish` VALUES ('37', '20230223202932-a7568dfae498c219', '20230223202939-release', '', 'dxp-manager', 'default', 'xxl-job-client', '{\"xxl.job.admin.addresses\":\"http://${XXL_JOB_ADMIN_HOST:127.0.0.1}:${XXL_JOB_ADMIN_PORT:8080}/xxl-job-admin\",\"xxl.job.accessToken\":\"default_token\",\"xxl.job.executor.appname\":\"xxl-job-manager\",\"xxl.job.executor.address\":\"\",\"xxl.job.executor.ip\":\"\",\"xxl.job.executor.port\":\"9999\",\"xxl.job.executor.logpath\":\"/data/applogs/xxl-job/jobhandler\",\"xxl.job.executor.logretentiondays\":\"30\"}', '\0', '\0', '0', 'apollo', '2023-02-23 06:29:32', 'apollo', '2023-02-23 06:29:32');
INSERT INTO `publish` VALUES ('38', '20230223205552-a7568dfae498c21a', '20230223205601-release', '', 'dxp-manager', 'default', 'xxl-job-client', '{\"xxl.job.admin.addresses\":\"http://${XXL_JOB_ADMIN_HOST:127.0.0.1}:${XXL_JOB_ADMIN_PORT:8080}/xxl-job-admin\",\"xxl.job.accessToken\":\"default_token\",\"xxl.job.executor.appname\":\"xxl-job-manager\",\"xxl.job.executor.address\":\"\",\"xxl.job.executor.ip\":\"\",\"xxl.job.executor.port\":\"9791\",\"xxl.job.executor.logpath\":\"/data/applogs/xxl-job/jobhandler\",\"xxl.job.executor.logretentiondays\":\"30\"}', '\0', '\0', '0', 'apollo', '2023-02-23 06:55:52', 'apollo', '2023-02-23 06:55:52');
INSERT INTO `publish` VALUES ('39', '20230223212715-a7568dfae498c21b', '20230223212725-release', '', 'dxp-manager', 'default', 'xxl-job-client', '{\"xxl.job.admin.addresses\":\"http://${XXL_JOB_ADMIN_HOST:10.18.41.192}:${XXL_JOB_ADMIN_PORT:9680}/xxl-job-admin\",\"xxl.job.accessToken\":\"default_token\",\"xxl.job.executor.appname\":\"xxl-job-manager\",\"xxl.job.executor.address\":\"\",\"xxl.job.executor.ip\":\"\",\"xxl.job.executor.port\":\"9791\",\"xxl.job.executor.logpath\":\"/data/applogs/xxl-job/jobhandler\",\"xxl.job.executor.logretentiondays\":\"30\"}', '\0', '\0', '0', 'apollo', '2023-02-23 07:27:16', 'apollo', '2023-02-23 07:27:16');
INSERT INTO `publish` VALUES ('40', '20230223214340-a7568dfae498c21c', '20230223214349-release', '', 'dxp-manager', 'default', 'xxl-job-client', '{\"xxl.job.admin.addresses\":\"http://${XXL_JOB_ADMIN_HOST:10.18.41.192}:${XXL_JOB_ADMIN_PORT:9680}/xxl-job-admin\",\"xxl.job.accessToken\":\"default_token\",\"xxl.job.executor.appname\":\"xxl-job-manager\",\"xxl.job.executor.address\":\"\",\"xxl.job.executor.ip\":\"\",\"xxl.job.executor.port\":\"9791\",\"xxl.job.executor.logpath\":\"./logs/xxl-job/jobhandler\",\"xxl.job.executor.logretentiondays\":\"30\"}', '\0', '\0', '0', 'apollo', '2023-02-23 07:43:40', 'apollo', '2023-02-23 07:43:40');
INSERT INTO `publish` VALUES ('41', '20230223214840-a7568dfae498c21d', '20230223214850-release', '', 'dxp-manager', 'default', 'xxl-job-client', '{\"xxl.job.admin.addresses\":\"http://${XXL_JOB_ADMIN_HOST:10.18.41.192}:${XXL_JOB_ADMIN_PORT:9680}/xxl-job-admin\",\"xxl.job.accessToken\":\"default_token\",\"xxl.job.executor.appname\":\"xxl-job-manager\",\"xxl.job.executor.address\":\"\",\"xxl.job.executor.ip\":\"\",\"xxl.job.executor.port\":\"9771\",\"xxl.job.executor.logpath\":\"./logs/xxl-job/jobhandler\",\"xxl.job.executor.logretentiondays\":\"30\"}', '\0', '\0', '0', 'apollo', '2023-02-23 07:48:41', 'apollo', '2023-02-23 07:48:41');
INSERT INTO `publish` VALUES ('42', '20230223235232-69ca8dfae498c21e', '20230223235240-release', '', 'dxp-manager', 'default', 'application', '{\"server.port\":\"9701\",\"server.servlet.context-path\":\"/\",\"dxp.properties.server.ip\":\"127.0.0.1\",\"dxp.properties.server.port\":\"6666\",\"dxp.properties.ftp.ip\":\"127.0.0.1\",\"dxp.properties.ftp.port\":\"22\"}', '\0', '\0', '0', 'apollo', '2023-02-23 09:52:33', 'apollo', '2023-02-23 09:52:33');
INSERT INTO `publish` VALUES ('43', '20230223235730-b1268dfae498c21f', '20230223235738-release', '', 'bip-dxp', 'default', 'application', '{\"apollo.meta\":\"http://${APOLLO_META:192.168.1.192:8080}\",\"dxp.properties.server.ip\":\"127.0.0.1\",\"dxp.properties.server.port\":\"2345\",\"dxp.properties.ftp.ip\":\"127.0.0.1\",\"dxp.properties.ftp.port\":\"22\"}', '\0', '', '0', 'apollo', '2023-02-23 09:57:31', 'apollo', '2023-02-24 00:01:52');
INSERT INTO `publish` VALUES ('44', '20230224212241-2a608dfae498c220', '20230224212249-release', '', 'ruoyi-mysql', 'default', 'ruoyi-mysql', '{\"spring.datasource.druid.ruoyi.url\":\"jdbc:mysql://192.168.1.128:3306/ry-vue?autoReconnect\\u003dtrue\\u0026autoReconnectForPools\\u003dtrue\\u0026failOverReadOnly\\u003dfalse\\u0026useUnicode\\u003dtrue\\u0026characterEncoding\\u003dUTF-8\\u0026useSSL\\u003dfalse\\u0026allowMultiQueries\\u003dtrue\\u0026serverTimezone\\u003dGMT%2B8\",\"spring.datasource.druid.ruoyi.driverClass\":\"com.mysql.jdbc.Driver\",\"spring.datasource.druid.ruoyi.username\":\"root\",\"spring.datasource.druid.ruoyi.password\":\"root\",\"spring.datasource.druid.ruoyi.testOnBorrow\":\"false\",\"spring.datasource.druid.ruoyi.testWhileIdle\":\"true\",\"spring.datasource.druid.ruoyi.initialSize\":\"2\",\"spring.datasource.druid.ruoyi.minIdle\":\"2\",\"spring.datasource.druid.ruoyi.maxActive\":\"50\",\"spring.datasource.druid.ruoyi.testOnReturn\":\"false\",\"spring.datasource.druid.ruoyi.validationQuery\":\"SELECT 1\",\"spring.datasource.druid.ruoyi.timeBetweenEvictionRunsMillis\":\"20000\"}', '\0', '\0', '0', 'apollo', '2023-02-24 07:22:41', 'apollo', '2023-02-24 07:22:41');
INSERT INTO `publish` VALUES ('45', '20230226222501-69ca8dfae498c221', '20230226222512-release', '', 'dxp-manager', 'default', 'application', '{\"server.port\":\"9701\",\"server.servlet.context-path\":\"/\",\"dxp.props.server.ip\":\"127.0.0.1\",\"dxp.props.server.port\":\"6666\",\"dxp.props.ftp.ip\":\"127.0.0.1\",\"dxp.props.ftp.port\":\"22\"}', '\0', '\0', '0', 'apollo', '2023-02-26 08:25:02', 'apollo', '2023-02-26 08:25:02');
INSERT INTO `publish` VALUES ('46', '20230302210021-69ca8dfae498c222', '20230302195728-release', '', 'dxp-manager', 'default', 'application', '{\"server.port\":\"9701\",\"server.servlet.context-path\":\"/\",\"dxp.props.server.ip\":\"127.0.0.1\",\"dxp.props.server.port\":\"6666\",\"dxp.props.ftp.ip\":\"10.18.41.192\",\"dxp.props.ftp.port\":\"21\",\"dxp.props.ftp.user\":\"leijmdas\",\"dxp.props.ftp.password\":\"s19976823\"}', '\0', '\0', '0', 'apollo', '2023-03-02 07:00:22', 'apollo', '2023-03-02 07:00:22');
INSERT INTO `publish` VALUES ('47', '20230302211653-69ca8dfae498c223', '20230302211708-release', '', 'dxp-manager', 'default', 'application', '{\"server.port\":\"9701\",\"server.servlet.context-path\":\"/\",\"dxp.props.server.ip\":\"127.0.0.1\",\"dxp.props.server.port\":\"6666\",\"dxp.props.ftp.ip\":\"10.18.41.192\",\"dxp.props.ftp.port\":\"21\",\"dxp.props.ftp.user\":\"leijmdas\",\"dxp.props.ftp.password\":\"s19976823\"}', '\0', '\0', '0', 'apollo', '2023-03-02 07:16:54', 'apollo', '2023-03-02 07:16:54');
INSERT INTO `publish` VALUES ('48', '20230308224616-69ca8dfae498c224', '20230308224638-release', '', 'dxp-manager', 'default', 'application', '{\"spring.application.name\":\"cninfo-manager\",\"server.port\":\"9701\",\"server.servlet.context-path\":\"/\",\"dxp.props.server.ip\":\"127.0.0.1\",\"dxp.props.server.port\":\"6666\",\"dxp.props.ftp.ip\":\"10.18.41.192\",\"dxp.props.ftp.port\":\"21\",\"dxp.props.ftp.user\":\"leijmdas\",\"dxp.props.ftp.password\":\"s19976823\"}', '\0', '\0', '0', 'apollo', '2023-03-08 08:46:16', 'apollo', '2023-03-08 08:46:16');
INSERT INTO `publish` VALUES ('49', '20230310201706-64b68dfae498c225', '20230310201725-release', '', 'dxp-icpserver', 'default', 'application', '{\"spring.application.name\":\"cninfo-icpserver\",\"server.port\":\"9801\",\"server.servlet.context-path\":\"/\",\"dxp.props.server.ip\":\"127.0.0.1\",\"dxp.props.server.port\":\"6666\",\"dxp.props.ftp.ip\":\"10.18.41.192\",\"dxp.props.ftp.port\":\"21\",\"dxp.props.ftp.user\":\"leijmdas\",\"dxp.props.ftp.password\":\"s19976823\"}', '\0', '\0', '0', 'apollo', '2023-03-10 06:17:06', 'apollo', '2023-03-10 06:17:06');
INSERT INTO `publish` VALUES ('50', '20230331233234-64b6d520d16d2fd6', '20230331233234-release', '', 'dxp-icpserver', 'default', 'application', '{\"spring.application.name\":\"cninfo-icpserver\",\"server.port\":\"9801\",\"server.servlet.context-path\":\"/\",\"dxp.props.server.ip\":\"127.0.0.1\",\"dxp.props.server.port\":\"6666\",\"dxp.props.ftp.ip\":\"10.18.41.192\",\"dxp.props.ftp.port\":\"21\",\"dxp.props.ftp.user\":\"leijmdas\",\"dxp.props.ftp.password\":\"s19976823\"}', '\0', '\0', '0', 'apollo', '2023-03-31 10:32:34', 'apollo', '2023-03-31 10:32:34');

-- ----------------------------
-- Table structure for service_info
-- ----------------------------
DROP TABLE IF EXISTS `service_info`;
CREATE TABLE `service_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `tenant_id` bigint(20) DEFAULT NULL COMMENT '租户信息',
  `service_id` varchar(512) NOT NULL COMMENT '服务ID',
  `service_name` varchar(512) NOT NULL COMMENT '服务名称',
  `ip` varchar(128) NOT NULL COMMENT '服务端ip',
  `port` int(11) NOT NULL COMMENT '端口',
  `metadata` mediumtext COMMENT '服务元数据',
  `check_url` varchar(512) DEFAULT NULL COMMENT '健康检查url',
  `check_interval` int(255) DEFAULT NULL COMMENT '健康检查间隔',
  `status` varchar(255) DEFAULT NULL COMMENT '状态: 启动、运行、停止',
  `version` varchar(255) DEFAULT NULL COMMENT '版本',
  `weight` double DEFAULT NULL COMMENT '权重',
  `balancing_strategy` varchar(255) DEFAULT NULL COMMENT '负载均衡策略',
  `register_interval` int(11) NOT NULL DEFAULT '20' COMMENT '注册间隔时间',
  `expire_time` int(11) NOT NULL DEFAULT '30' COMMENT '注册信息超时时间',
  `register_time` datetime DEFAULT NULL COMMENT '注册时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COMMENT='服务注册信息';

-- ----------------------------
-- Records of service_info
-- ----------------------------
INSERT INTO `service_info` VALUES ('5', '0', 'e5017773cc51480bb513fae34abbc9de', 'www.goplatform1.com', '192.168.4.121', '88', '', '', '0', '', 'v1', '0', '', '20', '30', '2024-06-11 11:04:14');
INSERT INTO `service_info` VALUES ('11', '0', '4e7220ad388d4c32ba3fc55262e5ef91', 'www.goplatform.com', '169.254.133.71', '88', '', '', '0', 'active', 'v1', '0', '', '20', '30', '2024-06-01 16:57:55');
INSERT INTO `service_info` VALUES ('13', '0', 'string', 'string', 'string', '0', 'string', 'string', '0', 'string', 'string', '0', 'string', '20', '30', '2024-06-01 16:58:20');
INSERT INTO `service_info` VALUES ('17', '0', '8e294c590ca342929fd5048d58722f72', 'www.abc.com', '127.0.0.1', '111', '', '', '0', '', '', '0', '', '30', '20', '2024-06-01 19:15:26');

-- ----------------------------
-- Table structure for tenant_info
-- ----------------------------
DROP TABLE IF EXISTS `tenant_info`;
CREATE TABLE `tenant_info` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `kp` varchar(128) COLLATE utf8_bin NOT NULL COMMENT 'kp',
  `tenant_id` varchar(128) COLLATE utf8_bin DEFAULT '' COMMENT 'tenant_id',
  `tenant_name` varchar(128) COLLATE utf8_bin DEFAULT '' COMMENT 'tenant_name',
  `tenant_desc` varchar(256) COLLATE utf8_bin DEFAULT NULL COMMENT 'tenant_desc',
  `create_source` varchar(32) COLLATE utf8_bin DEFAULT NULL COMMENT 'create_source',
  `gmt_create` bigint(20) NOT NULL COMMENT '创建时间',
  `gmt_modified` bigint(20) NOT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_tenant_info_kptenantid` (`kp`,`tenant_id`),
  KEY `idx_tenant_id` (`tenant_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='tenant_info';

-- ----------------------------
-- Records of tenant_info
-- ----------------------------
INSERT INTO `tenant_info` VALUES ('2', '1', '97a8b565-b337-4e72-aa3f-18271cb50c39', 'pro', 'pro', 'nacos', '1623060398932', '1623060398932');
INSERT INTO `tenant_info` VALUES ('3', '1', 'ipark-dev', 'park-service-dev', 'park-service-dev', 'nacos', '1670384110029', '1670384110029');
