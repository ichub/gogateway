package dto

import (
	"gitee.com/ichub/goconfig/common/base/basedto"
	"gitee.com/ichub/goconfig/common/base/baseiface"
	"gitee.com/ichub/gogateway/gateway/server/consts"
	"gitee.com/ichub/goweb/common/pagemsg/page"
	"gitee.com/ichub/goweb/common/webclient"
	"gitee.com/ichub/goweb/common/webclient/webmsg"
	"github.com/go-resty/resty/v2"
)

type GatewayRequest struct {
	basedto.BaseEntity
	*webmsg.WebMsg

	WebBasePath string `json:"web_base_path,omitempty"`
	ServiceId   string `json:"service_id" json:"service_id,omitempty"`
	Path        string `json:"path,omitempty"`

	webcli *webclient.IchubWebClient
}

func NewGatewayRequest() *GatewayRequest {
	return NewRequestPath("emp/query")
}

func NewRequestPath(path string) *GatewayRequest {
	var dto = &GatewayRequest{

		WebBasePath: consts.ApiGatewayHome,
		ServiceId:   consts.SEVICE_ID_NAME_DEPTEMP,
		Path:        path,

		WebMsg: webmsg.NewWebMsg(),
		webcli: webclient.Default(),
	}
	dto.InitProxy(dto)
	return dto
}

func (this *GatewayRequest) FullPath() string {
	return this.WebBasePath + "/" + this.ServiceId + "/" + this.Path
}

func (this *GatewayRequest) Post() *basedto.IchubResult {
	this.SetUrl(this.FullPath())
	return this.webcli.Post2Result(this.WebMsg)
}

// IbaseProxy
func (this *GatewayRequest) PostProxy(proxy baseiface.IbaseProxy) *basedto.IchubResult {
	if !this.webcli.IfServiceExists() {
		return basedto.ResultFailMsg(this.webcli.ServerName + consts.MSG_SEVICE_NOFOUND)
	}
	this.SetUrl(this.FullPath())
	return this.webcli.PostProxy(this.WebMsg, proxy)
}

func (this *GatewayRequest) PostPage(req *page.PageRequest) *page.PageResult {
	if !this.webcli.IfServiceExists() {
		return page.ResultFailMsg(this.webcli.ServerName + consts.MSG_SEVICE_NOFOUND)
	}
	this.SetUrl(this.FullPath())
	this.SetBody(req.ToJsonBytes())
	return this.webcli.Post2PageResult(this.WebMsg)
}

func (this *GatewayRequest) PostResp() (*resty.Response, error) {
	this.SetUrl(this.FullPath())
	return this.webcli.PostWeb(this.WebMsg)
}

func (this *GatewayRequest) Get() *basedto.IchubResult {
	this.SetUrl(this.FullPath())
	if !this.webcli.IfServiceExists() {
		return basedto.ResultFailMsg(this.webcli.ServerName + consts.MSG_SEVICE_NOFOUND)
	}
	var result = this.webcli.Get2Result(this.WebMsg)
	return result
}
func (this *GatewayRequest) GetResp() (*resty.Response, error) {
	this.SetUrl(this.FullPath())
	return this.webcli.GetWeb(this.WebMsg)
}

func (this *GatewayRequest) Post2PageResult() *page.PageResult {
	this.SetUrl(this.FullPath())
	return this.webcli.Post2PageResult(this.WebMsg)
}

func (this *GatewayRequest) Get2PageResult() *page.PageResult {
	this.SetUrl(this.FullPath())
	return this.webcli.Get2PageResult(this.WebMsg)
}
