package dto

import (
	"gitee.com/ichub/goconfig/common/base/baseiface"
	"gitee.com/ichub/goconfig/common/basedi"
	"github.com/sirupsen/logrus"
)

/*
	@Title  文件名称: gateway_request_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: LEIJMDAS@163.COM 时间(2024-05-05 09:00:17)
	@Update 作者: LEIJMDAS@163.COM 时间(2024-05-05 09:00:17)
*/

const singleNameGatewayRequest = "dto.GatewayRequest"

// init center load
func init() {
	registerBeanGatewayRequest()
}

// center GatewayRequest
func registerBeanGatewayRequest() {
	basedi.RegisterLoadBean(singleNameGatewayRequest, LoadGatewayRequest)
}

func FindBeanGatewayRequest() *GatewayRequest {
	bean, ok := basedi.FindBean(singleNameGatewayRequest).(*GatewayRequest)
	if !ok {
		logrus.Errorf("FindBeanGatewayRequest: failed to cast bean to *GatewayRequest")
		return nil
	}
	return bean

}

func LoadGatewayRequest() baseiface.ISingleton {
	var s = NewGatewayRequest()
	InjectGatewayRequest(s)
	return s

}

func InjectGatewayRequest(s *GatewayRequest) {

	logrus.Debug("inject")
}
