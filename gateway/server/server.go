package main

import (
	"gitee.com/ichub/goconfig/common/base/baseutils/fileutils"
	"gitee.com/ichub/goconfig/common/ichubconfig"
	"gitee.com/ichub/goconfig/common/ichublog"
	"gitee.com/ichub/gogateway/gateway/server/router"
	"gitee.com/ichub/goweb/common/webserver"
	"github.com/sirupsen/logrus"
)

/*
	@Title    文件名称: server.go
	@Description  描述: 通用网关服务
	@Contact.user raymond
	@Author  作者: leijianming@163.com  时间(2024-02-18 22:38:21)
	@Update  作者: leijianming@163.com  时间(2024-02-18 22:38:21)
*/

// https://www.jianshu.com/p/982c4fabb11d swagg参数

func main() {
	defer func() {
		if r := recover(); r != nil {
			ichublog.Error("[main] Recovered in ", r)

		}
	}()
	ichublog.Log(fileutils.GetCurPath())

	var config = ichubconfig.Default()
	serverDto := config.ReadIchubWebServer()
	swaggerClientDto := config.ReadWebSwagger()

	ichublog.Log("serverDto=", serverDto)
	var server = webserver.New(serverDto)
	logrus.Info("http://localhost:1000/swagger/index.html#/")
	//注册服务
	if swaggerClientDto.Enable == "false" {
		server.StartWeb(router.Register)
	} else {
		server.StartWebSwagger(router.Swagger, router.Register)

	}
}

// go get -u -v github.com/swaggo/gin-swagger
// go get -u -v github.com/swaggo/files
// go get -u -v github.com/alecthomas/template

// func main() {
//	router := gin.Default()
//
//	// 假设你想把所有/api/路径下的请求转发到另一个服务
//	apiProxy := router.Group("/api")
//	{
//		target, _ := url.Parse("http://backend-service.com/api/")
//		proxy := httputil.NewSingleHostReverseProxy(target)
//
//		apiProxy.Use(func(c *gin.Context) {
//			// 在这里你可以对请求进行任何预处理
//			// 比如修改请求头，删除或添加参数等
//
//			// 然后调用代理的ServeHTTP
//			proxy.ServeHTTP(c.Writer, c.Request)
//		})
//	}
//
//	router.Run(":8080")
//}
