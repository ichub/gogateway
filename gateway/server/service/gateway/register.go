package gateway

func Register() {

	//head options
	NewGatewayGetFuncService().Register()
	NewGatewayDeleteFuncService().Register()

	NewGatewayPostFuncService().Register()
	NewGatewayPutFuncService().Register()
	NewGatewayHeadFuncService().Register()
}
