package gateway

import (
	"gitee.com/ichub/gogateway/gateway/internal/route"
	"gitee.com/ichub/gogateway/gateway/server/consts"
	"gitee.com/ichub/goweb/common/webclient"
	"gitee.com/ichub/goweb/common/webserver/funchandler"
	"github.com/gin-gonic/gin"
	"net/http"
)

type GatewayPostFuncService struct {
	funchandler.FuncService
}

func NewGatewayPostFuncService() *GatewayPostFuncService {
	return new(GatewayPostFuncService).init()
}

func (this *GatewayPostFuncService) init() *GatewayPostFuncService {
	//this.FuncId = consts.FuncGatewayPost
	this.InitRoute(this, http.MethodPost, consts.ApiGateway)
	return this
}

// @Summary		gateway
// @Description	通用网关
// @Tags	   gateway网关
// @Produce		json
// @Param		body	body	string	true	"query params: string "
// @Success		200		{object}	string		"成功"
// @Failure		400		{object}	string							"请求错误"
// @Failure		500		{object}	string							"内部错误"
// @Router			/gateway [post]
func (this *GatewayPostFuncService) Execute(ctx *gin.Context) {

	//	ctx.IndentedJSON(http.StatusOK, (&basedto.IchubResult{}).SuccessData("Hello World"))

	resp := this.ExeFuncs(ctx)
	if resp.Err() != nil {
		this.WriteIfErr(ctx, resp.Err())
	}
	for k, v := range resp.Resp().Header() {
		ctx.Header(k, v[0])
	}
	ctx.Writer.Write(resp.Body())
}

func (this *GatewayPostFuncService) ExeFuncs(ctx *gin.Context) *webclient.WebMsgProxyResp {
	var dto = route.NewGatewayRouter()
	return dto.Parse(ctx).Post(ctx)

}
