package gateway

import (
	"gitee.com/ichub/gogateway/gateway/internal/route"
	"gitee.com/ichub/gogateway/gateway/server/consts"
	"gitee.com/ichub/goweb/common/webclient"
	"gitee.com/ichub/goweb/common/webserver/funchandler"
	"github.com/gin-gonic/gin"
	"net/http"
)

type GatewayHeadFuncService struct {
	funchandler.FuncService
}

func NewGatewayHeadFuncService() *GatewayHeadFuncService {
	return new(GatewayHeadFuncService).init()
}

func (this *GatewayHeadFuncService) init() *GatewayHeadFuncService {

	this.InitRoute(this, http.MethodHead, consts.ApiGateway)
	return this
}

// @Summary		gateway
// @Description	通用网关
// @Tags	   gateway网关
// @Produce		json
// @@@Param			body	body	string	true	"query params: string "
// @Success		200		{object}	string		"成功"
// @Failure		400		{object}	string							"请求错误"
// @Failure		500		{object}	string							"内部错误"
// @Router			/gateway [head]
func (this *GatewayHeadFuncService) Execute(ctx *gin.Context) {

	var resp = this.ExeFuncs(ctx)
	if resp.Err() != nil {
		this.WriteIfErr(ctx, resp.Err())
	}
	for k, v := range resp.Resp().Header() {
		ctx.Header(k, v[0])
	}
	ctx.Writer.Write(resp.Body())
}

func (this *GatewayHeadFuncService) ExeFuncs(ctx *gin.Context) *webclient.WebMsgProxyResp {
	var routeParse = route.NewGatewayRouter()
	return routeParse.Parse(ctx).Get(ctx)

}
