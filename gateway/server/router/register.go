package router

import (
	"gitee.com/ichub/gogateway/gateway/server/service/gateway"
	"gitee.com/ichub/goweb/common/webserver"
	"gitee.com/ichub/goweb/common/webserver/funchandler"
)

// go install github.com/swaggo/swag/cmd/swag
// go get -u github.com/swaggo/swag/cmd/swag
const swaggapis0 = `
		@Summary，接口简介
		@Description，接口描述
		@Tags，接口的标签，用来给 API 分组的
		@Accept，接口接收入参的类型，支持mpfd（表单），json 等
		@Produce，接口返回的出参类型，支持mpfd（表单），json 等
		@Param，入参参数定义，从前往后分别是：
		如代码中 @Param user_id query string true "用户ID" minlength(1) maxlength(100) 所示，@Param 格式为：
		
		1.参数名称 2.参数类型 3.数据类型 4.是否为必填字段 5.参数描述 6.其它属性
		
		关于接口的路径和响应注释有：
		
		@Success，指定成功响应的数据，格式为 1.HTTP响应码 2.响应参数类型 3.响应数据类型 4.其它描述
		@Failure，失败响应后的数据，和 Success 一样
		@Router，指定路由和 HTTP 方法
`

func Register() *webserver.WebHandlers {

	gateway.Register()
	return funchandler.Build()
}
