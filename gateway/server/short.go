package main

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"net/http"
)

// shortLong 短链接ID和目标长链接映射关系，模拟数据库存储
var shortLong = map[string]string{
	"bd": "https://baike.baidu.com/item/%E7%9F%AD%E9%93%BE%E6%8E%A5/7224556?fr=ge_ala",
	"sg": "https://baike.sogou.com/v72514301.htm?fromTitle=%E7%9F%AD%E9%93%BE%E6%8E%A5",
}

// redirectHandler 查找链接映射，跳转到目标长链接
func redirectHandler(c *gin.Context) {
	shortCode := c.Param("core")
	longUrl, ok := shortLong[shortCode]
	if !ok {
		c.IndentedJSON(http.StatusNotFound, gin.H{
			"detail": fmt.Sprintf("短链接(%s)无对应的长链接地址", shortCode),
		})
		return
	}
	c.Redirect(http.StatusMovedPermanently, longUrl)
}
func mains() {
	engine := gin.Default()
	engine.GET("/:core", redirectHandler)
	if err := engine.Run(":9999"); err != nil {
		logrus.Fatalf("启动gin server失败：%v", err)
	}

}
