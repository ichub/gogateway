package main

import (
	"gitee.com/ichub/goapi/api/goauth"
	"gitee.com/ichub/goapi/api/goauth/authdto"
	"gitee.com/ichub/goconfig/common/base/baseconsts"
	"gitee.com/ichub/goconfig/common/base/baseutils/fileutils"
	"gitee.com/ichub/goconfig/common/ichublog"
	"gitee.com/ichub/gogateway/gateway/server/consts"
	"gitee.com/ichub/gogateway/gateway/server/dto"
	"gitee.com/ichub/goweb/common/pagemsg/page"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/suite"
	"testing"
)

type TestGatewayPostPage struct {
	suite.Suite
	token string
}

func TestGatewayPostPages(t *testing.T) {
	suite.Run(t, new(TestGatewayPostPage))
}

func (this *TestGatewayPostPage) init() {
	fileutils.FindRootDir()
	ichublog.InitLogrus()

}
func (this *TestGatewayPostPage) SetupTest() {
	this.init()
	this.login()
	logrus.Info("SetupTest")

}

// var token = "4706dd81eb984eb1bed71869460940b6"
func (this *TestGatewayPostPage) login() {

	var result = goauth.SingleAuthApi().Login(authdto.NewLoginRequest())
	logrus.Info(result)
	if result.IsSuccess() {
		//this.token = dto["token"].(string)

	}
	this.Equal(200, result.Code)

}
func (this *TestGatewayPostPage) TearDownTest() {

	logrus.Info("TearDownTest")
}

func (this *TestGatewayPostPage) SetupSuite() {

	logrus.Println("Suite setup")
	logrus.Println("领域驱动设计，测试驱动开发!")

}

func (this *TestGatewayPostPage) TearDownSuite() {
	logrus.Info("Suite teardown")
}

func (this *TestGatewayPostPage) Test001_PostPageDept() {

	var gatewayRequest = dto.NewGatewayRequest()
	gatewayRequest.ServiceId = consts.SEVICE_ID_NAME_DEPTEMP
	gatewayRequest.Path = "dept/query"
	gatewayRequest.PutHeader(baseconsts.AUTH_HEADER_TOKEN, this.token)

	var req = page.Default()
	var pageResult = gatewayRequest.PostPage(req)
	//logrus.Info(pageResult)

	this.Equal(200, pageResult.Code)
}

func (this *TestGatewayPostPage) Test002_PostPageEmp() {

	var gatewayRequest = dto.NewGatewayRequest()
	gatewayRequest.ServiceId = consts.SEVICE_ID_NAME_DEPTEMP
	gatewayRequest.Path = "emp/query"
	gatewayRequest.PutHeader(baseconsts.AUTH_HEADER_TOKEN, this.token)

	var pageRequest = page.Default()
	pageRequest.PageSize = 2
	var pageResult = gatewayRequest.PostPage(pageRequest)
	logrus.Info(pageResult)

	this.Equal(200, pageResult.Code)
}

func (this *TestGatewayPostPage) Test003_PostProxy() {

	var gatewayRequest = dto.NewRequestPath("emp/query")
	gatewayRequest.ServiceId = consts.SEVICE_ID_NAME_DEPTEMP
	gatewayRequest.PutHeader(baseconsts.AUTH_HEADER_TOKEN, this.token)

	var pageRequest = page.NewPageRequest(1, 1)
	var pageResult = gatewayRequest.PostProxy(pageRequest)

	logrus.Info(pageResult.ToPrettyString())
	this.Equal(200, pageResult.Code)
}
