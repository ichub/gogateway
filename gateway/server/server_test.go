package main

import (
	"gitee.com/ichub/goapi/api/goauth"
	"gitee.com/ichub/goapi/api/goauth/authdto"
	"gitee.com/ichub/goconfig/common/base/baseconfig"
	"gitee.com/ichub/goconfig/common/base/baseconsts"
	"gitee.com/ichub/goconfig/common/base/baseiface"
	"gitee.com/ichub/goconfig/common/base/baseutils/fileutils"
	"gitee.com/ichub/goconfig/common/base/baseutils/goutils"
	"gitee.com/ichub/goconfig/common/base/baseutils/jsonutils"
	"gitee.com/ichub/goconfig/common/base/encrypt"
	"gitee.com/ichub/goconfig/common/ichubconfig"
	"gitee.com/ichub/goconfig/common/ichublog"
	"gitee.com/ichub/gogateway/gateway/server/consts"
	"gitee.com/ichub/gogateway/gateway/server/dto"
	"gitee.com/ichub/goweb/common/pagemsg/page"
	"gitee.com/ichub/goweb/common/webclient"
	"gitee.com/ichub/goweb/common/webclient/webmsg"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/suite"
	"os"
	"reflect"
	"testing"
)

type TestGatewaySuite struct {
	suite.Suite
	token string
	cfg   *ichubconfig.IchubConfig
}

func TestGatewaySuites(t *testing.T) {
	suite.Run(t, new(TestGatewaySuite))
}

func (this *TestGatewaySuite) init() {
	fileutils.FindRootDir()
	ichublog.InitLogrus()

}
func (this *TestGatewaySuite) SetupTest() {
	this.init()
	this.login()
	this.cfg = ichubconfig.Default()

}

func (this *TestGatewaySuite) TearDownTest() {

}

func (this *TestGatewaySuite) SetupSuite() {

	//logrus.Println("Suite setup 领域驱动设计，测试驱动开发!")

}

func (this *TestGatewaySuite) TearDownSuite() {

}
func (this *TestGatewaySuite) Test1001_gatewayQueryDept() {

	var web = webmsg.NewWebMsg()
	web.SetUrl("gateway/deptemp/dept/query")
	web.PutHeader(baseconsts.AUTH_HEADER_TOKEN, this.token)
	var req = page.Default()
	req.PageSize = 2

	web.SetBody(req.ToJsonBytes())
	var result = webclient.Default().Post2PageResult(web)

	this.Suite.Equal(200, result.Code)
	goutils.Info(result)

}
func (this *TestGatewaySuite) Test1002_gatewayQueryEmp() {

	var web = webmsg.NewWebMsg()
	web.SetUrl("gateway/deptemp/emp/query")
	web.PutHeader(baseconsts.AUTH_HEADER_TOKEN, this.token)
	var req = page.Default()
	req.PageSize = 2
	web.SetBody(req.ToJsonBytes())
	var result = webclient.Default().Post2PageResult(web)

	this.Suite.Equal(200, result.Code)
	goutils.Info(result, req)

}
func (this *TestGatewaySuite) Test1003_QueryEmpByGateway() {

	var request = dto.NewGatewayRequest()
	request.ServiceId = consts.SEVICE_ID_NAME_DEPTEMP
	request.Path = "emp/query"
	request.SetUrl(request.FullPath())
	request.PutHeader(baseconsts.AUTH_HEADER_TOKEN, this.token)
	request.SetBody(page.Default().ToJsonBytes())
	var result = request.Post2PageResult()

	this.Suite.Equal(200, result.Code)
	logrus.Info(result)
	ichublog.Log(result.ToPrettyString())

}
func (this *TestGatewaySuite) Test002_LogString() {
	this.cfg.Read()
	goutils.Info(os.Getenv(baseconsts.IchubBasePath))
}

func (this *TestGatewaySuite) Test003_TestParseEnvvar() {
	os.Setenv("WEB_SERVER_HOST", "server.gateway.com")
	logrus.Info(os.Getenv("WEB_SERVER_HOST"))
	var ichubConfig = ichubconfig.Default().Read()
	this.Equal(os.Getenv("WEB_SERVER_HOST"), ichubConfig.Web.Server.Name)
	var dto = ichubConfig.ReadIchubMysql()

	this.Equal("leijmdas@163.comL", dto.Password)
	goutils.Info(ichubConfig)

}
func (this *TestGatewaySuite) Test004_TestParseReadMap() {

	var ichubConfig = ichubconfig.Default().Read()

	m := ichubConfig.ReadMap("server")
	logrus.Info(jsonutils.ToJsonPretty(m))
}

func (this *TestGatewaySuite) Test005_TestReadStruct() {

	var clientDto = baseconfig.NewSwaggerClientDto()
	this.cfg.RegisterEncDec(encrypt.EncDecInst).ReadStruct("server.swagger", clientDto)
	logrus.Info(clientDto)

}
func (this *TestGatewaySuite) Test006_TestReadString() {

	var url = this.cfg.ReadString("es.url")
	logrus.Info("\r\n", url)

}

func (this *TestGatewaySuite) Test0007_findRoot() {

	var r = fileutils.FindRootDir()
	logrus.Info(r)

	var cfg = ichubconfig.NewIchubConfig()
	cfg.Read()
	logrus.Info(cfg)

}
func (this *TestGatewaySuite) Test0008_web() {

	var webcli = webclient.Default()

	var result = webcli.Get("/")
	logrus.Info(result)
	this.Suite.Equal(200, result.Code)
}

func (this *TestGatewaySuite) Test010_readConfigString() {

	this.cfg.Read()
	var s = this.cfg.ReadString("gateway.byPass.path")
	goutils.Info(s)

}
func (this *TestGatewaySuite) Test011_readgateArray() {

	var g = this.cfg.ReadGateway()
	logrus.Info("g =", jsonutils.ToJsonStr(g))
	var m = map[string]string{}
	m["1"] = "11"
	s, f := m["1"]
	logrus.Info(s, f)
	ss := m["1"]
	logrus.Info(ss)
	//	var name = reflect.TypeOf(ifuncservice).Name()

}
func (this *TestGatewaySuite) Test012_readTypeName() {

	var name = reflect.TypeOf(this).Name()
	logrus.Info("this=", name)
}

// var token = "4706dd81eb984eb1bed71869460940b6"
func (this *TestGatewaySuite) login() {

	var result = goauth.SingleAuthApi().Login(authdto.NewLoginRequest())

	this.Equal(200, result.Code)
	if result.IsSuccess() {
		var loginResult = result.Data.(*authdto.LoginResult)
		this.token = loginResult.Token
		goutils.Info(this.token)
	}
}

func (this *TestGatewaySuite) Test015_QueryEmpByGateway() {

	var dto = dto.NewGatewayRequest()
	dto.ServiceId = consts.SEVICE_ID_NAME_DEPTEMP
	dto.Path = "emp/query" //dto.SetUrl(dto.FullPath())
	dto.PutHeader(baseconsts.AUTH_HEADER_TOKEN, this.token)
	var result = dto.Get() // webcli.GetHttp(dto.WebHttpClient)// result = dto.Get()
	this.Suite.Equal(200, result.Code)

	ichublog.Log(result.ToPrettyString())
}

func (this *TestGatewaySuite) Test016_QueryDeptByGateway() {

	var dto = dto.NewGatewayRequest()
	dto.ServiceId = consts.SEVICE_ID_NAME_DEPTEMP
	dto.Path = "dept/query"
	dto.PutHeader(baseconsts.AUTH_HEADER_TOKEN, this.token)
	dto.PutHeader("Content-Type", "application/json")
	var req = page.NewPageRequest(1, 1)
	dto.SetBody(req.ToJsonBytes())

	var result = dto.Post()
	ichublog.Log(result.ToPrettyString())

	this.Suite.Equal(200, result.Code)

}
func (this *TestGatewaySuite) Test017_QueryEmpByGatewayPage() {

	var dto = dto.NewGatewayRequest()
	dto.ServiceId = consts.SEVICE_ID_NAME_DEPTEMP
	dto.Path = "emp/query"
	dto.PutHeader(baseconsts.AUTH_HEADER_TOKEN, this.token)
	var result = dto.Get2PageResult()
	this.Suite.Equal(200, result.Code)

	ichublog.Log(result.ToPrettyString())
}

func (this *TestGatewaySuite) Test018_QueryDeptByGatewayPage() {

	var dto = dto.NewGatewayRequest()
	dto.ServiceId = consts.SEVICE_ID_NAME_DEPTEMP
	dto.Path = "dept/query"
	dto.PutHeader(baseconsts.AUTH_HEADER_TOKEN, this.token)
	dto.PutHeader("Content-Type", "application/json")
	var req = page.NewPageRequest(2, 1)
	dto.SetBody(req.ToJsonBytes())

	var result = dto.Post2PageResult()
	ichublog.Log(result.ToPrettyString())

	this.Suite.Equal(200, result.Code)

}
func (this *TestGatewaySuite) Test019_QueryEmpByGatewaygGetResp() {

	var gatewayRequest = dto.NewGatewayRequest()
	gatewayRequest.ServiceId = consts.SEVICE_ID_NAME_DEPTEMP
	gatewayRequest.Path = "emp/query"
	gatewayRequest.PutHeader(baseconsts.AUTH_HEADER_TOKEN, this.token)
	var req = page.Default()
	req.PageSize = 1
	gatewayRequest.SetBody(req.ToJsonBytes())
	resp, err := gatewayRequest.PostResp()
	if err != nil {
		logrus.Error(err)
		return
	}
	var result = gatewayRequest.Post()

	logrus.Info(result, jsonutils.ToJsonPretty(resp.Header()))

}
func (this *TestGatewaySuite) Test020_QueryDptByGatewaygGetResp() {

	var gatewayRequest = dto.NewGatewayRequest()
	gatewayRequest.ServiceId = consts.SEVICE_ID_NAME_DEPTEMP
	gatewayRequest.Path = "dept/query"
	gatewayRequest.PutHeader(baseconsts.AUTH_HEADER_TOKEN, this.token)
	var req = page.Default()
	req.PageSize = 2
	gatewayRequest.SetBody(req.ToJsonBytes())
	resp, err := gatewayRequest.PostResp()
	if err != nil {
		logrus.Error(err)
		return
	}
	this.Equal(200, resp.StatusCode())

	logrus.Info(jsonutils.ToJsonPretty(resp.Header()), resp.StatusCode())
	var result = gatewayRequest.Post()
	logrus.Info(result)
}
func (this *TestGatewaySuite) Test021_gatewayPostProxy() {

	var gatewayRequest = dto.NewRequestPath("dept/query")
	gatewayRequest.ServiceId = consts.SEVICE_ID_NAME_DEPTEMP
	gatewayRequest.PutHeader(baseconsts.AUTH_HEADER_TOKEN, this.token)
	var proxy baseiface.IbaseProxy
	proxy = page.NewPageRequest(2, 1)
	var result = gatewayRequest.PostProxy(proxy)

	logrus.Info(result)
}
