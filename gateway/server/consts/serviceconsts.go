package consts

const (
	MSG_SEVICE_NOFOUND      = "centerservice not found "
	SEVICE_ID_NAME_DEPTEMP  = "deptemp"
	SEVICE_ID_NAME_DATADICT = "datadict"
	SEVICE_ID_NAME_GOUSER   = "gouser"
	SEVICE_ID_NAME_GOAUTH   = "goauth"

	SEVICE_ID_NAME_GOHRMS    = "gohrms"
	SEVICE_ID_NAME_GOPAYMENT = "gopayment"
	SEVICE_ID_NAME_GOATTEND  = "goattend"
)
