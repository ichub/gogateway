package consts

const ApiGatewayHome = "gateway"
const FuncGatewayGet = "gatewayget"
const FuncGatewayPost = "gatewaypost"

const ApiGateway = "/gateway/:serviceId/*path"
