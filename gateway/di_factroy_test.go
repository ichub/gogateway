package di

import (
	"errors"
	"fmt"

	"runtime"
	"testing"
	"time"
)

type TestDiFactoySuite struct {
	suite.Suite
	DiFactroy *difactroy.DiFactroy
	rootdir   string
}

func (this *TestDiFactoySuite) SetupTest() {
	logrus.Info("difactry...")
	ichublog.InitLogrus()
	this.rootdir = fileutils.FindRootDir()
	this.DiFactroy = difactroy.NewDiFactroy()

	var i baseiface.ISingleton = nil
	logrus.Info(i)

}

func TestDiFactoySuites(t *testing.T) {
	suite.Run(t, new(TestDiFactoySuite))
}
func CurrentFile() string {
	_, file, _, ok := runtime.Caller(1)
	if !ok {
		panic(errors.New("Can not get current file info"))
	}
	return file
}

// 指定一个文件生成
func (this *TestDiFactoySuite) Test001_MakeDiFile() {

	this.DiFactroy.MakeDiFile("./difactroy/di_factroy.go")
	logrus.Info(CurrentFile())

}

// 指定结构体structName生成注册与注入代码
func (this *TestDiFactoySuite) Test002_MakeDiStru() {

	this.DiFactroy.Rootdir = fileutils.FindRootDirGoMod()
	this.DiFactroy.MakeDiStru("SingleEntity")

}

// 生成所有, 继承baseentity的结果: 已经有不再生成！
func (this *TestDiFactoySuite) Test003_MakeDiAll() {

	this.DiFactroy.MakeDiAll()

}

// 生成所有 继承baseentity的结果，强制执行
func (this *TestDiFactoySuite) Test004_MakeDiForce() {

	this.DiFactroy.MakeDiAllForce(true)

}

// 查找所有GO文件
func (this *TestDiFactoySuite) Test007_FindAllFiles() {
	this.DiFactroy.FindGoFiles()

	logrus.Info("basedi=", jsonutils.ToJsonPretty(this.DiFactroy))
}

// AST分析所有文件
func (this *TestDiFactoySuite) Test008_ParseAll() {

	this.DiFactroy.ParseAll()
	var cf, found = this.DiFactroy.FindSome("SingleEntity")
	if found {
		logrus.Info(cf.ToString())
	} else {
		logrus.Error("\r\n!found ", found)
	}
}

func (this *TestDiFactoySuite) Test009_verifyCode() {
	var dd = base64Captcha.NewDriverDigit(80, 240, 5, 0.7, 80)
	var store = base64Captcha.NewMemoryStore(10, 10*time.Minute)
	// 创建一个验证码生成器
	capt := base64Captcha.NewCaptcha(dd, store)

	// 生成一个验证码
	id, b64s, code, err := capt.Generate()
	if err != nil {
		fmt.Println("生成验证码失败:", err, code)
		return
	}

	// 打印验证码的ID
	fmt.Println("验证码ID:", id)
	// 打印验证码的base64编码字符串
	fmt.Println("验证码base64编码:", b64s)

	// 这里可以保存验证码的ID和base64编码字符串到数据库中
	// 同时将base64编码解码为图片并展示给用户
	var userInputId string = id

	// 校验验证码
	if capt.Verify(userInputId, code, true) {
		goutils.Info("验证码正确", code)
	} else {
		fmt.Println("验证码错误")
	}
}
