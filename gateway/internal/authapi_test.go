package internal

import (
	"fmt"
	"gitee.com/ichub/goapi/api/goauth"
	"gitee.com/ichub/goapi/api/goauth/authdto"
	"gitee.com/ichub/goconfig/common/base/basedto"
	"gitee.com/ichub/goconfig/common/base/baseutils/fileutils"
	"gitee.com/ichub/goconfig/common/ichublog"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/suite"
	"testing"
)

/*
	@Title    文件名称: asuite_test.go
	@Description  描述: 有芯规则引擎执行的函数与对象集

	@Author  作者: leijianming@163.com  时间(2024-02-18 22:38:21)
	@Update  作者: leijianming@163.com  时间(2024-02-18 22:38:21)
*/
// 依赖 suite.Suite
type TestAuthApiWebSuite struct {
	suite.Suite
	token string
}

func (this *TestAuthApiWebSuite) loginToken() string {

	return goauth.SingleAuthApi().LoginToken(authdto.NewLoginRequest())

}
func (this *TestAuthApiWebSuite) login() {

	var result = goauth.SingleAuthApi().Login(authdto.NewLoginRequest())
	logrus.Info(result)
	if result.IsSuccess() {
		var data = result.Data.(*authdto.LoginResult)
		this.token = data.Token
	}
	this.Equal(200, result.Code)

}

// 用于 'go test' 的入口
func TestWebSuites(t *testing.T) {
	suite.Run(t, new(TestAuthApiWebSuite))
}

// 每个测试运行前，会执行
func (this *TestAuthApiWebSuite) SetupTest() {
	ichublog.InitLogrus()
	fileutils.FindRootDir() //this.login()
	this.token = this.loginToken()
	logrus.Info("SetupTest")

}

func (this *TestAuthApiWebSuite) TearDownTest() {

	logrus.Info("TearDownTest")
}
func (this *TestAuthApiWebSuite) SetupSuite() {
	fmt.Println("Suite setup")
	fmt.Println("领域驱动设计，测试驱动开发!")

}

func (this *TestAuthApiWebSuite) TearDownSuite() {
	fmt.Println("Suite teardown")
}
func (this *TestAuthApiWebSuite) Test0001_VerifyCode() {

	var result = goauth.SingleAuthApi().MakeVerifyCode()
	logrus.Info(result)
	this.Equal(200, result.Code)
}

func (this *TestAuthApiWebSuite) Test0002_Login() {

	var result = goauth.SingleAuthApi().Login(authdto.NewLoginRequest())
	logrus.Info(result)
	this.Equal(200, result.Code)

}

func (this *TestAuthApiWebSuite) Login() *basedto.IchubResult {

	var result = goauth.SingleAuthApi().Login(authdto.NewLoginRequest())
	this.Equal(200, result.Code)
	return result

}

func (this *TestAuthApiWebSuite) Test003_CheckLogin() {

	var result = goauth.SingleAuthApi().CheckLogin(this.token)
	logrus.Info(result)
	this.Equal(200, result.Code)
	this.Equal(true, result.Data.(bool))
}

func (this *TestAuthApiWebSuite) Test004_Auth() {

	var result = goauth.SingleAuthApi().Auth(this.token)
	logrus.Info(result)
	this.Equal(200, result.Code)
}

func (this *TestAuthApiWebSuite) Test005_Logout() {

	var result = goauth.SingleAuthApi().Logout(this.token)
	logrus.Info(result)
	this.Equal(200, result.Code)
}
