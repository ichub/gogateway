package consts

const (
	MSG_TOKEN_IS_NULL          = "token is null"
	MSG_APIKEY_IS_NULL         = "apikey is null"
	MSG_SEVICE_GATEWAY_NOFOUND = "服务名未找到！请检查网关配置！"
	MSG_SEVICE_NOFOUND         = "服务未找到！"
)
