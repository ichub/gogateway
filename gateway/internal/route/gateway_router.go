package route

import (
	"errors"
	"gitee.com/ichub/goapi/api/goauth"
	"gitee.com/ichub/goapi/base/redisutils"
	"gitee.com/ichub/goconfig/common/base/basedto"
	"gitee.com/ichub/goconfig/common/base/baseutils/goutils"
	"gitee.com/ichub/gogateway/gateway/internal/consts"
	"gitee.com/ichub/goweb/common/webclient"
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"net/http"
	"strings"
)

type GatewayRouter struct {
	*GatewayConfig
	WebClient *webclient.IchubWebClient
}

func NewGatewayRouter() *GatewayRouter {
	var r = &GatewayRouter{
		GatewayConfig: NewGatewayConfig(),
	}
	//r.Config = ichubconfig.Default()
	//r.Config.ReadGateway()

	r.InitProxy(r)
	r.WebClient = webclient.Default()

	return r
}

// gateway/deptemp/queryemp
// consts ApiGateway = "/gateway/:serviceId/*path"
func (r *GatewayRouter) Parse(ctx *gin.Context) *GatewayRouter {
	serviceId := ctx.Param("serviceId")
	path := ctx.Param("path")

	// 示例：简单的参数验证，实际情况应根据需求加强
	if serviceId == "" || path == "" {
		logrus.Error("ServiceId or Path is empty")
		return r
	}

	r.ServiceId = serviceId
	r.Path = path

	goutils.Info(r)
	return r

}

func (r *GatewayRouter) CheckLoginAuth(ctx *gin.Context) *basedto.IchubResult {

	// 使用配置文件或常量管理错误消息
	const errorMsgLoginFailed = "请重新登录！"

	token := redisutils.GetToken(ctx) // 改为更健壮的错误处理

	result := goauth.SingleAuthApi().CheckLogin(token)
	if !result.Data.(bool) {
		result.FailMessage(errorMsgLoginFailed)
		result.InitProxy(result)
	}
	return result

}

func (r *GatewayRouter) buildRouteUrl() (string, error) {

	r.WebClient.ServerName = r.ServiceId
	if err := r.RefreshAddr(); err != nil {
		logrus.Error(err.Error())
		return "", err
	}
	return r.DestPath(), nil

}

func (r *GatewayRouter) DestPath() string {
	var builder strings.Builder
	builder.WriteString(r.WebClient.BaseUrl)
	builder.WriteString(r.Path)
	return builder.String()
}

func (r *GatewayRouter) RefreshAddr() error {

	r.WebClient.ServerName = r.FindServiceName()
	if r.WebClient.ExistServiceName() {
		r.WebClient.TestUrl = ""
		r.WebClient.RefreshAddr()
		return nil
	}
	return basedto.NewIchubError(500, consts.MSG_SEVICE_GATEWAY_NOFOUND)
}

func (r *GatewayRouter) Filter(ctx *gin.Context) *basedto.IchubResult {

	if r.Bypass() {
		return basedto.NewIchubResult().Success()
	}
	logrus.Warn(r.FullPath())
	return r.CheckLoginAuth(ctx)

}

// 提高代码复用性，减少冗余
func (r *GatewayRouter) processRequest(ctx *gin.Context, m string) *webclient.WebMsgProxyResp {
	result := r.Filter(ctx)
	if !result.IsSuccess() {
		var res = webclient.NewWebMsgProxyResp()
		res.SetBody(result.ToBytes())
		return res
	}
	url, err := r.buildRouteUrl()
	if err != nil {
		var res = webclient.NewWebMsgProxyResp()
		res.SetErr(err)
		res.SetBody(result.ToBytes())
		return res
	}
	var req = webclient.NewWebMsgProxy(url)
	switch m {
	case http.MethodGet:
		return req.Get(ctx, r.WebClient)
	case http.MethodPost:
		return req.Post(ctx, r.WebClient)
	case http.MethodDelete:
		return req.Delete(ctx, r.WebClient)
	case http.MethodPut:
		return req.Put(ctx, r.WebClient)
	default:
		var res = webclient.NewWebMsgProxyResp()
		res.SetErr(errors.New("不支持的方法！"))
		res.SetBody(result.ToBytes())
		return res
	}
	// 此处根据实际需要调用对应的方法，如Get, Post, Delete, Put
}
func (r *GatewayRouter) Get(ctx *gin.Context) *webclient.WebMsgProxyResp {
	return r.processRequest(ctx, http.MethodGet)
}

// copyHeaders
func (r *GatewayRouter) Post(ctx *gin.Context) *webclient.WebMsgProxyResp {
	return r.processRequest(ctx, http.MethodPost)
}

func (r *GatewayRouter) Delete(ctx *gin.Context) *webclient.WebMsgProxyResp {

	return r.processRequest(ctx, http.MethodDelete)
}

func (r *GatewayRouter) Put(ctx *gin.Context) *webclient.WebMsgProxyResp {

	return r.processRequest(ctx, http.MethodPut)
}
