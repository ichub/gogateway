package route


import (
	"gitee.com/ichub/goconfig/common/base/baseutils/fileutils"
	"gitee.com/ichub/goconfig/common/ichublog"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/suite"
	"testing"
)

type TestGatewayConfigSuite struct {
	suite.Suite
	inst *GatewayConfig

	rootdir string
}

func (this *TestGatewayConfigSuite) SetupTest() {

	ichublog.InitLogrus()
	//this.inst = FindBeanGatewayConfig()

	this.rootdir = fileutils.FindRootDir()

}

func TestGatewayConfigSuites(t *testing.T) {
	suite.Run(t, new(TestGatewayConfigSuite))
}



func (this *TestGatewayConfigSuite) Test001_init() {
    logrus.Info(1)

}


func (this *TestGatewayConfigSuite) Test002_FindServiceName() {
    logrus.Info(1)

}


func (this *TestGatewayConfigSuite) Test003_Bypass() {
    logrus.Info(1)

}


func (this *TestGatewayConfigSuite) Test004_FullPath() {
    logrus.Info(1)

}


func (this *TestGatewayConfigSuite) Test005_LogGateway() {
    logrus.Info(1)

}

