package route

import (
	"gitee.com/ichub/goconfig/common/base/basedto"
	"gitee.com/ichub/goconfig/common/base/baseutils/jsonutils"
	"gitee.com/ichub/goconfig/common/ichubconfig"
	"github.com/sirupsen/logrus"
)

type GatewayConfig struct {
	basedto.BaseEntity
	Config *ichubconfig.IchubConfig `json:"-"`

	ServiceId string `json:"service_id"`
	Path      string `json:"path"`
}

func NewGatewayConfig() *GatewayConfig {
	var G = &GatewayConfig{}
	G.Config = ichubconfig.Default()
	G.Config.ReadGateway()

	G.InitProxy(G)
	return G
}

func (this *GatewayConfig) init() {
	this.Config.ReadGateway()
}

func (this *GatewayConfig) FindServiceName() string {
	var gateway = this.Config.ReadGateway()

	//path: /datadict
	//serviceId: server.platform.com
	return gateway.FindServiceName("/" + this.ServiceId)
}

func (this *GatewayConfig) Bypass() bool {
	var gateway = this.Config.ReadGateway()
	return gateway.Bypass(this.FullPath())
}

func (this *GatewayConfig) FullPath() string {
	return this.ServiceId + this.Path
}
func (this *GatewayConfig) LogGateway() {
	var gateway = this.Config.ReadGateway()
	logrus.Info(jsonutils.ToJsonPretty(gateway))

}
