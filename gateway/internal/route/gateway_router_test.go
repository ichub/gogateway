package route

import (
	"gitee.com/ichub/goconfig/common/base/baseconfig"
	"gitee.com/ichub/goconfig/common/base/baseutils/fileutils"
	"gitee.com/ichub/goconfig/common/base/baseutils/goutils"
	"gitee.com/ichub/goconfig/common/base/baseutils/jsonutils"
	"gitee.com/ichub/goconfig/common/base/encrypt"
	"gitee.com/ichub/goconfig/common/ichubconfig"
	"gitee.com/ichub/goconfig/common/ichublog"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/suite"
	"os"
	"testing"
)

type TestGatewaySuite struct {
	suite.Suite
}

func TestGatewaySuites(t *testing.T) {
	suite.Run(t, new(TestGatewaySuite))
}

func (this *TestGatewaySuite) SetupTest() {
	this.init()
	logrus.Info("SetupTest")

}

func (this *TestGatewaySuite) TearDownTest() {

	logrus.Info("TearDownTest")
}

func (this *TestGatewaySuite) SetupSuite() {
	this.init()
	logrus.Println("Suite setup")
	logrus.Println("领域驱动设计，测试驱动开发!")

}

func (this *TestGatewaySuite) TearDownSuite() {
	logrus.Info("Suite teardown")
}

func (this *TestGatewaySuite) init() {
	fileutils.FindRootDir()
	ichublog.InitLogrus()

}
func (this *TestGatewaySuite) Test001_TestParsevar() {
	os.Setenv("WEB_SERVER_HOST", "huawei.server.com")
	logrus.Info(os.Getenv("WEB_SERVER_HOST"))
	var ichubConfig = ichubconfig.Default().Read()
	this.Equal(os.Getenv("WEB_SERVER_HOST"), ichubConfig.Web.Server.Name)
	var ichubMysql = ichubConfig.ReadIchubMysql()

	this.Equal("leijmdas@163.comL", ichubMysql.Password)
	goutils.Info(ichubMysql)

}
func (this *TestGatewaySuite) Test002_ReadGateway(t *testing.T) {

	os.Setenv("WEB_SERVER_HOST", "huawei.server.com")
	logrus.Info(os.Getenv("WEB_SERVER_HOST"))
	var ichubConfig = ichubconfig.Default().Read()
	this.Equal(os.Getenv("WEB_SERVER_HOST"), ichubConfig.Web.Server.Name)
	//var g = ichubConfig.ReadGateway()

}

func (this *TestGatewaySuite) Test003_TestCfg() {

	var config = ichubconfig.Default().Read()
	var gateway = config.ReadGateway()

	ichublog.Log("g=", jsonutils.ToJsonPretty(gateway))

}
func (this *TestGatewaySuite) Test004_TestParseReadMap() {

	var ichubConfig = ichubconfig.Default().Read()

	m := ichubConfig.ReadMap("server")
	logrus.Info(jsonutils.ToJsonPretty(m))
}

func (this *TestGatewaySuite) Test005_TestReadStruct() {

	var clientDto = baseconfig.NewSwaggerClientDto()
	ichubconfig.Default().RegisterEncDec(encrypt.EncDecInst).ReadStruct("server.swagger", clientDto)
	logrus.Info(clientDto)

}
func (this *TestGatewaySuite) Test006_TestReadString() {

	var url = ichubconfig.Default().ReadString("es.url")
	logrus.Info("\r\n", url)
	ichublog.Log(url)

}

func (this *TestGatewaySuite) Test0007_findRoot() {

	var r = fileutils.FindRootDir()
	logrus.Info(r)

	var cfg = ichubconfig.Default()
	cfg.Read()
	logrus.Info(cfg)

}

func (this *TestGatewaySuite) Test0010_LOGGATEWAY() {

	NewGatewayRouter().LogGateway()

}
