package route

import (
	"gitee.com/ichub/goconfig/common/base/baseiface"
	"gitee.com/ichub/goconfig/common/basedi"
	"github.com/sirupsen/logrus"
)

/*
	@Title  文件名称: gateway_config_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: LEIJMDAS@163.COM 时间(2024-05-05 09:00:17)
	@Update 作者: LEIJMDAS@163.COM 时间(2024-05-05 09:00:17)
*/

const singleNameGatewayConfig = "route.GatewayConfig"

// init center load
func init() {
	registerBeanGatewayConfig()
}

// center GatewayConfig
func registerBeanGatewayConfig() {
	basedi.RegisterLoadBean(singleNameGatewayConfig, LoadGatewayConfig)
}

func FindBeanGatewayConfig() *GatewayConfig {
	bean, ok := basedi.FindBean(singleNameGatewayConfig).(*GatewayConfig)
	if !ok {
		logrus.Errorf("FindBeanGatewayConfig: failed to cast bean to *GatewayConfig")
		return nil
	}
	return bean

}

func LoadGatewayConfig() baseiface.ISingleton {
	var s = NewGatewayConfig()
	InjectGatewayConfig(s)
	return s

}

func InjectGatewayConfig(s *GatewayConfig) {

	logrus.Debug("inject")
}
