package gwconst

import "gitee.com/ichub/goweb/common/webclient"

const WEB_SEVICE_PF = "server.platform.com"
const WEB_SEVICE_BIZ = "server.biz.com"

func FindWebClientBiz() *webclient.IchubWebClient {
	return webclient.FindWebClient(WEB_SEVICE_BIZ)
}
func FindWebClientPlatform() *webclient.IchubWebClient {
	return webclient.FindWebClient(WEB_SEVICE_PF)
}
